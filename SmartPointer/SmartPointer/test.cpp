#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#include <memory>

//void Func1();
//void Func2();

//void Func1()
//{
//	int* p1 = new int;
//	int* p2 = new int;
//	int* p3 = new int;
//
//	Func2(); //执行流乱跳
//	//new 是可能抛异常的
//	//p1 抛异常 - 无需处理
//	//p2 抛异常 - 需要释放 p1
//	//p3 抛异常 - 需要释放 p1 p2
//	//Func2 抛异常 - 需要释放 p1 p2 p3
//	//如果需要处理，则需要先捕获释放再抛异常，十分复杂
//
//	//引出智能指针
//
//	delete p1;
//	delete p2;
//	delete p3;
//
//}
//
//void Func2()
//{
//	throw 1;
//}
//
//int main()
//{
//	Func1();
// 
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//智能指针原型
//void Func1();
//void Func2();
//
//template <class T>
//class SmartPtr
//{
//public:
//	SmartPtr(T* ptr) :_ptr(ptr) {}
//	~SmartPtr() { cout << "Delete:" << _ptr << endl; delete _ptr; }
//
//	T& operator*()
//	{
//		return *_ptr;
//	}
//
//	T* operator->()
//	{
//		return _ptr;
//	}
//
//private:
//	T* _ptr;
//};
//
//void Func1()
//{
//	int* p1 = new int;
//	SmartPtr<int> sp1(p1);
//	int* p2 = new int;
//	SmartPtr<int> sp2(p2);
//	int* p3 = new int;
//	SmartPtr<int> sp3(p3);
//
//	//或直接写成：Smart<int> sp(new int);
//
//	Func2();
//}
//
//void Func2()
//{
//	throw 1; //执行流跳出到 main 会销毁 Func1 即会析构 sp1 sp2 sp3 达到释放目的
//}
//
//int main()
//{
//	try
//	{
//		Func1();
//	}
//	catch (int e)
//	{
//		cout << "Expection" << endl;
//	}
// 
//	//但如果出现以下情况则会对同一空间释放两次，导致错误
//	//SmartPtr<int> sp1(new int);
//	//SmartPtr<int> sp2(sp1);
// 
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//智能指针的发展：
//C++98 - auto_ptr - 管理权转移
//C++98 ~ C++11 - boost库中使用 scoped_ptr shared_ptr weak_ptr
//C++11 - unique_ptr shared_ptr weak_ptr

///////////////////////////////////////////////////////////////////////////////////////////////////

//C++98 - auto_ptr
//namespace Thepale
//{
//	template <class T>
//	class auto_ptr
//	{
//	public:
//		auto_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//
//		auto_ptr(Thepale::auto_ptr<T>& obj) //管理权转移
//		{
//			_ptr = obj._ptr;
//			obj._ptr = nullptr; //被拷贝对象悬空
//		}
//
//		~auto_ptr()
//		{
//			cout << "Delete:" << _ptr << endl;
//			delete _ptr;
//		}
//
//	private:
//		T* _ptr;
//	};
//}
//
//int main()
//{
//	//手动实现：
//	Thepale::auto_ptr<int> tap1(new int(1));
//	Thepale::auto_ptr<int> tap2(tap1);
//
//	//库实现：（调试观察）
//	std::auto_ptr<int> sap1(new int(1));
//	std::auto_ptr<int> sap2(sap1);
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//C++11 - unique_ptr
//namespace Thepale
//{
//	template <class T>
//	class unique_ptr
//	{
//	public:
//		unique_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//
//		unique_ptr(Thepale::unique_ptr<T>& obj) = delete; //禁止拷贝，维持唯一性
//
//		~unique_ptr()
//		{
//			cout << "Delete:" << _ptr << endl;
//			delete _ptr;
//		}
//
//	private:
//		T* _ptr;
//	};
//}
//
//int main()
//{
//	//手动实现：
//	Thepale::unique_ptr<int> tup1(new int);
//	//Thepale::unique_ptr<int> tup2(tup1); - error
//
//	//库实现：
//	std::unique_ptr<int> sup1(new int);
//	//std::unique_ptr<int> sup2(sup1); - error
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//C++11 - shared_ptr
//namespace Thepale
//{
//	template <class T>
//	class shared_ptr
//	{
//	public:
//		shared_ptr(T* ptr)
//			:_ptr(ptr)
//			,_RefCount(new int(1))
//		{}
//
//		shared_ptr(Thepale::shared_ptr<T>& obj)
//			:_ptr(obj._ptr)
//			,_RefCount(obj._RefCount)
//		{
//			(*_RefCount)++;
//		}
//
//		Thepale::shared_ptr<T>& operator= (Thepale::shared_ptr<T>& obj)
//		{
//			if (_ptr == obj._ptr) { return *this; } //防止自赋值（用引用计数判断较好）
//			//tsp1 = tsp1 可以防止，若 tsp1 = tsp2 也是自赋值
//
//			if (*_RefCount == 1) { delete _ptr; delete _RefCount; }
//			else { (*_RefCount)--; }
//			 //原空间引用计数自减
//			//如果不是最后一个对象则原对象引用计数自减，转移该对象管理 obj 对象
//			//如果是最后一个对象则释放原对象空间，转移该对象管理 obj 对象
//
//			_RefCount = obj._RefCount;
//			_ptr = obj._ptr;
//
//			(*obj._RefCount)++;
//
//			return *this;
//		}
//
//		~shared_ptr()
//		{
//			if ((*_RefCount) == 1)
//			{
//				cout << "Delete:" << _ptr << endl;
//				delete _ptr;
//				_ptr = nullptr;
//			}
//			else
//			{
//				(*_RefCount)--;
//			}
//		}
//
//	private:
//		T* _ptr;
//		int* _RefCount;
//	};
//}
//
//int main()
//{
//	//手动实现：
//	//不使用 static 变量的原因：任何类实例化出的对象都会使用同一空间，这样创建新的对象（非拷贝构造创建）
//	//会把 static 变量初始化为 1，导致计数逻辑错乱
//
//	//思路：只有在首次创建时开辟引用计数空间，并一同管理，若非拷贝构造，则会重新开辟引用计数空间
//	Thepale::shared_ptr<int> tsp1(new int);
//	Thepale::shared_ptr<int> tsp2(tsp1);
//	Thepale::shared_ptr<int> tsp(new int); //额外开辟依然能正常释放
//	Thepale::shared_ptr<int> tsp3(tsp2);
//
//	tsp1 = tsp;
//	tsp2 = tsp;
//	tsp3 = tsp;
//
//	//库实现：
//	std::shared_ptr<int> ssp1(new int);
//	std::shared_ptr<int> ssp2(ssp1);
//	std::shared_ptr<int> ssp(new int);
//	std::shared_ptr<int> ssp3(ssp2);
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//循环引用问题（以 std 为 base）：
//struct ListNode
//{
//	int _val;
//	std::shared_ptr<ListNode> _prev;
//	std::shared_ptr<ListNode> _next;
//};
//
//int main()
//{
//	std::shared_ptr<ListNode> node1(new ListNode);
//	std::shared_ptr<ListNode> node2(new ListNode);
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	//循环引用（本例使用 std 中的 shared_ptr，若 shared_ptr 中涉及引用计数，则出现以下问题：）
//	node1->_next = node2;
//	node2->_prev = node1;
//	//node2 由于被 node1 的 _next 引用，所以 node2 的引用计数为 2
//	//node1 由于被 node2 的 _prev 引用，所以 node1 的引用计数为 1
//	//node2 后创建先销毁，引用计数自减为 1，此时 node2 的释放取决于 node1 的 _next
//	//node1 先创建后销毁，引用计数自减为 1，此时 node1 的释放取决于 node2 的 _prev
//	//互相牵制，你中有我我中有你，导致循环引用无法释放
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	return 0;
//}

//解决方案：
//weak_ptr - 可以正常访问节点资源但是不参与节点资源释放管理（即不增加计数）
//struct ListNode
//{
//	int _val;
//	std::weak_ptr<ListNode> _prev;
//	std::weak_ptr<ListNode> _next;
//};
//
//int main()
//{
//	std::shared_ptr<ListNode> node1(new ListNode);
//	std::shared_ptr<ListNode> node2(new ListNode);
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	node1->_next = node2;
//	node2->_prev = node1;
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//循环引用问题（以 Thepale 为 base）：
//namespace Thepale
//{
//	template <class T>
//	class shared_ptr
//	{
//	public:
//		shared_ptr(T* ptr = nullptr)
//			:_ptr(ptr)
//			,_RefCount(new int(1))
//		{}
//
//		shared_ptr(Thepale::shared_ptr<T>& obj)
//			:_ptr(obj._ptr)
//			,_RefCount(obj._RefCount)
//		{
//			(*_RefCount)++;
//		}
//
//		int use_count()
//		{
//			return *_RefCount;
//		}
//
//		T* get_ptr() const
//		{
//			return _ptr;
//		}
//
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T* operator->()
//		{
//			return _ptr;
//		}
//
//		Thepale::shared_ptr<T>& operator= (Thepale::shared_ptr<T>& obj)
//		{
//			if (_ptr == obj._ptr) { return *this; }
//
//			if (*_RefCount == 1) { delete _ptr; delete _RefCount; }
//			else { (*_RefCount)--; }
//
//			_RefCount = obj._RefCount;
//			_ptr = obj._ptr;
//
//			(*obj._RefCount)++;
//
//			return *this;
//		}
//
//		~shared_ptr()
//		{
//			if ((*_RefCount) == 1)
//			{
//				cout << "Delete:" << _ptr << endl;
//				delete _ptr;
//				_ptr = nullptr;
//			}
//			else
//			{
//				(*_RefCount)--;
//			}
//		}
//
//	private:
//		T* _ptr;
//		int* _RefCount;
//	};
//
//	//C++11 - weak_ptr
//	template <class T>
//	class weak_ptr
//	{
//	public:
//		weak_ptr(T* ptr = nullptr)
//			:_ptr(ptr)
//			, _RefCount(new int(1))
//		{}
//
//		weak_ptr(const Thepale::shared_ptr<T>& ptr)
//			:_ptr(ptr.get_ptr())
//		{}
//
//		Thepale::weak_ptr<T>& operator= (const Thepale::shared_ptr<T>& ptr)
//		{
//			_ptr = ptr.get_ptr();
//			return *this;
//		}
//
//		T& operator* ()
//		{
//			return *_ptr;
//		}
//
//		T* operator-> ()
//		{
//			return _ptr;
//		}
//
//	private:
//		T* _ptr;
//		int* _RefCount;
//	};
//}

//struct ListNode
//{
//	int _val = 0;
//	Thepale::shared_ptr<ListNode> _prev;
//	Thepale::shared_ptr<ListNode> _next;
//};
//
//int main()
//{
//	Thepale::shared_ptr<ListNode> node1(new ListNode);
//	Thepale::shared_ptr<ListNode> node2(new ListNode);
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	node1->_next = node2;
//	node2->_prev = node1;
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	//可发现 node1 和 node2 都没有析构 - 内存泄漏
//	return 0;
//}

//struct ListNode
//{
//	int _val = 0;
//	Thepale::weak_ptr<ListNode> _prev;
//	Thepale::weak_ptr<ListNode> _next;
//};
//
//int main()
//{
//	Thepale::shared_ptr<ListNode> node1(new ListNode);
//	Thepale::shared_ptr<ListNode> node2(new ListNode);
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	node1->_next = node2;
//	node2->_prev = node1;
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	//可发现 node1 和 node2 正常析构，引用计数不改变
//	//weak_ptr 的出现本质就是为了解决 shared_ptr 循环引用的问题
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//定制删除器
template<class T>
struct DeleteArray
{
	void operator() (const T* ptr)
	{
		cout << "DeleteArray" << endl;
		delete[] ptr;
	}
};

template<class T>
struct DeleteDefault
{
	void operator() (const T* ptr)
	{
		cout << "DeleteDefault" << endl;
		delete ptr;
	}
};

struct DeleteFile
{
	void operator() (FILE* fptr)
	{
		cout << "DeleteFile" << endl;
		fclose(fptr);
	}
};

namespace Thepale
{
	template <class T, class Delete = DeleteDefault<T>>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		unique_ptr(Thepale::unique_ptr<T>& obj) = delete;

		~unique_ptr()
		{
			cout << "Delete:" << _ptr << endl;

			//用仿函数释放空间
			Delete del;
			del(_ptr);
		}

	private:
		T* _ptr;
	};
}

struct Test
{
	~Test() 
	{
		cout << "~Test()" << endl;
	}

private:
	char a = 0;
	short b = 0;
	int c = 0;
};

int main()
{
	//std：
	//std::unique_ptr<Test, DeleteDefault<Test>> sup1(new Test);
	//std::unique_ptr<Test> sup2(new Test[10]); //报错 - 未对应释放（应使用 delete[]）
	//std::unique_ptr<Test, DeleteArray<Test>> sup2(new Test[10]); //仿函数决定删除方式

	//Thepale：
	Thepale::unique_ptr<Test, DeleteDefault<Test>> tup1(new Test);
	Thepale ::unique_ptr<Test, DeleteArray<Test>> tup2(new Test[10]);

	//释放文件类型（fopen/fclose）
	Thepale::unique_ptr<FILE, DeleteFile> tup3(fopen("test.txt", "w"));

	//unique_ptr 的删除器是在模板参数给（传类型） shared_ptr 的删除器在构造函数的参数给（传对象 - 则可以用 lambda 表达式）
	std::shared_ptr<FILE> ssp1(fopen("test.txt", "w"), DeleteFile());
	std::shared_ptr<FILE> ssp2(fopen("test.txt", "w"), [](FILE* fptr) {fclose(fptr);});

	return 0;
}