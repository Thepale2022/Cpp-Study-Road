#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

namespace K //Key模型
{
	template<class K>
	class BSTNode
	{
	public:
		BSTNode(const K& data = K()) :_data(data), _left(nullptr), _right(nullptr) {}

		BSTNode<K>* _left;
		BSTNode<K>* _right;
		K _data;
	};

	template <class K>
	class BSTree
	{
	public:
		BSTree() :_root(nullptr) {}

		bool Insert(const K& data)
		{
			if (_root == nullptr)
			{
				_root = new BSTNode<K>(data);
			}

			BSTNode<K>* prev = nullptr;
			BSTNode<K>* cur = _root;
			while (cur)
			{
				prev = cur;
				if (data > cur->_data)
				{
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}

			if (data > prev->_data)
			{
				prev->_right = new BSTNode<K>(data);
			}
			else
			{
				prev->_left = new BSTNode<K>(data);
			}

			return true;
		}

		//中序遍历（排序）
		void InOrder()
		{
			_InOrder(_root); //这样写是为了方便调用，因为没法直接传_root
		}
		void _InOrder(BSTNode<K>* node)
		{
			if (node == nullptr)
			{
				return;
			}

			_InOrder(node->_left);
			cout << node->_data << " ";
			_InOrder(node->_right);
		}

		BSTNode<K>* Find(const K& data)
		{
			BSTNode<K>* cur = _root;
			while (cur)
			{
				if (data > cur->_data)
				{
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}

			return cur; //本质就是nullptr
		}

		bool Erase(const K& data)
		{

			BSTNode<K>* cur = _root;
			BSTNode<K>* prev = nullptr;
			while (cur)
			{
				if (data > cur->_data)
				{
					prev = cur; //记录cur的前一个位置
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					prev = cur; //记录cur的前一个位置
					cur = cur->_left;
				}
				else
				{
					break;
				}
			}

			if (!cur) //判断是否找到
			{
				return false;
			}

			//1.没有孩子的节点   2.只有一个孩子节点
			if (cur->_left == nullptr || cur->_right == nullptr)
			{
				if (cur == _root) //防止prev空指针的情况出现（删只有一边孩子的根节点）
				{
					if (cur->_left == nullptr)
					{
						_root = cur->_right;
						delete cur;
					}
					else
					{
						_root = cur->_left;
						delete cur;
					}
					return true;
				}

				if (prev->_left == cur) //确定cur在prev的哪一边
				{
					if (cur->_left == nullptr) //确定prev接管谁
					{
						prev->_left = cur->_right;
					}
					else
					{
						prev->_left = cur->_left;
					}

					delete cur;
				}
				else
				{
					if (cur->_left == nullptr) //确定prev接管谁
					{
						prev->_right = cur->_right;
					}
					else
					{
						prev->_right = cur->_left;
					}

					delete cur;
				}

				return true;
			}
			else //3.有两个孩子节点
			{
				//找左边的最大节点
				BSTNode<K>* temp = cur->_left;
				BSTNode<K>* tempprev = nullptr;
				while (temp->_right)
				{
					tempprev = temp;
					temp = temp->_right;
				}
				if (tempprev == nullptr) //说明temp就是最大节点，即cur是前一节点（或者把直接把tempprev初始化成cur）
				{
					tempprev = cur;
				}

				//替换法删除（可直接替换值，也可替换节点，这里替换值）
				cur->_data = temp->_data;
				if (tempprev->_left == temp)
				{
					tempprev->_left = nullptr;
				}
				else
				{
					tempprev->_right = nullptr;
				}
				delete temp;

				return true;
			}
		}

		BSTNode<K>* _FindR(BSTNode<K>* node, const K& data)
		{
			if (node == nullptr) { return nullptr; }

			if (data > node->_data) { return _FindR(node->_right, data); }
			else if (data < node->_data) { return _FindR(node->_left, data); }
			else { return node; }
		}

		BSTNode<K>* FindR(const K& data)
		{
			return _FindR(_root, data);
		}

		bool _InsertR(BSTNode<K>*& node, const K& data) //这里的node是上一层父节点左/右指针的别名
		{
			if (node == nullptr)
			{
				node = new BSTNode<K>(data); //点睛之笔，引用传参
				return true;
			}

			if (data > node->_data) { return _InsertR(node->_right, data); }
			else if (data < node->_data) { return _InsertR(node->_left, data); }
			else { return false; }
		}

		bool InsertR(const K& data)
		{
			return _InsertR(_root, data);
		}

		bool _EraseR(BSTNode<K>*& node, const K& data) //这里的node是上一层父节点左/右指针的别名
		{
			if (node == nullptr) { return false; }

			if (data > node->_data) { return _EraseR(node->_right, data); }
			else if (data < node->_data) { return _EraseR(node->_left, data); }
			else
			{
				BSTNode<K>* del = node;
				//1.叶子节点情况   2.一个孩子节点情况
				if (node->_left == nullptr) { node = node->_right; delete del; }
				else if (node->_right == nullptr) { node = node->_left; delete del; } //点睛之笔，引用删除
				else //3.双孩子节点情况
				{
					BSTNode<K>* max_left = node->_left;
					while (max_left->_right) { max_left = max_left->_right; }

					swap(node->_data, max_left->_data);
					//递归删除
					_EraseR(node->_left, data);
				}
			}

			return true;
		}

		bool EraseR(const K& data)
		{
			return _EraseR(_root, data);
		}

	protected:
		BSTNode<K>* _root;
	};
}

namespace KV //Key/Value模型
{
	template<class K, class V>
	class BSTNode
	{
	public:
		BSTNode(const K& data = K(), const V& value = V()) :_data(data), _left(nullptr), _right(nullptr), _value(value) {}

		BSTNode<K, V>* _left;
		BSTNode<K, V>* _right;
		K _data;
		V _value;
	};

	template <class K, class V>
	class BSTree
	{
	public:
		BSTree() :_root(nullptr) {}

		bool Insert(const K& data, const V& value)
		{
			if (_root == nullptr)
			{
				_root = new BSTNode<K, V>(data, value);
			}

			BSTNode<K, V>* prev = nullptr;
			BSTNode<K, V>* cur = _root;
			while (cur)
			{
				prev = cur;
				if (data > cur->_data)
				{
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}

			if (data > prev->_data)
			{
				prev->_right = new BSTNode<K, V>(data, value);
			}
			else
			{
				prev->_left = new BSTNode<K, V>(data, value);
			}

			return true;
		}

		//中序遍历（排序）
		void InOrder()
		{
			_InOrder(_root); //这样写是为了方便调用，因为没法直接传_root
		}
		void _InOrder(BSTNode<K, V>* node)
		{
			if (node == nullptr)
			{
				return;
			}

			_InOrder(node->_left);
			cout << node->_data << ":" << node->_value << " ";
			_InOrder(node->_right);
		}

		BSTNode<K, V>* Find(const K& data)
		{
			BSTNode<K, V>* cur = _root;
			while (cur)
			{
				if (data > cur->_data)
				{
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}

			return cur; //本质就是nullptr
		}

		bool Erase(const K& data)
		{

			BSTNode<K, V>* cur = _root;
			BSTNode<K, V>* prev = nullptr;
			while (cur)
			{
				if (data > cur->_data)
				{
					prev = cur; //记录cur的前一个位置
					cur = cur->_right;
				}
				else if (data < cur->_data)
				{
					prev = cur; //记录cur的前一个位置
					cur = cur->_left;
				}
				else
				{
					break;
				}
			}

			if (!cur) //判断是否找到
			{
				return false;
			}

			//1.没有孩子的节点   2.只有一个孩子节点
			if (cur->_left == nullptr || cur->_right == nullptr)
			{
				if (cur == _root) //防止prev空指针的情况出现（删只有一边孩子的根节点）
				{
					if (cur->_left == nullptr)
					{
						_root = cur->_right;
						delete cur;
					}
					else
					{
						_root = cur->_left;
						delete cur;
					}
					return true;
				}

				if (prev->_left == cur) //确定cur在prev的哪一边
				{
					if (cur->_left == nullptr) //确定prev接管谁
					{
						prev->_left = cur->_right;
					}
					else
					{
						prev->_left = cur->_left;
					}

					delete cur;
				}
				else
				{
					if (cur->_left == nullptr) //确定prev接管谁
					{
						prev->_right = cur->_right;
					}
					else
					{
						prev->_right = cur->_left;
					}

					delete cur;
				}

				return true;
			}
			else //3.有两个孩子节点
			{
				//找左边的最大节点
				BSTNode<K, V>* temp = cur->_left;
				BSTNode<K, V>* tempprev = nullptr;
				while (temp->_right)
				{
					tempprev = temp;
					temp = temp->_right;
				}
				if (tempprev == nullptr) //说明temp就是最大节点，即cur是前一节点（或者把直接把tempprev初始化成cur）
				{
					tempprev = cur;
				}

				//替换法删除（可直接替换值，也可替换节点，这里替换值）
				cur->_data = temp->_data;
				cur->_value = temp->_value;

				if (tempprev->_left == temp)
				{
					tempprev->_left = nullptr;
				}
				else
				{
					tempprev->_right = nullptr;
				}
				delete temp;

				return true;
			}
		}

	protected:
		BSTNode<K, V>* _root;
	};
}
