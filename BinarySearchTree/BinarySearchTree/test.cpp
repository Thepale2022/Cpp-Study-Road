#include <iostream>
#include "binary_search_tree.h"
using namespace std;

int main()
{
	//Key测试
	{
		//bst1
		cout << "bst1:" << endl;
		K::BSTree<int> bst1;
		int a[] = { 5,3,4,1,7,8,2,6,0,9 };
		for (auto e : a)
		{
			bst1.Insert(e);
		}

		//BST天生具有排序+去重属性
		bst1.InOrder(); cout << endl;
		cout << bst1.Find(0)->_data << endl;


		//bst2
		cout << "bst2:" << endl;
		K::BSTree<int> bst2;
		for (auto e : a)
		{
			bst2.Insert(e);
		}

		bst2.Erase(6);
		bst2.Erase(8);
		bst2.InOrder(); cout << endl;


		//bst3
		cout << "bst3:" << endl;
		K::BSTree<int> bst3;
		for (auto e : a)
		{
			bst3.Insert(e);
		}

		for (auto e : a)
		{
			bst3.Erase(e);
			bst3.InOrder(); cout << endl;
		}

		//bst4
		cout << "bst4:" << endl;
		K::BSTree<int> bst4;
		for (auto e : a)
		{
			bst4.Insert(e);
		}
		K::BSTNode<int>* p = bst4.FindR(7);
		cout << p->_data << endl;


		//bst5
		cout << "bst5:" << endl;
		K::BSTree<int> bst5;
		for (auto e : a)
		{
			bst5.InsertR(e);
		}
		bst5.InOrder(); cout << endl;


		//bst6
		cout << "bst6:" << endl;
		K::BSTree<int> bst6;
		for (auto e : a)
		{
			bst6.InsertR(e);
		}
		bst6.InOrder(); cout << endl;

		//bst6.EraseR(5);
		//bst6.InOrder(); cout << endl;

		//bst6.EraseR(7);
		//bst6.InOrder(); cout << endl;

		//bst6.EraseR(8);
		//bst6.InOrder(); cout << endl;

		for (auto e : a)
		{
			bst6.Erase(e);
			bst6.InOrder(); cout << endl;
		}
	} 
	
	//Key/Value测试
	{
		//dir - 字典KV模型
		//KV::BSTree<string, string> dir;
		//dir.Insert("honey", "宝贝、蜂蜜");
		//dir.Insert("I", "我");
		//dir.Insert("love", "爱");
		//dir.Insert("you", "你");
		//dir.Insert("forever", "永远");

		//string str;
		//while (cin >> str)
		//{
		//	KV::BSTNode<string, string>* ret = dir.Find(str);
		//	if (ret != nullptr)
		//	{
		//		cout << "the word means:" << ret->_value << endl;
		//	}
		//	else
		//	{
		//		cout << "未找到此单词" << endl;
		//	}
		//}


		//count - 计数KV模型（字典模型Ctrl+C结束会导致程序异常，请勿一起测试）
		string arr[] = { "苹果", "梨子", "葡萄", "苹果", "苹果", "梨子", "苹果", "苹果" };
		
		KV::BSTree<string, int> CountTree;
		
		for (auto& str : arr)
		{
			auto ret = CountTree.Find(str);
			if (ret == nullptr)
			{
				CountTree.Insert(str, 1);
			}
			else
			{
				ret->_value++;
			}
		}

		CountTree.InOrder();
	}

	//set - key模型搜索树
	//map - key/value模型搜索树

	return 0;
}