#define _CRT_SECURE_NO_WARNINGS 1

//cout - 标准输出流 - c out
//cin - 标准输入流 - c in
//<< - 流插入运算符
//>> - 流提取运算符
//endl - 行结束 - 等价于 '\n' - end of line

#include <iostream> //Input/Output stream - 输入输出流
using namespace std; //std - C++库的实现定义在一个叫std的命名空间中	
//一般不这样写↑
//建议把常用的展开，例如：std::cout; std::cin;

int main()
{
	cout << "hello world" << endl; //看作字符串流入 cout
	//cout << "hello world\n;" - 等价
	//printf("hello world\n"); - 等价

	int a = 520;
	float b = 1.314;

	cout << a << b << endl; //可以直接输出，不需要%d %f，自动识别类型

	cin >> a >> b; //看作输入值流向 a 和 b
	cout << a << b << endl;

	return 0;
}