#include <iostream>
using namespace std;

//多态：必须是基类的指针或引用调用虚函数
//reson：只有基类的指针或引用可以既接收基类又接收派生类对象

//多态：虚函数和三同（函数名、参数、返回值）
//reson：语法规定

///////////////////////////////////////////////////////////////////////////////////////////////////
//C++11 final

//无法被继承的类
//私有构造间接限制 - 需要内部调用构造（C++98）
//class A
//{
//private:
//	A(int a = 0)
//		:_a(a)
//	{}
//
//public:
//	static A CreatObj(int a = 0) //必须加static，因为调用此函数必须构造对象，但此函数的作用就是构造对象
//	{
//		return A(a);
//	} 
//
//protected:
//	int _a;
//};
//
////直接限制 - 声明最终类（C++11）
//class B final
//{
//protected:
//	int _b;
//};
//
////限制函数重写
//class C
//{
//public:
//	virtual void Func() final
//	{
//		cout << "C::void Func()" << endl;
//	}
//};
//
//class D : public C
//{
//public:
//	virtual void Func()
//	{
//		cout << "D::void Func()" << endl;
//	}
//};
//
//int main()
//{
//	A a = A::CreatObj(); //间接创建方式
//	
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//C++11 override
//class A
//{
//public:
//	virtual void Func()
//	{
//		cout << "A::void Func()" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	virtual void Func() override //保证派生类中此函数必须重写基类的函数
//	{
//		cout << "B::void Func()" << endl;
//	}
//};
//
//int main()
//{
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//重载：
//1.两个函数在同一作用域
//2.参数不同

//重写/覆盖：
//1.两个函数分别在基类和派生类的作用域
//2.函数名、参数、返回值相同（协变例外）
//3.两个函数必须是虚函数

//重定义/隐藏：
//1.两个函数分别在基类和派生类的作用域
//2.函数名相同（但不构成重写）

//*如果重写不参与动态多态实现，实则重写就是一种特殊的重定义*
//详见下文，会在编译时直接确定地址，而不是去虚函数表里找


///////////////////////////////////////////////////////////////////////////////////////////////////
//抽象类 - 包含纯虚函数的类（接口类）
//class Car //抽象类不可实例化出对象
//{
//public:
//	virtual void Func1() = 0; //纯虚函数一般只声明不实现，实现无价值
//	void Func2()
//	{
//		cout << "void Func2()" << endl;
//	}
//};
//
//int main()
//{
//	Car* p = nullptr;
//	p->Func2(); //先理解，成员函数不放在类中，这里只是把p的值给了this指针
//	//但没有解引用，所以并没有引发崩溃
//
//	return 0;
//}

//class Car
//{
//public:
//	virtual void Func() = 0;
//};
//
//class Benz : public Car 
//{
//	virtual void Func() //对纯虚函数进行重写即可调用
//	{//就算重新了也只能调用到派生类的，所以实现基类的Func无意义
//		cout << "Benz!" << endl; //（只能调到派生类的Func是因为重写是一种特殊的隐藏）
//	}
//};
//
//int main()
//{
//	//new Benz; //error - 继承纯虚函数也不能被实例化
//	new Benz;
//	return 0;
//}

//**抽象类强制子类去完成虚函数重写，而override只是语法上检查**
//**抽象类体现了接口继承**	

//class Base
//{
//public:
//	virtual void Func1() {}
//	virtual void Func2() {}
//	virtual void Func3() {}
//
//protected:
//	int _a;
//	char _ch;
//};
//
//int main()
//{
//	Base b; //会存一个虚函数表地址
//	cout << sizeof(Base) << endl; //最终结果是 8 + 4 + 1 + 3(补齐)
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//class Person
//{
//public:
//	virtual void func()
//	{
//		cout << "无折扣" << endl;
//	}
//
//protected:
//	int _p = 0;
//};
//
//class Student : public Person
//{
//public:
//	virtual void func()
//	{
//		cout << "八折优惠" << endl;
//	}
//
//protected:
//	int _s = 0;
//};
//
//void Function(Person obj)
//{
//	obj.func();
//}
//
//int main()
//{
//	Person Jones;
//	Function(Jones);
//
//	Student Thepale;
//	Function(Thepale);
//
//	//***调试发现：Jones和Thepale的虚函数表是不一样的***
//	
//	//***调用逻辑：都是调用Function()，但传父类对象就去父类对象的虚函数表里找到虚函数调用
//	//而传子类会切片，切片后相当于对父类进行赋值，子类对象的虚函数表和父类对象不同***
//	
//	//**子类继承父类重写虚函数，即在虚函数表中重写此虚函数地址，导致子类对象和父类对象的虚表地址不一***
//	//简而言之：虚函数重写导致虚表不一样，导致最后调用到的函数不一样
//
//
//	//***对象切片不会传虚表***：
//	Person& ref = Thepale;
//	Person obj = Thepale;
//	//如果对象可以拷贝虚表，那么虚表控制不单一，就好似同一块开辟的内存被两个指针管理
//	//以此诠释必须使用引用或指针保证关系稳定
//
//	//***同类型的对象虚表指针是一样的，调用的函数都是同一虚表中的***
//	//换言之，虚表只能给自己人用，切片就是给其他人了
//	Student s1;
//	Student s2;
//
//	//**普通函数和虚函数都储存在公共代码段，只是虚函数要把地址存一份到虚表中，方便实现多态**
//
//	return 0;
//}

//*不是多态时，调用函数地址是编译时确定的；是多态时，调用函数的地址是运行时确定的*
//*只有符合多态的两个条件时，才会去虚函数表里找函数调用，不符合多态会直接确定地址*

//对于覆盖的理解：
//覆盖是重写的原理层面的展示
//**可认为当子类对象构造时，会拷贝父类虚表，如果子类有虚函数对父类进行了重写，则覆盖掉父类的
//其余的未重写部分是父类代码，所以称之为覆盖**

//（加上virtual的函数都会置于虚函数表中）

//**VS虚函数表储存在常量区（代码段） - x86 **
//class TEST
//{
//public:
//	virtual void func1() {}
//	void func2() {}
//};
//
//int main()
//{
//	int a = 0;
//	printf("栈：%p\n", &a);
//
//	int* p = new int;
//	printf("堆：%p\n", p);
//
//	const char* str = "Thepale";
//	printf("常量区：%p\n", str);
//
//	printf("代码段：%p\n", &TEST::func2);
//
//	TEST t;
//	printf("虚函数表:%p\n", *((int*)&t));
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
////虚表的单继承情况：
//class Parent
//{
//public:
//	virtual void Func1() {}
//	virtual void Func2() {}
//};
//
//class Son : public Parent
//{
//public:
//	virtual void Func1() {}
//	virtual void Func3() {}
//};
//
////虚表的多继承情况：
//class Father
//{
//public:
//	virtual void Func1() {}
//	virtual void Func2() {}
//};
//
//class Mother
//{
//public:
//	virtual void Func1() {}
//	virtual void Func2() {}
//};
//
//class Daughter : public Father, public Mother
//{
//public:
//	virtual void Func1() {}
//	virtual void Func3() {}
//};
//
//int main()
//{
//	//单继承：
//	//通过内存查看（监视窗口是编译器优化过的，看不到细节）
//	//Func1被重写，Func2不变，Func3也在虚表中，但监视看不到，需要通过内存查看
// 	Parent p;
//	Son s;
//
//	//多继承：
//	//通过内存查看，派生类中有两个虚表，Func1都被重写，Func2具有二义性
//	//Func3是放在第一个虚表中的
//	//虽然监视窗口重写Func1后两个虚表对应地址不一样，但跳转后仍然是一致的
//	//（Func2具有二义性是因为构成隐藏、重写、多态需要的是父类和子类，而不是同级类）
//	Father f;
//	Mother m;
//	Daughter d;
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//构成多态 - 运行时决议 - 运行时决定调用谁（动态绑定）
//不构成多态 - 编译时决议 - 编译时决定调用谁（静态绑定）

//*虚函数表是在构造函数时初始化的*
//class Base1
//{
//public:
//	Base1(int a = 0)
//	{
//		_a = a;
//	}
//
//	virtual void Func1() {} //**注意：没有虚函数的类不会有虚函数表**
//	virtual void Func2() {}
//
//protected:
//	int _a;
//};
//
//class Base2
//{
//public:
//	Base2(int a = 0)
//	{
//		_a = a;
//	}
//
//	virtual void Func3() {} //**注意：没有虚函数的类不会有虚函数表**
//	virtual void Func4() {}
//
//protected:
//	int _a;
//};
//
//class Derive : public Base1, public Base2
//{
//public:
//	virtual void Func1() {}
//	virtual void Func3() {}
//};
//
//template <class T>
//void PrintVFT(T* ptr) //打印前建议清理解决方案，VS会有BUG有时不加上0x00000000
//{
//	int* pftptr = (int*)*((int*)ptr); //获得虚表指针（本质是函数指针数组）- x86
//
//	size_t i = 0;
//	while (pftptr[i])
//	{
//		printf("%p\n", pftptr[i]);
//		i++;
//	}
//}
//
////整体建议用函数指针的方式实现一份 - C语言内容
////对于x86和x64的兼容，可以使用条件编译
//
////记得清理解决方案
//int main()
//{
//	Base1 b1;
//	PrintVFT(&b1);
//
//	cout << endl;
//
//	Base2 b2;
//	PrintVFT(&b2);
//
//	cout << endl;
//
//	Derive d;	
//	PrintVFT(&d);
//	PrintVFT((Derive*)((char*)&d + sizeof(Base1)));
//
//	cout << endl;
//
//	//切片可以导致指针偏移
//	Base2 b = d;
//	PrintVFT(&b);
//
//	cout << endl;
//
//	//对于void**的解释
//	//转换成Type类型的指针
//	typedef int* Type;
//	int t = 1;
//	cout << (Type)*((void**)&t) << endl; //转成int*类型
//
//	//void**是可以被解引用的，因为只是获取地址而已
//	//但void*不能解引用和运算，因为不知道读取内存的大小
//	int test = 1;
//	int* p1 = &test;
//	int** p2 = &p1;
//	void** pv1 = (void**)p2;
//
//	cout << p1 << endl;
//	cout << *pv1 << endl;
//	
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//可供研究：菱形虚继承的虚基表	
