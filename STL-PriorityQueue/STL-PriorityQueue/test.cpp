#include <iostream>
#include <queue> //priority_queue 是容器适配器
#include <functional>

using namespace std;

int main()
{
	//pq1
	priority_queue<int> pq1;
	pq1.push(1);
	pq1.push(7);
	pq1.push(9);
	pq1.push(5);
	pq1.push(3);

	//默认是大的优先级高
	while (!pq1.empty())
	{
		cout << pq1.top() << " "; //取堆顶数据（优先级最高的数据）
		pq1.pop();
	}
	cout << endl;


	//pq2
	priority_queue<int, vector<int>, greater<int>> pq2; //仿函数
	//默认大的优先级高仿函数是 less，让小的优先级高要传 great
	pq2.push(1);
	pq2.push(7);
	pq2.push(9);
	pq2.push(5);
	pq2.push(3);

	while (!pq2.empty())
	{
		cout << pq2.top() << " ";
		pq2.pop();
	}
	cout << endl;

	return 0;
}