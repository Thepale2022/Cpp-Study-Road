#include "Vector.h"

int main()
{
	//v1
	Thepale::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	v1.push_back(6);

	for (size_t i = 0; i < v1.size(); i++)
	{
		cout << v1[i] << " ";
	}
	cout << endl;

	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	v1.reserve(100);
	cout << "capacity:" << v1.capacity() << " " << "size:" << v1.size() << endl;
	v1.resize(666);
	cout << "capacity:" << v1.capacity() << " " << "size:" << v1.size() << endl;
	v1.resize(2);
	cout << "capacity:" << v1.capacity() << " " << "size:" << v1.size() << endl;

	//v2
	Thepale::vector<int> v2;
	v2.push_back(1);
	v2.push_back(2); 
	v2.push_back(3);
	v2.push_back(4);
	
	Thepale::vector<int>::iterator it = find(v2.begin(), v2.end(), 3);
	v2.insert(it, 666); //此时在insert处理迭代器失效问题（注意此处是传值，修正后it还是失效的）
	//迭代器失效本质是野指针（由于reserve扩容导致）

	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;

	//v3
	Thepale::vector<int> v3(v2);
	for (auto e : v3)
	{
		cout << e << " ";
	}
	cout << endl;

	v3 = v1;
	for (auto e : v3)
	{
		cout << e << " ";
	}
	cout << endl;

	//v4
	Thepale::vector<int> v4;
	v4.push_back(1);
	v4.push_back(2);
	v4.push_back(3);
	v4.push_back(4);
	v4.push_back(5);

	//迭代器遍历删除偶数的情况erase会出现迭代器失效问题
	//（VS对erase进行了强制检查，三种情况：1.1 2 3 4 5，2.1 2 3 4，3.1 2 4 5 都会断言报错）
	Thepale::vector<int>::iterator iter = v4.begin();
	while (iter != v4.end())
	{
		if (*iter % 2 == 0)
		{
			iter = v4.erase(iter); //it 重新赋值，防止迭代器失效
		}
		iter++;
	}

	for (auto e : v4)
	{
		cout << e << " ";
	}
	cout << endl;

	//v5
	Thepale::vector<string> v5;
	v5.push_back("11111111");
	v5.push_back("11111111");
	v5.push_back("11111111");
	v5.push_back("11111111111111111111111111");
	v5.push_back("11111111111111111111111111"); //长串乱码问题是因为VS做了优化，多的空间存在了堆上

	//此处处理memcpy浅拷贝问题：
	//如果是vector<string>类型，扩容对于string的拷贝是浅拷贝
	for (auto& e : v5)
	{
		cout << e << " ";
	}
	cout << endl;

	return 0;
}
