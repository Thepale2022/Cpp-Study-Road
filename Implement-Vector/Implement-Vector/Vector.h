#pragma once

#include <iostream>
#include <assert.h>
#include <algorithm>
#include <string>

using std::cout;
using std::endl;
using std::swap;
using std::find;
using std::string;


namespace Thepale
{
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		
		
		//构造
		vector()
				:_start(nullptr)
				,_finish(nullptr)
				,_endofstorage(nullptr)
			{}

		template<class InputIterator>
		vector(InputIterator first, InputIterator last) // - 迭代器区间初始化
			: _start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		//vector(const vector<T>& v) - 传统写法
		//{
		//	_start = new T[v.capacity()];
		//	_finish = _start + v.size();
		//	_endofstorage = _start + v.capacity();

		//	memcpy(_start, v._start, sizeof(T) * v.size());
		//}

		vector(const vector<T>& v) // - 现代写法
			: _start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			vector<T> temp(v.cbegin(), v.cend()); //用const接收调用的是const的迭代器，所以用cbegin和cend
			swap(_start, temp._start);
			swap(_finish, temp._finish);
			swap(_endofstorage, temp._endofstorage);
		}

		~vector()
		{
			delete[] _start;
			_start = nullptr;
			_finish = nullptr;
			_endofstorage = nullptr;
		}

		//成员函数
		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t size_data = size();
				T* temp = new T[n];

				//memcpy(temp, _start, sizeof(T) * size()); - 存在浅拷贝问题
				for (size_t i = 0; i < size(); i++)
				{
					temp[i] = _start[i]; //赋值带深拷贝
				}

				delete[] _start;

				_start = temp;
				_finish = size_data + _start;
				_endofstorage = n + _start;
			}
		}

		void resize(size_t n, const T& data = T()) //**缺省值重点理解
		{
			if (n < size())
			{
				_finish = _start + n;
			}
			else
			{
				if (n > capacity())
				{
					reserve(n);
				}

				while (_finish != _start + n)
				{
					*_finish = data;
					_finish++;
				}
			}
		}

		iterator insert(iterator pos, const T& data)
		{ //可复用至push_back，但为了展现过程，暂时不复用
			assert(pos >= _start && pos <= _finish);

			if (_finish == _endofstorage)
			{
				size_t gap = pos - _start; //防止迭代器失效
				reserve(capacity() == 0 ? 4 : capacity() * 2);
				pos = _start + gap;
			}

			for (size_t i = 0; i < _finish - pos; i++) //正常情况
			{
				_start[size() - i] = _start[size() - i - 1];
			}
				
			*pos = data;
			_finish++;

			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);

			for (size_t i = 0; i < _finish - pos - 1; i++)
			{
				*(pos + i) = *(pos + i + 1);
			}

			_finish--;

			return pos;
		}

		void push_back(const T& data)
		{
			if (_finish == _endofstorage)
			{
				//扩容
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}

			*_finish = data;
			_finish++;
		}

		void pop_back()
		{
			assert(_start != _finish);
			_finish--;
		}


		//运算符重载
		T& operator[] (size_t pos)
		{
			return *(_start + pos);
		}

		const T& operator[] (size_t pos) const
		{
			return *(_start + pos);
		}

		//vector<T>& operator= (const vector<T>& v) - 传统写法
		//{
		//	T* temp = new T[v.capacity()];
		//	delete[] _start;
		//	_start = temp;
		//	memcpy(_start, v._start, v.size() * sizeof(T));

		//	_finish = _start + v.size();
		//	_endofstorage = _start + v.capacity();

		//	return *this;
		//}

		vector<T>& operator= (const vector<T>& v) // - 现代写法
		{
			vector<T> temp(v); //或省略此行将定义写成 vector<T>& operator= (const vector<T> v) 然后和v交换即可
			swap(_start, temp._start);
			swap(_finish, temp._finish);
			swap(_endofstorage, temp._endofstorage);

			return *this;
		}

		//迭代器
		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator cbegin() const
		{
			return _start;
		}

		const_iterator cend() const
		{
			return _finish;
		}
			

	private:
		iterator _start; //指向数据头
		iterator _finish; //指向数据尾的下一个位置
		iterator _endofstorage; //指向容量尾的下一个位置
	};
}