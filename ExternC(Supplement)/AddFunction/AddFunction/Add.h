#pragma once

//这里 extern 只能在 C++ 中使用
//这样也能够实现用 C 语言的函数修饰方式
//**不允许重载了

//这里的条件编译是保证在 C++ 环境下使用 extern "C"
//在 C 语言环境下不使用
//**因为在 .c 文件中引用 Add.h 会不支持 extern "C" 的使用
#ifdef __cplusplus
extern "C"
{
#endif

	int Add(int x, int y);

#ifdef __cplusplus
}
#endif
