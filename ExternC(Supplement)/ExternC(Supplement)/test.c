#define _CRT_SECURE_NO_WARNINGS 1

//此为 C 中使用 C++ 函数的用法
//前步骤一致，主要关注 extern 用法

#include <stdio.h>
#include "../AddFunction/AddFunction/Add.h"

/*
int main()
{
	int ret = Add(1, 2);
	printf("%d", ret);

	return 0;
}
*/

//**其本质都是改变 *.o 中符号表的函数名形式，即改变了函数名修饰规则