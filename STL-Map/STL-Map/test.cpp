#include <iostream>
using namespace std;

#include <map>
#include <string>

int main()
{
	//map1 - 构造插入方式、迭代器
	cout << "map1 - 构造插入方式、迭代器：" << endl;
	map<string, string> map1;

	pair<string, string> pr1("我", "I");
	map1.insert(pr1);

	map1.insert(pair<string, string>("爱", "love"));

	map1.insert(make_pair("你", "you")); //常用

	map<string, string>::iterator it1 = map1.begin();
	while (it1 != map1.end())
	{
		cout << it1->first << ":" << it1->second << endl; //实际上是map->pair->first，这里编译器优化成了一个箭头
		++it1;
	}
	cout << endl;

	for (auto& e : map1)
	{
		cout << e.first << ":" << e.second << endl;
	}
	cout << endl;


	//map2 - 计数与[]
	cout << "map2 - 计数与[]" << endl;
	map<string, int> map2;
	string arr[] = { "草莓", "葡萄", "橘子","葡萄", "橘子", "草莓", "葡萄", "草莓", "葡萄", "葡萄", "火龙果"};

	//for (auto& e : arr)
	//{
	//	auto ret = map2.find(e);

	//	if (ret == map2.end())
	//	{
	//		map2.insert(make_pair(e, 1)); //如果这个值没有，实际上查找了两次（一次find，一次insert）
	//	}
	//	else
	//	{
	//		ret->second++; //second可以改，first不能改
	//	}
	//}

	//for (auto& e : arr) //优化
	//{
	//	auto ret = map2.insert(make_pair(e, 1));
	//	if (ret.second == false) { ret.first->second++; }

	//	//***insert返回一个pair<iterator, bool>，如果没有相同值，则插入成功，iterator指向新插入元素***
	//	//***bool为true；如果有相同值，则插入失败，iterator指向与其相同的元素，bool为false***
	//}

	for (auto& e : arr)
	{
		map2[e]++;
		//等价于：
		//mapped_type& operator[] (const key_type& k)
		//{
		//	return (*((this->insert(make_pair(k, mapped_type()))).first)).second;
		//}

		//解析：
		//(*((pair).first)).second;
		//(*iterator).second;
		//返回[(<insert返回的pair>的迭代器)所指向的second]
	}

	for (auto& e : map2)
	{
		cout << e.first << ":" << e.second << endl;
	}
	cout << endl;

	
	//map3 - []的作用：查找、插入、修改（一般不单独查找，查找没有会插入）
	cout << "map3 - []的作用：查找、插入、修改（一般不单独查找，查找没有会插入）" << endl;
	map<string, string> map3;
	map3["永远"] = "forever"; //查找、插入
	map3["可爱"] = "cute";

	map3["引用"] = "referance";
	map3["引用"] = "reference"; //查找、修改

	//obj[Key] 返回的是 value 的引用

	for (auto& e : map3)
	{
		cout << e.first << ":" << e.second << endl;
	}


	//multimap1 - 插入、查找、删除特性
	cout << "multimap1 - 插入、查找、删除特性" << endl;
	multimap<string, string> multimap1;
	multimap1.insert(make_pair("pale", "苍白的"));
	multimap1.insert(make_pair("pale", "淡色的"));
	multimap1.insert(make_pair("pale", "暗淡的"));
	multimap1.insert(make_pair("pale", "微弱的"));
	multimap1.insert(make_pair("pale", "微弱的"));

	//*multimap不支持[]*

	for (auto& e : multimap1)
	{
		cout << e.first << ":" << e.second << endl;
	}
	cout << endl;

	cout << "multimap1中共有" << multimap1.count("pale") << "个pale" << endl; //计算pale个数

	auto retm1 = multimap1.find("pale");
	cout << retm1->first << ":" << retm1->second << endl; //返回的是中序第一个pale - 可画图理解

	multimap1.erase("pale"); //会删除所有的pale

	for (auto& e : multimap1)
	{
		cout << e.first << ":" << e.second << endl;
	}
	cout << endl;

	return 0;
}