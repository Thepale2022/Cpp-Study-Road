#include "queue.h"

int main()
{
	Thepale::queue<int, list<int>> qu1;
	qu1.push(1);
	qu1.push(2);
	qu1.push(3);
	qu1.push(4);
	qu1.push(5);

	while (!qu1.empty())
	{
		cout << qu1.front() << " ";
		qu1.pop();
	}
	cout << endl;

	return 0;
}