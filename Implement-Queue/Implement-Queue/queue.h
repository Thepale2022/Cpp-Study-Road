#pragma once

#include <iostream>
#include <vector>
#include <list>

using namespace std;

namespace Thepale
{
	template<class T, class Container>
	class queue
	{
	public:
		void push(const T& data)
		{
			_con.push_back(data);
		}

		void pop()
		{
			_con.pop_front();
		}

		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& front()
		{
			return _con.front();
		}

	private:
		Container _con;
	};
}
