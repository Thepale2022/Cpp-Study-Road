#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

/*
int& test1(int i)
{
	static int arr1[10] = { 0 };
	return arr1[i];
}

int test2(int i)
{
	static int arr2[10] = { 0 };
	return arr2[i];
}

int main()
{
	for (size_t i = 0; i < 10; i++)
	{
		test1(i) = i; //返回引用，本质上是 arr[i] 的别名，是可修改的左值，所以可以直接赋值
	}

	for (size_t i = 0; i < 10; i++)
	{
		cout << test1(i) << " ";
	}
	cout << endl;

	//test2(0) = 0; -- error
	//int 类型返回是以临时变量的形式返回，临时变量具有常属性
	//常属性的返回是右值，无法被修改

	return 0;
}
*/


/*
//地址也可以这样玩（差点紫砂）
int* test()
{
	static int a = 0;
	cout << a << endl;
	return &a;
}

int main()
{
	*test() = 1;
	test();
	
	return 0;
}
*/


/*
//权限深解
int main()
{
	double a = 5.21;

	int b = a; //可以赋值，且保留了整数部分；实际上是隐式类型转换
	//a(double)->tmp(int)->b(int) - a 的值先给了 int 类型的临时变量，然后再给了 b

	//int& c = a; - error
	//不可以引用，临时变量给 c 时存在权限放大
	//**需要注意的是，int b = a; 不存在权限放大，因为具有常属性的临时变量只是给值给 b
	//**类比于：int b = 5; 这是允许的
	//**但是 int& c = a; 是取别名，存在权限放大不可以取别名，因为 c 的改变本质是临时变量的改变
	//**而临时变量的常属性不允许其被改变，所以需要加上 const 使权限相等

	const int& d = a; //可以引用，因为临时变量具有常属性，d 的权限与其相同

	return 0;
}

//**const type& 通吃 - 可以接受各种类型的对象
*/