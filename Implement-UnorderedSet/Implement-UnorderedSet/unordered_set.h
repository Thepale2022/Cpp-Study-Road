#pragma once

#include <iostream>
using namespace std;

#include "hash_table-plus.h"

template <class T>
struct HashFunc_Default
{
	size_t operator() (const T& t)
	{
		return t;
	}
};

template <>
struct HashFunc_Default<string>
{
	size_t operator() (const string& str)
	{
		size_t ret = 0;

		for (auto& e : str)
		{
			ret += e * 31;
		}

		return ret;
	}
};

namespace Thepale
{
	template <class K, class HashFunc = HashFunc_Default<K>>
	class unordered_set
	{
	public:
		//仿函数
		struct Set_KeyOfT
		{
			const K& operator() (const K& key)
			{
				return key;
			}
		};

		typedef typename LinkHash::HashTable<K, K, Set_KeyOfT, HashFunc>::iterator iterator;

		//成员函数
		pair<iterator, bool> Insert(const K& key)
		{
			return _ht.Insert(key);
		}

		//迭代器
		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

	protected:
		LinkHash::HashTable<K, K, Set_KeyOfT, HashFunc> _ht;
	};
}