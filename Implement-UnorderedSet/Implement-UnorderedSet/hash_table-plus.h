#pragma once

#include <iostream>
using namespace std;

#include <vector>
#include <string>
#include <list>


namespace LinkHash
{
	template <class T>
	struct HashData
	{
		HashData(const T& data)
			:_data(data)
			,_next(nullptr)
		{}

		T _data;
		HashData<T>* _next;
	};

	//使用前提前声明
	template <class K, class T, class KeyOfT, class HashFunc>
	class HashTable;

	template <class K, class T, class Ref, class Ptr, class KeyOfT, class HashFunc>
	class HashTable_Iterator
	{
	public:
		typedef HashData<T> Node;
		typedef HashTable_Iterator<K, T, Ref, Ptr, KeyOfT, HashFunc> Self;
		KeyOfT kot;
		HashFunc hf;

		//构造
		HashTable_Iterator(Node* node, HashTable<K, T, KeyOfT, HashFunc>* pht)
			:_node(node)
			,_pht(pht)
		{}

		Ref operator* ()
		{
			return _node->_data;
		}

		Ptr operator-> ()
		{
			return &_node->_data;
		}

		Self& operator++ ()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				size_t index = hf(kot(_node->_data)) % _pht->_ht.size();
				++index;

				while (index < _pht->_ht.size())
				{
					if (_pht->_ht[index])
					{
						break;
					}
					else
					{
						++index;
					}
				}

				if (index == _pht->_ht.size())
				{
					_node = nullptr;
				}
				else
				{
					_node = _pht->_ht[index];
				}
			}

			return *this;
		}

		bool operator!= (const Self& s)
		{
			return _node != s._node;
		}
	private:
		Node* _node;
		HashTable<K, T, KeyOfT, HashFunc>* _pht;
	};


	template <class K, class T, class KeyOfT, class HashFunc>
	class HashTable
	{
		//定义友元让Iterator可以访问HashTable的私有
		template <class K, class T, class Ref, class Ptr, class KeyOfT, class HashFunc>
		friend class HashTable_Iterator;

	public:
		typedef HashTable_Iterator<K, T, T&, T*, KeyOfT, HashFunc> iterator;
		typedef HashData<T> Node;
		HashFunc hf;
		KeyOfT kot;

		//构造函数
		HashTable()
			:_n(0)
		{
			if (_ht.size() == 0)
			{
				_ht.resize(10, nullptr);
			}
		}

		HashTable(const HashTable<K, T, KeyOfT, HashFunc>& ht)
		{
			_ht.resize(ht._ht.size());

			for (size_t i = 0; i < ht._ht.size(); ++i)
			{
				Node* cur = ht._ht[i];
				while (cur)
				{
					Node* copy = new Node(cur->_data);
					copy->_next = _ht[i];
					_ht[i] = copy;

					cur = cur->_next;
				}
			}
		}

		~HashTable()
		{
			for (size_t i = 0; i < _ht.size(); ++i)
			{
				Node* del = _ht[i];
				Node* next = nullptr;
				while (del)
				{
					next = del->_next;
					delete del;
					del = next;
				}

				_ht[i] = nullptr;
			}
		}

		HashTable<K, T, KeyOfT, HashFunc>& operator= (HashTable<K, T, KeyOfT, HashFunc> ht)
		{
			swap(ht._ht, _ht);
			swap(ht._n, _n);
			return *this;
		}

		//成员函数
		iterator Find(const K& key)
		{
			size_t pos = hf(key) % _ht.size();

			Node* cur = _ht[pos];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return iterator(cur, this);
				}

				cur = cur->_next;
			}

			return iterator(nullptr, this);
		}

		pair<iterator, bool> Insert(const T& data)
		{
			auto ret = Find(kot(data));
			if (ret != end())
			{
				return make_pair(ret, false);
			}

			//扩容
			if (_ht.size() == _n)
			{
				size_t NewSize = _ht.size() * 2;
				vector<Node*> NewTable;
				NewTable.resize(NewSize);

				for (size_t i = 0; i < _ht.size(); ++i)
				{
					Node* cur = _ht[i];
					while (cur != nullptr)
					{
						Node* next = cur->_next;
						size_t pos = hf(kot(cur->_data)) % NewSize;
						
						//头插
						Node* temp = NewTable[pos];
						NewTable[pos] = cur;
						cur->_next = temp;

						cur = next;
					}
					_ht[i] = nullptr;
				}

				swap(_ht, NewTable);
			}


			//插入
			size_t pos = hf(kot(data)) % _ht.size();

			Node* temp = _ht[pos];
			_ht[pos] = new Node(data);
			_ht[pos]->_next = temp;

			_n++;

			return make_pair(iterator(_ht[pos], this), true);
		}

		bool Erase(const K& key)
		{
			if (_ht.empty())
			{
				return false;
			}

			size_t pos = hf(key) % _ht.size();

			Node* prev = nullptr;
			Node* cur = _ht[pos];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					//头删	
					if (prev == nullptr)
					{
						_ht[pos] = cur->_next;
					}
					//正常删除
					else
					{
						prev->_next = cur->_next;
					}

					--_n;
					delete cur;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}
			
			return false;
		}


		//迭代器
		iterator begin()
		{
			for (size_t i = 0; i < _ht.size(); ++i)
			{
				if (_ht[i])
				{
					return iterator(_ht[i], this);
				}
			}

			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

	protected:
		vector<Node*> _ht;
		size_t _n = 0;
	};
}


