#define _CRT_CECURE_NOWARNINGS 1

#include "unordered_set.h"

int main()
{
	//us1
	Thepale::unordered_set<int> us1;
	us1.Insert(4);
	us1.Insert(14);
	us1.Insert(5);
	us1.Insert(8);

	//hs2
	Thepale::unordered_set<string> us2;
	us2.Insert("ZC");
	us2.Insert("I");
	us2.Insert("LOVE");
	us2.Insert("YOU");

	Thepale::unordered_set<string>::iterator it2 = us2.begin();
	while (it2 != us2.end())
	{
		cout << *it2 << endl;
		++it2;
	}

	//hs3
	Thepale::unordered_set<string> us3(us2);
	for (auto e : us3)
	{
		cout << e << endl;
	}

	return 0;
}