#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <assert.h>
#include <fstream>
#include <sstream>
using namespace std;

class A
{
public:
	operator bool() //operator [typename] - 将对象转换为 [typename] 类型（隐式类型转换）
	{
		return _a != 0; //返回值类型对应 [typename]
	}

	int _a;
};

struct ServerInfo
{
	char _ip[32];
	int _port;
};

class ConfigManager
{
public:
	ConfigManager(const char* filename)
		:_filename(filename)
	{}

	void Write_Bin(ServerInfo& info)
	{
		ofstream ofs(_filename.c_str(), ios_base::out | ios_base::binary); //按位或可以把其或在一起
		ofs.write((const char*)&info, sizeof(info)); //无需显式调用close，析构自动调用
	}

	void Write_Text(ServerInfo& info)
	{
		ofstream ofs(_filename.c_str(), ios_base::out);
		ofs << info._ip << " " << info._port; //为了读取，必须加空格
	}

	void Read_Bin(ServerInfo& info)
	{
		ifstream ifs(_filename.c_str(), ios_base::in | ios_base::binary);
		ifs.read((char*)&info, sizeof(info));
	}

	void Read_Text(ServerInfo& info)
	{
		ifstream ifs(_filename.c_str(), ios_base::in);
		ifs >> info._ip >> info._port;
	}

private:
	string _filename;
};

void Bool_Object1()
{
	//*cin >> str 本质是 cin.operator(str) 返回值就是 cin，其类型为 istream
	//istream 能做逻辑判断本质是对 bool 的重载
	string str;
	while (cin >> str)
	{
		cout << str << endl;
	}
}

void Bool_Object2()
{
	//直接用对象做逻辑判断
	//实际上被转换为 while(a.operator()) - 见上
	A a;
	while (a)
	{
		cin >> a._a;
		cout << a._a;
	}
}

void File_Write_C()
{
	FILE* fin = fopen("test1.data", "w");
	assert(fin);

	ServerInfo info = { "127.0.0.1", 80 };
	fprintf(fin, "%s %d", info._ip, info._port); //为了读取，必须加空格
	fclose(fin);

	printf("File_Write_C:%s:%d\n", info._ip, info._port);
}

void File_BWrite_C()
{
	FILE* fin = fopen("test2.data", "wb");
	assert(fin);

	ServerInfo info = { "127.0.0.1", 80 };
	fwrite(&info, sizeof(info), 1, fin);
	fclose(fin);

	printf("File_BWrite_C:%s:%d\n", info._ip, info._port);
}

void File_Read_C()
{
	FILE* fout = fopen("test1.data", "r");
	assert(fout);

	ServerInfo info;
	fscanf(fout, "%s %d", info._ip, &info._port);
	fclose(fout);

	printf("File_Read_C:%s:%d\n", info._ip, info._port);
}

void File_BRead_C()
{
	FILE* fout = fopen("test2.data", "rb");
	assert(fout);

	ServerInfo info;
	fread(&info, sizeof(info), 1, fout);

	printf("File_BRead_C:%s:%d\n", info._ip, info._port);
}

int main()
{
	//逻辑判断为何可以用 istream ?
	//Bool_Object1();

	//Bool_Object2();


	//文件读写 - C
	File_Write_C();
	File_BWrite_C();
	File_Read_C();
	File_BRead_C();

	//文件读写 - C++
	ServerInfo info1 = { "127.0.0.1", 80 };
	ConfigManager cm1("test3.data");
	cm1.Write_Bin(info1);
	cout << "cm1.Write_Bin(info2):" << info1._ip << ":" << info1._port << endl;

	ServerInfo info2;
	cm1.Read_Bin(info2);
	cout <<"cm1.Read_Bin(info2):" << info2._ip << ":" << info2._port << endl;


	ServerInfo info3 = { "127.0.0.1", 80 };
	ConfigManager cm2("test4.data");
	cm2.Write_Text(info3);
	cout << "cm2.Write_Text(info3):" << info3._ip << ":" << info3._port << endl;

	ServerInfo info4;
	cm2.Read_Text(info4);
	cout << "cm2.Read_Text(info4):" << info4._ip << ":" << info4._port << endl;
	//*对于自定义类型可重载 ofstream& operator<< (ofstream& ofs, [TYPE]& [name]);*


	//C++ 数据转字符串
	//序列化
	ServerInfo info = { "666.666.666.666", 65535 };
	ostringstream oss;
	oss << info._ip << " " << info._port;

	string total = oss.str();
	cout << total << endl;

	//反序列化
	istringstream iss(total);
	char str[32];
	int port;
	iss >> str >> port;

	cout << str << " " << port << endl;

	return 0;
}