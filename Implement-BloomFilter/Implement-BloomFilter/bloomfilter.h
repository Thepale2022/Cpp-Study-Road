#pragma once

#include <iostream>
using namespace std;

#include <bitset>
#include <string>

namespace StringHash
{
    // BKDR Hash Function
    struct BKDRHash
    {
        size_t operator() (const string& str)
        {
            size_t seed = 131; // 31 131 1313 13131 131313 etc..
            size_t hash = 0;

            for (auto& e : str)
            {
                hash = hash * seed + e;
            }

            return (hash & 0x7FFFFFFF);
        }
    };

    // AP Hash Function
    struct APHash
    {
        size_t operator() (const string& str)
        {
            size_t hash = 0;
            int i = 0;

            for (auto& e : str)
            {
                if ((i & 1) == 0)
                {
                    hash ^= ((hash << 7) ^ e ^ (hash >> 3));
                }
                else
                {
                    hash ^= (~((hash << 11) ^ e ^ (hash >> 5)));
                }

                i++;
            }

            return (hash & 0x7FFFFFFF);
        }
    };


    // DJB Hash Function
    struct DJBHash
    {
        size_t operator() (const string& str)
        {
            size_t hash = 5381;

            for (auto& e : str)
            {
                hash += (hash << 5) + e;
            }

            return (hash & 0x7FFFFFFF);
        }
    };
}

namespace Thepale
{
	template <size_t N, 
        class HashFunc1 = StringHash::BKDRHash, 
        class HashFunc2 = StringHash::APHash, 
        class HashFunc3 = StringHash::DJBHash, 
        class K = string>
	class BloomFilter
	{ 
	public:
		void set(const K& key)
		{
            size_t pos1 = HashFunc1()(key) % N * 4;
            size_t pos2 = HashFunc2()(key) % N * 4;
            size_t pos3 = HashFunc3()(key) % N * 4;

            _bs.set(pos1);
            _bs.set(pos2);
            _bs.set(pos3);

		}

        //无法实现删除 - 可能导致误判
        //引入计数可以一定成都实现

	protected:
		bitset<N * 4> _bs; //k = m / n * ln2 --- k:函数个数   m:布隆过滤器长度   n:数据个数
	};
}