#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//extern "C"{} - 在 C++ 中使用 C 函数

//引用库：1.包含头文件   2.右键项目->属性->链接器->常规->附加库目录（添加*.lib）
//3.右键项目->属性->链接器->输入->附加依赖项（添加*.lib）
//#include "../AddFunction/AddFunction/Add.h" //相对路径 - 包含 AddFunction 的头文件


/*
int main()
{
	int ret = 0;

	ret = Add(1, 2); 
	// (?Add@@YAHHH@Z) - C编译的库，用C++的函数名寻找方式是找不到的
	//故这里出现链接错误（函数名修饰规则不一样）
	//如果把 lib 改为 cpp 重新编译则可以正常调用

	cout << ret << endl;

	return 0;
}
*/


/*
extern "C" // - 告诉编译器以 C 的函数修饰方式寻址
{
	#include "../AddFunction/AddFunction/Add.h"
}

int main()
{
	cout << Add(1, 2) << endl;

	return 0;
}
*/