#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#include <vector>
#include <list>

///////////////////////////////////////////////////////////////////////////////////////////////////
void L_R_Value_Reference()
{
	//左值与左值引用
	//这里的 a pa fa 都是左值 - 左值可以被取地址和赋值（const除外） - 可以出现在等号左边
	int a = 1;
	int* pa = &a;
	int& ra = a; //左值引用

	const int ca = 10;
	//ca = 20; - 非法

	//这里要注意：左值引用的不仅仅是一个变量或对象
	int& r = *pa; //这里的引用的是一个表达式


	//右值与右值引用
	//右值 - 字面常量、表达式返回值、传值返回的非左值引用返回值等
	//右值不能被取地址 - 只能出现在赋值符号的右边
	10;
	5 * 2 * 0;

	//10 = a; - 非法

	//右值引用
	int&& rr1 = 10;
	int&& rr2 = 10 + 20;


	//**左值引用不能引用右值（但const左值引用可以引用右值）**
	//int& rb = 10; - 非法
	const int& crb = 10;

	//**右值引用不能引用左值（但右值引用可以引用move后的左值）**
	//int&& rr3 = a; - 非法
	int&& rr3 = move(a);

	//右值起别名后可以取地址或修改（const除外）
	//即常量、表达式返回值等，都是一个临时变量，起别名（即右值引用）导致其储存到了特定位置
	//可以认为是右值到左值的转换
	int&& rri = 10;
	int* prri = &rri;
	rri = 20;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Thepale
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}

		// 拷贝构造
		string(const string& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(const string& s) -- 深拷贝" << endl;

			string tmp(s._str);
			swap(tmp);
		}

		 //移动构造
		string(string&& s) noexcept
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) -- 资源转移" << endl;

			this->swap(s);
		}

		 //移动赋值
		string& operator=(string&& s) noexcept
		{
			cout << "string& operator=(string&& s) -- 转移资源" << endl;
			swap(s);

			return *this;
		}

		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 深拷贝" << endl;
			string tmp(s);
			swap(tmp);

			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string operator+(char ch)
		{
			string tmp(*this);
			push_back(ch);

			return tmp;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity; // 不包含最后做标识的\0
	};

	Thepale::string to_string(int value)
	{
		Thepale::string str;
		while (value)
		{
			int val = value % 10;
			str += ('0' + val);
			value /= 10;
		}
		reverse(str.begin(), str.end());

		return str;
	}
}

Thepale::string Func1()
{
	Thepale::string str1("Thepale2022");
	return str1; //这里属于函数返回值，被系统解读为将亡值 - 直接调用移动构造，免去深拷贝
	
	//都创建了新对象，但是左值引用是拷贝原对象的所有数据再销毁原对象
	//右值引用是新对象直接交换将要销毁的原对象的数据，再销毁原对象，减少了拷贝消耗
}

void Move_Construction()
{
	//左值引用无法完美处理的构造
	Func1(); //右值分为纯右值和将亡值，无移动构造时，将会进行深拷贝

	//Case1:
	cout << Thepale::to_string(5201314).c_str() << endl;
	//main开辟其栈帧，会在其为临时变量开辟好空间，调用 to_string 时，是在 to_string 的函数栈帧还没销毁时
	//（即在 return str; 时），会将 to_string 函数栈帧内的 str 拷贝构造给 main 函数栈帧内的临时变量
	//无移动构造时（编译器无法优化），实现上述代码需要的步骤是：拷贝构造给临时变量（深拷贝），再用临时变量调用 c_str()
	//有移动构造，有编译器优化时，编译器直接将 str 将亡左值识别成右值，直接用 str 对临时对象进行移动构造

	//Case2:
	Thepale::string ret = Thepale::to_string(5201314);
	//同理，to_string 在销毁时会拷贝构造给 main 栈帧的临时对象，临时对象作为 to_string 函数调用的返回值
	//在进行赋值操作给 ret 接收

	//无移动构造，无编译器优化时，实现上述代码需要的步骤是：1.拷贝构造给临时对象（深拷贝）   2.赋值给 ret（深拷贝）
	//（具体为：str 拷贝给临时对象，销毁 str，临时对象拷贝给 ret，销毁临时对象）
	//有移动构造，无编译器优化时，会将 str（左值）拷贝给临时对象（右值），用临时对象移动构造来初始化 ret
	//无移动构造，有编译器优化时，编译器会优化成 str 直接对 ret 进行赋值，省去了略显累赘的临时对象
	//有移动构造，有编译器优化时，编译器直接将 str 将亡左值识别成右值，直接用 str 对 ret 进行移动构造
}

void Move_Assignment()
{
	Thepale::string str1("Thpale2022");
	Thepale::string str2("I love you");

	str2 = str1; //因为是 左值 = 左值，所以调用的是拷贝赋值 - 深拷贝
	str2 = move(str1); //变为 左值 = 右值，所以调用的是移动赋值 - 资源转移

	//Thepale::string str3 = Thepale::to_string(5201314); - 这样写无法检验移动赋值
	Thepale::string str4;
	str4 = Thepale::to_string(5201314); //如果没有移动赋值就是移动构造 + 深拷贝，有移动赋值就是移动构造 + 移动赋值
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void R_Value_Reference_Container()
{
	//这里特意采用了list，因为list的实现是简单的赋值，便于理解，vector有待探讨
	//*请注意，调用list的左值和右值引用版本其实结果无差，还是取决于string有没有移动构造，因为就算list想要移动构造它的
	//成员，就要调用成员的移动构造，如果没有只能调用拷贝构造*

	list<Thepale::string> v;
	//所有容器的插入都有一个右值引用版本

	//Case1:
	Thepale::string str("Thepale2022");
	v.push_back(str);
	cout << endl;
	//string 无右值引用：深拷贝 - str 是左值，正常拷贝于 list 中（调用 list 的左值引用）
	//string 有右值引用：深拷贝 - str 是左值，正常拷贝于 list 中（调用 list 的左值引用）

	//Case2:
	v.push_back("5201314"); //***隐式转换构造的对象和临时对象都属于右值***
	cout << endl;
	//string 无右值引用：深拷贝 - 隐式构造右值对象后赋值给链表元素调用 string 的拷贝构造（调用 list 的右值引用）
	//string 有右值引用：移动构造 - 隐式构造右值对象后赋值给链表元素调用 string 的移动构造（调用 list 的右值引用）

	//Case3:
	v.push_back(Thepale::to_string(5201314));
	cout << endl;
	//string 无右值引用：深拷贝 + 深拷贝 - to_string 深拷贝 str 给 main 内的临时对象 + 临时对象赋值给链表元素调用
	//string 的拷贝构造（调用 list 的右值引用）
	//string 有右值引用：移动构造 + 移动构造 - to_string 移动构造 str 给 main 内的临时对象 + 临时对象赋值给链表元素
	//调用 string 的移动构造（调用 list 的右值引用）

	//Case4:
	v.push_back(move(str));
	//*如果将 str move 会将其转换为右值，调用 list 的右值引用插入，此操作会导致 str 的资源被转移，str 将无数据*
	//string 无右值引用：深拷贝 - str 是右值，调用 list 的右值引用插入，但赋值操作 string 没有可匹配的移动构造版本
	//故还是调用了拷贝构造
	//string 有右值引用：资源转移 - str 是右值，调用 list 的右值引用插入，赋值操作 string 存在移动构造，则调用
	//stirng 的移动构造对 list 元素赋值，为资源转移
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void Func1(int& a) { cout << "void Func1(int& a) - 左值引用" << endl; }
void Func1(int&& a) { cout << "void Func1(int&& a) - 右值引用" << endl; }
void Func1(const int& a) { cout << "void Func1(const int& a) - const左值引用" << endl; }
void Func1(const int&& a) { cout << "void Func1(const int&& a) - const右值引用" << endl; }


template<class T>
void Func(T&& t)
{
	Func1(t);
}

template<class T>
void PF_Func(T&& t)
{
	Func1(forward<T>(t));
}


void Universal_Reference_And_Perfect_Forward() 
{
	int a = 520;
	int const cla = 520;
	Func(a); //左值
	Func(520); //右值
	Func(cla); //const左值
	Func(move(a)); //const右值
	cout << endl;
	//会发现参数只是决定了调用，但实际使用会由右值退化成左值（const属性不变）

	int pfa = 520;
	PF_Func(pfa); //左值
	PF_Func(520); //右值
	PF_Func(cla); //const左值
	PF_Func(move(pfa)); //const右值
	//完美转发可以解决这一问题，即保留属性
}

int main()
{
	//左值引用与右值引用
	//L_R_Value_Reference();

	//移动构造
	//Move_Construction();

	//移动赋值
	//Move_Assignment();

	//右值引用于容器插入
	//R_Value_Reference_Container();

	//万能引用与完美转发
	Universal_Reference_And_Perfect_Forward();
	return 0;
} 