#include "priority_queue.h"

int main()
{
	//pq1
	Thepale::priority_queue<int> pq1;
	pq1.push(9);
	pq1.push(3);
	pq1.push(2);
	pq1.push(1);
	pq1.push(5);
	pq1.push(8);
	pq1.push(4);
	pq1.push(6);
	pq1.push(7);
	
	pq1.pop();

	//A是仿函数类型，a是函数对象（像函数一样使用的对象）
	Thepale::A<int> a; //a不是函数名也不是函数指针
	cout << a(5, 10) << endl; //而是被重载成了 a.operator()(5, 10);


	//pq2
	Thepale::priority_queue<int, vector<int>, Thepale::Greater<int>> pq2; //仿函数构建小堆
	pq2.push(9);
	pq2.push(3);
	pq2.push(2);
	pq2.push(1);
	pq2.push(5);
	pq2.push(8);
	pq2.push(4);
	pq2.push(6);
	pq2.push(7);

	pq2.pop();

	return 0;
}