#pragma once

#include <iostream>
using namespace std;
#include <vector>
#include <assert.h>

namespace Thepale
{
	template <class T>
	struct Less
	{
		bool operator() (const T& e1, const T& e2)
		{
			return e1 < e2;
		}
	};

	template <class T>
	struct Greater
	{
		bool operator() (const T& e1, const T& e2)
		{
			return e1 > e2;
		}
	};

	//默认实现大堆
	template<class T, class Container = vector<T>, class Compare = Less<T>>
	class priority_queue
	{
	private:
		void adjust_up(int child)
		{
			Compare com; //定义仿函数
			int parent = (child - 1) / 2;

			while (child > 0)
			{
				if (com(_con[parent], _con[child]))
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) / 2;

				}
				else
				{
					break;
				}
			}
		}
  
		void adjust_down(size_t parent)
		{
			Compare com; //定义仿函数
			size_t child = parent * 2 + 1;

			while (child < _con.size())
			{
				if (child != _con.size() - 1 && com(_con[child], _con[child + 1]))
				{
					child++;
				}

				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}

	public:
		priority_queue() //无参构造
		{}

		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last) //迭代器构造
		{
			_con(first, last);

			//构建大堆
			int parent = ((_con.size() - 1) - 1) / 2;
			for (int i = parent; i >= 0; i--)
			{
				adjust_down(i);
			}
		}


		void push(const T& data)
		{
			_con.push_back(data);
			adjust_up(_con.size() - 1);
		}

		void pop()
		{
			assert(!_con.empty());
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}

		T& top()
		{
			return _con[0];
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}

	private:
		Container _con;
	};


	//仿函数原理
	template <class T>
	class A
	{
	public:
		T operator()(const T& a, const T& b)
		{
			return a + b;
		}
	};
}
