#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#include <functional>

//包装器
	//int func1(int a, int b)
	//{
	//	return a + b;
	//}
	//
	//class Test1
	//{
	//public:
	//	int operator() (int a, int b)
	//	{
	//		return a * b;
	//	}
	//};
	//
	//class Test2
	//{
	//public:
	//	int func1(char a, char b)
	//	{
	//		return a > b;
	//	}
	//
	//	static double func2(double a, double b)
	//	{
	//		return a + b;
	//	}
	//};
	//
	//int main()
	//{
	//	//包装普通函数和仿函数 - 统一变成 function<int(int, int)> 类型
	//	function<int(int, int)> f1 = func1;
	//	cout << f1(520, 1314) << endl;
	//	function<int(int, int)> f2 = Test1(); //调用仿函数给对象
	//	cout << f2(520, 1314) << endl;
	//
	//	//成员函数的调用需要 & 且声明类域(静态成员可以不用，但建议加上)
	//	function<int(Test2 ,char, char)> f3 = &Test2::func1; //成员函数需要加一个类类型，调用需要给对象
	//	cout << f3(Test2(), 123, 111) << endl;
	//
	//	function<double(double, double)> f4 = &Test2::func2; //静态成员函数可直接调用
	//	cout << f4(6.66, 3.33) << endl;
	//
	//	//包装lambda表达式
	//	function<int(int, int)> f5 = [](int a, int b) { return a + b; };
	//	cout << f5(6666, 3333) << endl;
	//
	//	return 0;
	//}

///////////////////////////////////////////////////////////////////////////////////////////////////
//template<class F, class T>
//void Func(F f, T t)
//{
//	static int count = 0;
//	cout << &count << endl;
//	cout << ++count << endl;
//}
//
//double F_Double(double d)
//{
//	return d / 2;
//}
//
//class Test
//{
//public:
//	double operator()(double d)
//	{
//		return d / 2;
//	}
//};
//
//
//int main()
//{
//	Func(F_Double(6.66), 5.55); //函数调用
//	Func([](double d)->double {return d; }, 6.78); //lambda 表达式
//	Func(Test(), 1.11); //仿函数调用
//
//	//以上代码实例化出了三个不同的版本
//
//
//	function<double(double)> f1 = F_Double;
//	function<double(double)> f2 = [](double d)->double {return d; };
//	function<double(double)> f3 = Test();
//
//	Func(f1, 5.55);
//	Func(f2, 6.78);
//	Func(f3, 1.11);
//
//	//以上代码类型一致，包装成一份，只实例化出一个版本
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////
int Sub(int a, int b)
{
	return a - b;
}

class Thepale
{
public:
	int Sub(int a, int b)
	{
		return a - b;
	}
};

int main()
{
	function<int(int, int)> f1 = Sub;
	cout << f1(10, 6) << endl; //正常包装普通函数

	function<int(int, int)> f2 = bind(Sub, placeholders::_1, placeholders::_2);
	cout << f2(10, 6) << endl; //正常绑定包装

	function<int(int, int)> f3 = bind(Sub, placeholders::_2, placeholders::_1);
	cout << f3(10, 6) << endl; //调换参数位置


	function<int(Thepale,int, int)> f4 = &Thepale::Sub;
	cout << f4(Thepale(), 10, 6) << endl; //正常包装成员函数

	function<int(int, int)> f5 = bind(&Thepale::Sub, Thepale(), placeholders::_1, placeholders::_2); //绑定调整参数个数（写死传递对象）
	cout << f5(10, 6) << endl; //不用传对象
	return 0;
}

//以上省略线程内容 thread