#include <iostream>
#include <string>
#include <list>
#include <unordered_map>
#include <algorithm>

#include <windows.h>

using std::cout;
using std::cin;
using std::endl;
using std::pair;
using std::make_pair;

class env_base //基类，记录作物的环境信息
{
protected:
	//分别是：范围区间，当前温度，偏移量
	pair<pair<double, double>, pair<double, double>> _temperature; //摄氏度
	pair<pair<double, double>, pair<double, double>> _humidity; //百分比
	pair<pair<double, double>, pair<double, double>> _light; //勒克斯
	pair<pair<double, double>, pair<double, double>> _pressure; //千帕
	pair<pair<double, double>, pair<double, double>> _wind; //米每秒

public:
	env_base(const pair<pair<double, double>, pair<double, double>> temperature = make_pair(make_pair(0, 0), make_pair(0, 0)),
		const pair<pair<double, double>, pair<double, double>> humidity = make_pair(make_pair(0, 0), make_pair(0, 0)),
		const pair<pair<double, double>, pair<double, double>> light = make_pair(make_pair(0, 0), make_pair(0, 0)),
		const pair<pair<double, double>, pair<double, double>> pressure = make_pair(make_pair(0, 0), make_pair(0, 0)),
		const pair<pair<double, double>, pair<double, double>> wind = make_pair(make_pair(0, 0), make_pair(0, 0)))
		:_temperature(temperature)
		,_humidity(humidity)
		,_light(light)
		,_pressure(pressure)
		,_wind(wind)
	{}

	//虚函数
	virtual pair<pair<double, double>, pair<double, double>>& gain_temperature() { return _temperature; }
	virtual pair<pair<double, double>, pair<double, double>>& gain_humidity() { return _humidity; }
	virtual pair<pair<double, double>, pair<double, double>>& gain_light() { return _light; }
	virtual pair<pair<double, double>, pair<double, double>>& gain_pressure() { return _pressure; }
	virtual pair<pair<double, double>, pair<double, double>>& gain_wind() { return _wind; }
};

class farmland_block : public env_base //派生类，记录名称和块号
{
	friend class agric_center;

private:
	std::string _name; //作物名称
	size_t _num; //块编号（可能单个农作物有多个编号，从 0 编号）
	
public:
	farmland_block(const std::string& name, const size_t num, const env_base& env = env_base())
		:env_base(env)
		,_name(name)
		,_num(num)
	{}

	bool check()
	{
		//温度检测
		if (_temperature.second.first > _temperature.first.second) _temperature.second.second = _temperature.second.first - _temperature.first.second;
		else if (_temperature.second.first < _temperature.first.first) _temperature.second.second = _temperature.second.first - _temperature.first.first;
		else _temperature.second.second = 0;

		//湿度检测
		if (_humidity.second.first > _humidity.first.second) _humidity.second.second = _humidity.second.first - _humidity.first.second;
		else if (_humidity.second.first < _humidity.first.first) _humidity.second.second = _humidity.second.first - _humidity.first.first;
		else _humidity.second.second = 0;

		//光照检测
		if (_light.second.first > _light.first.second) _light.second.second = _light.second.first - _light.first.second;
		else if (_light.second.first < _light.first.first) _light.second.second = _light.second.first - _light.first.first;
		else _light.second.second = 0;

		//大气压强检测
		if (_pressure.second.first > _pressure.first.second) _pressure.second.second = _pressure.second.first - _pressure.first.second;
		else if (_pressure.second.first < _pressure.first.first) _pressure.second.second = _pressure.second.first - _pressure.first.first;
		else _pressure.second.second = 0;

		//风速检测
		if (_wind.second.first > _wind.first.second) _wind.second.second = _wind.second.first - _wind.first.second;
		else if (_wind.second.first < _wind.first.first) _wind.second.second = _wind.second.first - _wind.first.first;
		else _wind.second.second = 0;

		if (_temperature.second.second != 0 || _humidity.second.second != 0 || _light.second.second != 0
			|| _pressure.second.second != 0 || _wind.second.second != 0) return false;

		return true;
	}

	void err_print()
	{
		printf("————————————————————————————————————\n");
		printf("作物名称：%s\n", _name.c_str());
		printf("所在块：%u\n", (unsigned int)_num);

		if (_temperature.second.second != 0) 
			printf("温度正常范围为：%.4f -- %.4f，当前温度为：%.4f, 温度偏移：%.4f\n", 
				_temperature.first.first, _temperature.first.second, _temperature.second.first, _temperature.second.second);

		if (_humidity.second.second != 0)
			printf("湿度正常范围为：%.4f -- %.4f，当前湿度为：%.4f, 湿度偏移：%.4f\n",
				_humidity.first.first, _humidity.first.second, _humidity.second.first, _humidity.second.second);

		if (_light.second.second != 0)
			printf("光照强度正常范围为：%.4f -- %.4f，当前光照强度为：%.4f, 光照强度偏移：%.4f\n",
				_light.first.first, _light.first.second, _light.second.first, _light.second.second);

		if (_pressure.second.second != 0)
			printf("大气压强正常范围为：%.4f -- %.4f，当前大气压强为：%.4f, 大气压强偏移：%.4f\n",
				_pressure.first.first, _pressure.first.second, _pressure.second.first, _pressure.second.second);

		if (_wind.second.second != 0)
			printf("风速正常范围为：%.4f -- %.4f，当前风速为：%.4f, 风速偏移：%.4f\n",
				_wind.first.first, _wind.first.second, _wind.second.first, _wind.second.second);

		printf("————————————————————————————————————\n");
	}

	bool operator==(const farmland_block& fb)
	{
		return _name == fb._name;
	}
};

farmland_block Null_Crop("NONE", -1);

class agric_center //主控类
{
protected:
	std::list<farmland_block> farmlands;
	std::unordered_map<std::string, env_base> hash_check;
	std::vector<std::vector<bool>> matrix;

public:
	agric_center(size_t row, size_t col)
	{
		matrix.resize(row);
		for (size_t i = 0; i < matrix.size(); ++i)
		{
			matrix[i].resize(col, 1);
		}
	}

	void add_crops(const farmland_block& fb)
	{
		farmlands.push_back(fb);
	}

	void del_crops(const farmland_block& fb)
	{
		auto ret = std::find(farmlands.begin(), farmlands.end(), fb);
		if (ret != farmlands.end()) 
		{
			farmlands.erase(ret);
		}
	}

	//多种方式查找 - 1.名称查找
	pair<farmland_block&, bool> find_crops(const std::string& name)
	{
		auto it = farmlands.begin();
		while (it != farmlands.end())
		{
			if (it->_name == name)
			{
				return pair<farmland_block&, bool>(*it, true);
			}

			++it;
		}

		return pair<farmland_block&, bool>(Null_Crop, false);
	}

	//多种方式查找 - 2.编号查找
	pair<farmland_block&, bool> find_crops(size_t num)
	{
		auto it = farmlands.begin();
		while (it != farmlands.end())
		{
			if (it->_num == num)
			{
				return pair<farmland_block&, bool>(*it, true);
			}

			++it;
		}

		return pair<farmland_block&, bool>(Null_Crop, false);
	}


	void check_crops()
	{
		std::list<farmland_block>::iterator it = farmlands.begin();
		while (it != farmlands.end())
		{
			if (!it->check())
			{
				it->err_print();
				int num = it->_num;
				matrix[num / matrix[0].size()][num % matrix[0].size()] = false;
			}

			++it;
		}
	}

	void print_crops(const farmland_block& fb)
	{
		printf("————————————————————————————————————\n");
		printf("作物名称：%s\n", fb._name.c_str());
		printf("所在块：%u\n", (unsigned int)fb._num);

			printf("温度正常范围为：%.4f -- %.4f，当前温度为：%.4f, 温度偏移：%.4f\n",
				fb._temperature.first.first, fb._temperature.first.second, fb._temperature.second.first, fb._temperature.second.second);

			printf("湿度正常范围为：%.4f -- %.4f，当前湿度为：%.4f, 湿度偏移：%.4f\n",
				fb._humidity.first.first, fb._humidity.first.second, fb._humidity.second.first, fb._humidity.second.second);

			printf("光照强度正常范围为：%.4f -- %.4f，当前光照强度为：%.4f, 光照强度偏移：%.4f\n",
				fb._light.first.first, fb._light.first.second, fb._light.second.first, fb._light.second.second);

			printf("大气压强正常范围为：%.4f -- %.4f，当前大气压强为：%.4f, 大气压强偏移：%.4f\n",
				fb._pressure.first.first, fb._pressure.first.second, fb._pressure.second.first, fb._pressure.second.second);

			printf("风速正常范围为：%.4f -- %.4f，当前风速为：%.4f, 风速偏移：%.4f\n",
				fb._wind.first.first, fb._wind.first.second, fb._wind.second.first, fb._wind.second.second);

		printf("————————————————————————————————————\n");
	}

	void check_matrix()
	{
		for (size_t i = 0; i < matrix.size(); ++i)
		{
			for (size_t j = 0; j < matrix[i].size(); ++j)
			{
				if (matrix[i][j] == true)
				{
					printf("√ ");
				}
				else
				{
					printf("× ");
				}
			}
			
			cout << endl;
		}

		cout << endl;
	}

};

void menu()
{
	printf("****************************************\n");
	printf("**   0.初始化农田    1.添加作物信息   **\n");
	printf("**   2.删除作物信息  3.查找作物信息   **\n");
	printf("**   4.进入自检状态  5.退出程序       **\n");
	printf("****************************************\n");
}

int main()
{
	int choice = 0;
	agric_center* ptr = (agric_center*)malloc(sizeof(agric_center)); //为了采用定位 new，因为不支持默认初始化 

	do
	{
		menu();

		cin >> choice;
		switch (choice)
		{
		case 0: 
		{
			printf("请输入农田分块的行列信息(row col)：");

			int row, col; cin >> row >> col;
			new(ptr)agric_center(row, col); //replacement new

			printf("农田初始化成功！\n");

			break;
		}

		case 1:
		{
			std::string name;
			size_t num;
			pair<pair<double, double>, pair<double, double>> tem;
			pair<pair<double, double>, pair<double, double>> hum;
			pair<pair<double, double>, pair<double, double>> light;
			pair<pair<double, double>, pair<double, double>> pressure;
			pair<pair<double, double>, pair<double, double>> wind;

			printf("请输入农作物名称：");
			cin >> name;
			printf("请输入农作物所在的农田编号：");
			cin >> num;
			printf("请输入%s的温度区间[a, b]：", name.c_str());
			cin >> tem.first.first >> tem.first.second;
			printf("请输入%s的湿度区间[a, b]：", name.c_str());
			cin >> hum.first.first >> hum.first.second;
			printf("请输入%s的光照强度区间[a, b]：", name.c_str());
			cin >> light.first.first >> light.first.second;
			printf("请输入%s的大气压强区间[a, b]：", name.c_str());
			cin >> pressure.first.first >> pressure.first.second;
			printf("请输入%s的风速区间[a, b]：", name.c_str());
			cin >> wind.first.first >> wind.first.second;

			farmland_block fb(name, num, env_base(tem, hum, light, pressure, wind));
			fb.check();

			ptr->add_crops(fb);

			printf("录入完成！\n");

			break;
		}

		case 2:
		{
			int c = 0;

			do
			{
				printf("以名称删除请输入 1，以编号删除请输入 2：");
				cin >> c;

				if (c == 1)
				{
					std::string str;
					printf("请输入农作物名称：");
					cin >> str;

					auto ret = ptr->find_crops(str);
					if (ret.second == false) printf("农作物不存在！\n");
					else 
					{
						ptr->del_crops(ret.first);
						printf("删除成功！\n");
					}

					break;
				}
				else if (c == 2)
				{
					size_t num;
					printf("请输入农作物编号：");
					cin >> num;

					auto ret = ptr->find_crops(num);
					if (ret.second == false) printf("农作物不存在！\n");
					else
					{
						ptr->del_crops(ret.first);
						printf("删除成功！\n");
					}

					break;
				}
				else
				{
					printf("输入错误，请重新输入：");
				}

			} while (1);

			ptr->check_crops();

			break;
		}

		case 3:
		{
			int c = 0;

			printf("以名称查找请输入 1，以编号查找请输入 2：");
			cin >> c;

			if (c == 1)
			{
				std::string str;
				printf("请输入农作物名称：");
				cin >> str;

				auto ret = ptr->find_crops(str);
				if (ret.second == false) printf("农作物不存在！\n");
				else
				{
					ptr->print_crops(ret.first);
				}
			}
			else if (c == 2)
			{
				size_t num;
				printf("请输入农作物编号：");
				cin >> num;

				auto ret = ptr->find_crops(num);
				if (ret.second == false) printf("农作物不存在！\n");
				else
				{
					ptr->print_crops(ret.first);
				}
			}
			else
			{
				printf("输入错误，请重新输入：");
			}

			break;
		}

		case 4:
		{
			while (1)
			{
				ptr->check_crops();
				ptr->check_matrix();

				Sleep(1000);
			}
			break;
		}

		case 5:
		{
			break;
		}

		default:
			printf("输入错误，请重新输入：");
			break;
		}
	} while (choice != 5);

	free(ptr);
	printf("程序已退出！\n");

	return 0;
}