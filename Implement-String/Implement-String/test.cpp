#define _CRT_SECURE_NO_WARNINGS 1
#include "String.h"

int main()
{
	Thepale::string s1("Hello Thepale!"); //正常构造
	Thepale::string s2(s1); //拷贝构造
	Thepale::string s3("");
	s3 = s2; //赋值重载
	Thepale::string s4; //空对象构造

	const Thepale::string s5("Iloveyou");
	//s5[1] = 'L'; - 匹配const版本[]，不可被修改
	s1[1] = 'a'; //匹配普通版本[]

	for (int i = 0; i < s1.size(); i++)
	{
		cout << s1[i];
	}
	cout << endl;

	Thepale::string::iterator it = s1.begin(); //迭代器
	*it = 'Z'; //非const
	while (it != s1.end())
	{
		cout << *it;
		it++;
	}
	cout << endl;

	const Thepale::string s6("Happyness");
	Thepale::string::const_iterator cit = s6.cbegin();
	//cit[0] = 'h'; - 匹配const版本，不可被修改

	//****请勿使用:
	//typedef char* newtype;
	//char ch = 'a';
	//const newtype p = &ch;
	//*p = 'b';
	//**** typedef 会做特殊处理，使 const 失效

	s1.push_back('!');
	s1.push_back('!');
	s1.append("This is you time!");
	for (auto e : s1)
	{
		cout << e;
	}
	cout << endl;

	return 0;

}