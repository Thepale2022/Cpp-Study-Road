#pragma once

#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::strlen;
using std::strcpy;
using std::swap;

namespace Thepale
{
	class string
	{
	public:
		//默认成员函数
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size + 1) //算上 '\0'
		{
			_str = new char[_capacity];
			strcpy(_str, str);
		}

		//传统写法
		//string(string& str)
		//	:_size(str._size)
		//	,_capacity(str._capacity)
		//{
		//	_str = new char[_capacity];
		//	strcpy(_str, str._str);
		//}
		//现代写法
		string(const string& str)
			:_size(str._size)
			,_capacity(str._capacity)
			,_str(nullptr)
		{
			string tmp(str._str);
			swap(_str, tmp._str);
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}
		

		//操作符重载
		char& operator[] (size_t pos) //普通版本
		{
			return _str[pos];
		}
		const char& operator[] (size_t pos) const //const对象版本
		{
			return _str[pos];
		}

		//传统写法
		//string& operator= (const string& str)
		//{
		//	if (&str == this)
		//	{
		//		return *this;
		//	}
		//	_size = str._size;
		//	_capacity = str._capacity;

		//	char* tmp = new char[_capacity];
		//	delete[] _str; //建议把delete放在new后面，如果new失败抛异常，不会导致_str被释放
		//	//当然，这就需要用临时变量temp来接收new出来的空间
		//	_str = tmp;
		//	strcpy(_str, str._str);

		//	return *this;
		//}
		//现代写法1
		//string& operator= (const string& str)
		//{
		//	if (&str == this)
		//	{
		//		return *this;
		//	}
		//	_size = str._size;
		//	_capacity = str._capacity;

		//	string tmp(str);
		//	swap(_str, tmp._str); //一石二鸟，拷贝了值，且释放了原空间

		//	return *this;
		//}
		//现代写法2
		string& operator= (string str)
		{
			_size = str._size;
			_capacity = str._capacity;
			swap(_str, str._str);
			return *this;
		}


		//成员函数
		size_t size() const 
		{
			return _size;
		}

		char* c_str() const
		{
			return _str;
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
			}
		}

		void push_back(char ch)
		{
			if (_size + 1 == _capacity)
			{
				reserve(_capacity * 2);
			}
			
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';

		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (len + _size + 1 > _capacity)
			{
				reserve(len + _size + 1);
			}

			//for (int i = 0; i < len; i++)
			//{
			//	_str[_size++] = str[i];
			//}
			//_str[_size] = '\0';
			strcpy(_str + _size, str);
			_size += len;
		}


		//迭代器 (范围for的本质)
		typedef char* iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		typedef const char* const_iterator;

		const_iterator cbegin() const
		{
			return _str;
		}

		const_iterator cend() const
		{
			return _str + _size;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}


