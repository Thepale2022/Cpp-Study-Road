#include <iostream>
using namespace std;

#include <set>

int main()
{
	//s1 - 基础插入和迭代器打印
	set<int> s1;
	s1.insert(3);
	s1.insert(1);
	s1.insert(5);
	s1.insert(2);
	s1.insert(8);
	s1.insert(1);
	s1.insert(8);

	set<int>::iterator it1 = s1.begin();
	while (it1 != s1.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;


	//s2 - 查找和删除(迭代器位置删除、值删除)
	set<int> s2;
	s2.insert(3);
	s2.insert(1);
	s2.insert(5);
	s2.insert(2);
	s2.insert(8);

	set<int>::iterator pos = s2.find(5);
	if (pos != s2.end())
	{
		s2.erase(pos);
	}

	s2.erase(3);

	set<int>::iterator it2 = s2.begin();
	while (it2 != s2.end())
	{
		cout << *it2 << " ";
		it2++;
	}
	cout << endl;


	//ms1 - 不去重
	multiset<int> ms1;
	ms1.insert(3);
	ms1.insert(1);
	ms1.insert(5);
	ms1.insert(2);
	ms1.insert(8);
	ms1.insert(1);
	ms1.insert(8);

	multiset<int>::iterator mit1 = ms1.begin();
	while (mit1 != ms1.end())
	{
		cout << *mit1 << " ";
		mit1++;
	}
	cout << endl;


	//ms2 - 多删除（迭代器位置）
	multiset<int> ms2;
	ms2.insert(3);
	ms2.insert(1);
	ms2.insert(5);
	ms2.insert(2);
	ms2.insert(8);
	ms2.insert(1);
	ms2.insert(8);
	ms2.insert(1);
	ms2.insert(1);

	multiset<int>::iterator mpos = ms2.find(1); //找到的是中序遍历的第一个
	while (mpos != ms2.end())
	{
		cout << *mpos << " ";
		mpos++;
	}
	cout << endl;

	multiset<int>::iterator mdel = ms2.find(1);
	while (mdel != ms2.end())
	{
		ms2.erase(mdel);
		mdel = ms2.find(1);
	}

	for (auto e : ms2)
	{
		cout << e << " ";
	}
	cout << endl;


	//ms3 - 多删除（值）
	multiset<int> ms3;
	ms3.insert(3);
	ms3.insert(1);
	ms3.insert(5);
	ms3.insert(2);
	ms3.insert(8);
	ms3.insert(1);
	ms3.insert(8);
	ms3.insert(1);
	ms3.insert(1);

	cout << "本次共删除了：" << ms3.erase(1) << "个" << endl;
	for (auto e : ms3)
	{
		cout << e << " ";
	}
	cout << endl;
	return 0;
}