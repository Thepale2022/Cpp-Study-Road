#define _CRT_SECURE_NO_WARNINGS 1

//函数重载 - 同名函数，只需要形参列表中：1.函数参数类型 2.函数参数个数 3.参数（不同类型）顺序
//其一不相同，则构成“一词多义”，根据实际调用情况决定调用哪个函数

#include <iostream>
using namespace std;

/*
//1.函数参数类型不同
int Add(int x, int y)
{
	return x + y;
}

double Add(double x, double y)
{
	return x + y;
}

int main()
{
	cout << Add(1, 2) << endl;
	cout << Add(1.1, 2.2) << endl;

	return 0;
}
*/


/*
//函数参数个数不同
int Func(int a)
{
	return a;
}

int Func(int a, int b)
{
	return a + b;
}

int main()
{
	cout << Func(1) << endl;
	cout << Func(1, 2) << endl;

	return 0;
}
*/


/*
//参数顺序不同
void Print(char a, int b)
{
	cout << a << " " << b << endl;
}

void Print(int a, char b)
{
	cout << a << " " << b << endl;
}

int main()
{
	Print('Z', 520);
	Print(1314, 'C');

	return 0;
}
*/


//注：缺省参数无特殊性质
/*
//以下两个函数不构成函数重载
void test(int a = 0)
{
	cout << a << endl;
}

void test(int a)
{
	cout << a << endl;
}

int main()
{
	test(520);
	test(1314);

	return 0;
}
*/


/*
//以下可以构成函数重载，但会出现问题
void test()
{
	cout << "hello world" << endl;
}

void test(int a = 0)
{
	cout << a << endl;
}

int main()
{
	test(1); //传参可以正常调用
	test(); //不传参产生二义性，调用不明确
	return 0;
}
*/