#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//template - 模板
//template <class [name]> 或 template <typename [name]>

/*
//函数模板
template <class T>
void Swap(T& e1, T& e2)
{
	T temp = e1;
	e1 = e2;
	e2 = temp;
}

//函数模板会在使用时被实例化，实例化成对于类型的函数
//故其 int double char 调用的 Swap函数 不是同一个，而是函数重载

int main()
{
	int a = 520;
	int b = 1314;
	Swap(a, b);

	double c = 5.2;
	double d = 1.314;
	Swap(c, d);

	char e = 'Z';
	char f = 'C';
	Swap(e, f);

	return 0;
}
*/

/*
template <class T>
T Add(const T& e1, const T& e2) //这里加 const 是因为
//强制类型转换生成临时变量，临时变量具有常属性
{
	return e1 + e2;
}

int main()
{
	int a = 10, b = 20;
	double c = 11.1, d = 22.2;

	//正常情况 - 隐式实例化：让编译器自己推导类型
	cout << Add(a, b) << endl;
	cout << Add(c, d) << endl;

	//异常情况
	//Add(a, d); - error
	//1.强制类型转换
	cout << Add(a, (int)d) << endl;
	//2.显式实例化
	cout << Add<int>(a, d) << endl;
	return 0;
}
*/

/*
int Add(int e1, int e2) //1
{
	return e1 + e2;
}

template <class T>
T Add(const T& e1, const T& e2) //2
{
	return e1 + e2;
}
//同时出现则调用 1 - 效率至上

int main()
{
	int a = 10;
	int b = 20;
	Add(a, b);

	return 0;
}
*/


/*
//类模板
template <class T>
class Stack
{
private:
	T* _arr;
	int _top;
	int _capacity;

public:
	Stack(int capacity = 4)
		:_top(0)
		,_capacity(capacity)
		,_arr(new T[capacity])
	{}

	void Push() {};

	~Stack()
	{
		delete[] _top;
		_top = 0;
		_capacity = 0;
	}
};

template <class T> //声明定义分离时的指定方式
void Stack<T>::Push(){} //同时指定类域和模板

int main()
{
	Stack<int> st1; //类模板必须显式实例化

	return 0;
}
*/