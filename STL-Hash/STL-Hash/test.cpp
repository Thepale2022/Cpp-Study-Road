#define _CRT_CECURE_NOWARNINGS 1

#include "hash.h"

void Test_For_Insert(vector<int>& v, set<int>& s, unordered_set<int>& us)
{
	size_t begin = 0, end = 0;

	//随机插入
	{
		set<int> s;
		unordered_set<int> us;

		begin = clock();
		for (auto e : v)
		{
			s.insert(e);
		}
		end = clock();

		cout << "Set插入耗时：" << (end - begin) << "ms" << endl;

		begin = clock();
		for (auto e : v)
		{
			us.insert(e);
		}
		end = clock();

		cout << "Unordered_Set插入耗时：" << (end - begin) << "ms" << endl;
	}



	//有序插入
	{
		set<int> s;
		unordered_set<int> us;

		begin = clock();
		for (size_t i = 0; i < 32768; ++i)
		{
			s.insert(i);
		}
		end = clock();

		cout << "Set插入耗时：" << (end - begin) << "ms" << endl;

		begin = clock();
		for (size_t i = 0; i < 32768; ++i)
		{
			us.insert(i);
		}
		end = clock();

		cout << "Unordered_Set插入耗时：" << (end - begin) << "ms" << endl;
	}
	
}

void Test_For_Find(vector<int>& v, set<int>& s, unordered_set<int>& us)
{
	size_t begin = 0, end = 0;

	//随机查找
	{
		begin = clock();
		for (auto e : v)
		{
			s.find(e);
		}
		end = clock();

		cout << "Set查找耗时：" << (end - begin) << "ms" << endl;


		begin = clock();
		for (auto e : v)
		{
			us.find(e);
		}
		end = clock();

		cout << "Unordered_Set查找耗时：" << (end - begin) << "ms" << endl;
	}


	//顺序查找
	{
		begin = clock();
		for (size_t i = 0; i < 1000000; ++i)
		{
			s.find(i);
		}
		end = clock();

		cout << "Set查找耗时：" << (end - begin) << "ms" << endl;


		begin = clock();
		for (size_t i = 0; i < 1000000; ++i)
		{
			us.find(i);
		}
		end = clock();

		cout << "Unordered_Set查找耗时：" << (end - begin) << "ms" << endl;
	}
}

void Test_For_Erase(vector<int>& v, set<int>& s, unordered_set<int>& us)
{
	size_t begin = 0, end = 0;

	{
		begin = clock();
		for (auto e : v)
		{
			s.erase(e);
		}
		end = clock();
		cout << "Set移除耗时：" << (end - begin) << "ms" << endl;


		begin = clock();
		for (auto e : v)
		{
			us.erase(e);
		}
		end = clock();
		cout << "Unordered_Set移除耗时：" << (end - begin) << "ms" << endl;
	}
}

int main()
{
	srand((size_t)time(0));
	vector<int> v;
	size_t n = 1000000;
	v.reserve(n);

	for (size_t i = 0; i < n; ++i)
	{
		v.push_back(rand());
	}

	set<int> s;
	unordered_set<int> us;
	for (auto e : v)
	{
		s.insert(e);
		us.insert(e);
	}

	//Test_For_Insert(v);
	//Test_For_Find(v, s, us);
	Test_For_Erase(v, s, us);

	return 0;
}

//综上观之，unordered_set 的性能略优于 set（map同理）

//1.unordered_set是无序的，set是有序的
//2.unordered_set是单向迭代器，set是双向迭代器
