#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//try - catch - throw 抛异常 
//double Div(double e1, double e2)
//{
//	if (e2 == 0)
//	{
//		throw "The Dividend could't be zero";
//	}
//
//	return e1 / e2;
//}
//
//void Func1()
//{
//	int e1, e2;
//	cin >> e1 >> e2;
//
//	cout << Div(e1, e2) << endl;
//}
//
//int main()
//{
//	try
//	{
//		Func1();
//	}
//	catch (const char* errstr)
//	{
//		cout << errstr << endl;
//	}
//	catch (...)
//	{
//		cout << "Unexpected exception" << endl;
//	}
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//不同异常类型的匹配以及调用链异常的就近捕获
//

///////////////////////////////////////////////////////////////////////////////////////////////////

//函数调用链中，异常导致栈展开
//过程：
//1.检查 throw 是否在 try 语句块内部，是则查找匹配的 catch
//2.没有匹配的 catch 语句会退出当前函数栈，继续在调用当前函数的函数栈中查找匹配的 catch
//3.到 main 函数还是为找到匹配 catch，则终止程序
//4.找到 catch 并处理后，会继续执行此 catch 所在函数的在 catch 后的语句

//int main()
//{
//	try
//	{
//		throw 1;
//	}
//	catch (int i)
//	{
//		cout << 666 << endl;
//	}
//
//	cout << "异常被捕获，此行正常输出" << endl;
//
//	return 0;
//}

void Func1(); void Func2();

//void Func2()
//{
//	cout << "Func2 - begin" << endl;
//
//	int* arr = new int[10]; //内存泄漏
//	throw 1; //抛出异常，找不到对应 catch，结束当前函数栈帧
//
//	cout << "Func2 - end" << endl; //此行未被执行
//
//	//***如果在此异常抛出后，前有动态开辟的空间，则发生内存泄漏***
//	delete[] arr;
//	cout << "delete finished" << endl;
//}

//解决内存泄漏的问题，可以如下处理（非智能指针的解决方案）：
//void Func2()
//{
//	cout << "Func2 - begin" << endl;
//
//	int* arr = new int[10];
//	try 
//	{
//		throw 1;
//	}
//	catch (int i)
//	{
//		delete[] arr;
//		cout << "delete finished" << endl;
//		throw i; //接收异常处理后再抛出异常
//	}
//	//或写成
//	//catch (...)
//	//{
//	//	//...
//	//	throw; //此行表示接收到什么对象就再抛出去
//	//}
//	
//	cout << "Func2 - end1" << endl;
//}
//
//void Func1()
//{
//	cout << "Func1 - begin" << endl;
//
//	try
//	{
//		Func2();
//	}
//	catch (int i)
//	{
//		cout << "Func1" << endl;
//	}
//
//	cout << "Func1 - end" << endl;
//}
//
//int main()
//{
//	try
//	{
//		Func1();
//	}
//	catch (int i)
//	{
//		cout << "main" << endl;
//	}
//
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//允许抛出派生类对象使用基类捕获
//class Base
//{
//public:
//	Base(const string& errmsg, int errid)
//		:_errmsg(errmsg)
//		,_errid(errid)
//	{}
//
//	const string& what() const
//	{
//		return _errmsg;
//	}
//
//protected:
//	const string _errmsg;
//	int _errid;
//};
//
//class Son1 : public Base
//{
//public:
//	Son1(const string& errmsg, int errid, const string& mymsg)
//		:Base(errmsg, errid)
//		,_mymsg(mymsg)
//	{}
//
//private:
//	const string _mymsg;
//};
//
//class Son2 : public Base
//{
//public:
//	Son2(const string& errmsg, int errid, const string& mymsg)
//		:Base(errmsg, errid)
//		, _mymsg(mymsg)
//	{}
//
//private:
//	const string _mymsg;
//};
//
//void Func1()
//{
//	Son1 s1("网络错误", 1000, "port");
//	throw s1;
//}
//
//void Func2()
//{
//	Son2 s2("权限错误", 1001, "w");
//	throw s2;
//}
//
//int main()
//{
//	try
//	{
//		//Func1(); //执行此行 - 网络错误
//		Func2(); //执行此行 - 权限错误
//	}
//	catch (const Base& base)
//	{
//		cout << base.what() << endl;
//	}
//
//	//假如多个函数都出现网络错误、权限错误，怎么知道是哪个地方错了呢？
//	//*使用多态解决*
//
//	return 0;
//}


//多态
//class Base
//{
//public:
//	Base(const string& errmsg, int errid)
//		:_errmsg(errmsg)
//		,_errid(errid)
//	{}
//
//	virtual string what() const
//	{
//		string str = "Base:";
//		str += _errmsg;
//		return str;
//	}
//
//protected:
//	const string _errmsg;
//	int _errid;
//};
//
//class Son1 : public Base
//{
//public:
//	Son1(const string& errmsg, int errid, const string& mymsg)
//		:Base(errmsg, errid)
//		,_mymsg(mymsg)
//	{}
//
//	virtual string what() const
//	{
//		string str = "Son1:";
//		str += _errmsg;
//		return str;
//	}
//
//private:
//	const string _mymsg;
//};
//
//class Son2 : public Base
//{
//public:
//	Son2(const string& errmsg, int errid, const string& mymsg)
//		:Base(errmsg, errid)
//		, _mymsg(mymsg)
//	{}
//
//	virtual string what() const
//	{
//		string str = "Son2:";
//		str += _errmsg;
//		return str;
//	}
//
//private:
//	const string _mymsg;
//};
//
//void Func1()
//{
//	Son1 s1("网络错误", 1000, "port");
//	throw s1;
//}
//
//void Func2()
//{
//	Son2 s2("权限错误", 1001, "w");
//	throw s2;
//}
//
//int main()
//{
//	try
//	{
//		//Func1();
//		Func2();
//	}
//	catch (Base& base) //多态 - 传谁的对象就调用谁的 what
//	{
//		cout << base.what() << endl;
//	}
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////////////////////////////

//异常安全
//1.构造函数中尽量不要抛异常，否则可能导致对象不完整或未完全初始化
//2.析构函数中尽量不要抛异常，否则可能导致内存泄漏等

//异常规范
void Function1() throw(int, double, char, short) //表示此函数可能抛出 int、double、char、short 几种类型中的一种异常
{}

void Function2() throw(int) //表示此函数只可能抛出 int 类型的异常
{}

void Function3() throw() //表示此函数不会抛出异常
{}

void Function4() noexcept //C++11 明确表示不抛异常
{}

//异常优点：
//1.相比错误码容易定位错误信息，且可以索引到对应函数栈帧
//2.可以直接让最外层拿到错误，错误码必须层层返回，且占用了函数返回值
//3.部分函数（如 T& operator[]）不可以通过返回值表示错误信息
//4.更多的第三方库同样使用异常
//5.很多测试框架都使用异常，可以更好的使用单元测试等白盒测试

//异常缺点：
//1.执行流乱跳，可能导致断点无法成功截断等调试困难
//2.有少许性能开销（可忽略不计）
//3.C++没有垃圾回收机制，容易导致内存泄漏、死锁等问题