#include "List.h"

int main()
{
	//ls1
	Thepale::list<int> ls1;
	ls1.push_back(1);
	ls1.push_back(2);
	ls1.push_back(3);
	ls1.push_back(4);

	Thepale::list<int>::iterator it1 = ls1.begin();
	while (it1 != ls1.end())
	{
		*it1 = 666;
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;

	//const ls2
	const Thepale::list<int> ls2;
	ls2.push_back(1);
	ls2.push_back(2);
	ls2.push_back(3);
	ls2.push_back(4);

	Thepale::list<int>::const_iterator it2 = ls2.begin();
	while (it2 != ls2.end())
	{
		//*it2 = 666; - error
		cout << *it2 << " ";
		it2++;
	}
	cout << endl;

	//ls3
	Thepale::list<Thepale::Date> ls3;
	ls3.push_back(Thepale::Date(2022, 9, 12));
	ls3.push_back(Thepale::Date(2022, 10, 1));
	ls3.push_back(Thepale::Date(2022, 12, 17));

	Thepale::list<Thepale::Date>::iterator it3 = ls3.begin();
	while (it3 != ls3.end())
	{
		cout << it3->_year << " " << it3->_month << " " << it3->_day << endl;
		it3++;
	}

	//ls4
	Thepale::list<int> ls4;
	ls4.push_back(1);
	ls4.push_back(2);
	ls4.push_back(3);
	ls4.push_back(4);

	ls4.insert(ls4.begin(), 520);
	ls4.insert(ls4.end(), 1314);

	Thepale::list<int>::iterator it4 = ls4.begin();
	while (it4 != ls4.end())
	{
		cout << *it4 << " ";
		it4++;
	}
	cout << endl;

	//ls5
	Thepale::list<int> ls5;
	ls5.push_back(1);
	ls5.push_back(2);
	ls5.push_back(3);
	ls5.push_back(4);
	ls5.push_back(5);

	ls5.erase(ls5.begin());
	ls5.erase(--ls5.end());

	Thepale::list<int>::iterator it5 = ls5.begin();
	while (it5 != ls5.end())
	{
		cout << *it5 << " ";
		it5++;
	}
	cout << endl;


	//ls6
	Thepale::list<int> ls_test1;
	ls_test1.push_back(1);
	ls_test1.push_back(2);
	ls_test1.push_back(3);
	ls_test1.push_back(4);

	Thepale::list<int> ls_test2;
	ls_test2.push_back(12);
	ls_test2.push_back(23);
	ls_test2.push_back(34);
	ls_test2.push_back(45);

	Thepale::list<int> ls6(ls_test1);
	for (auto e : ls6)
	{
		cout << e << " ";
	}
	cout << endl;

	ls6 = ls_test2;
	for (auto e : ls6)
	{
		cout << e << " ";
	}
	cout << endl;

	//ls7
	//Thepale::list<int> ls7(5, 1); //报错 - 非法的间接寻址（匹配错误构造）
	//解决办法：提供一个重载为 int 的版本，则变为更匹配的
	//**编译器永远找最匹配的

	Thepale::list<int> ls7(5, 1); 
	for (auto e : ls7)
	{
		cout << e << " ";
	}
	cout << endl;

	//ls8
	Thepale::list<int> ls8;
	ls8.push_back(1);
	ls8.push_back(2);
	ls8.push_back(3);
	ls8.push_back(4);

	Thepale::list<int>::reverse_iterator rit = ls8.rbegin();
	while (rit != ls8.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;

	return 0;
}