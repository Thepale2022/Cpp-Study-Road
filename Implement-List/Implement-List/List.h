#pragma once
#include <iostream>
#include <assert.h>
#include "reverse_iterator.h"

using std::cout;
using std::endl;
using std::swap;

namespace Thepale
{
	//节点
	template<class T>
	struct ListNode
	{
	public:
		ListNode(const T& data = T())
			:_prev(nullptr)
			,_next(nullptr)
			,_data(data)
		{}

		//以下成员必须公开共迭代器访问
		ListNode<T>* _prev;
		ListNode<T>* _next;
		T _data;
	};

	//迭代器
	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
	public:
		typedef ListNode<T> Node;
		typedef __list_iterator<T, Ref, Ptr> self;

		__list_iterator(Node* address)
			:_node(address)
		{}

		// ++it
		self& operator++ ()
		{
			_node = _node->_next;
			return *this;
		}

		// it++
		self operator++ (int place)
		{
			__list_iterator temp(_node);
			_node = _node->_next;
			return temp;
		}

		// --it
		self& operator-- ()
		{
			_node = _node->_prev;
			return *this;
		}

		// it--
		self operator-- (int place)
		{
			__list_iterator temp(_node);
			_node = _node->_prev;
			return temp;
		}

		Ref operator* ()
		{
			return _node->_data;
		}

		Ptr operator-> ()
		{
			return &(_node->_data);
		}

		bool operator!= (const self obj) const
		{
			return _node != obj._node;
		}

		bool operator== (const self  obj) const
		{
			return _node == obj._node;
		}

	//private:
		Node* _node;
	};

	//链表
	template<class T>
	class list
	{
	public:
		typedef ListNode<T> Node;
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;

		//构造
		list()
		{
			_head = new Node();
			_head->_prev = _head;
			_head->_next = _head;
		}

		//list(const list<T>& ls) - 传统写法
		//{
		//	_head = new Node;
		//	_head->_prev = _head;
		//	_head->_next = _head;
		//	_head->_data = -1;

		//	for (auto e : ls)
		//	{
		//		push_back(e);
		//	}
		//}

		//现代写法
		template<class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			//这里是初始化temp的头
			_head = new Node();
			_head->_prev = _head;
			_head->_next = _head;

			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		list(const list<T>& ls)
		{
			//这个头节点只是为了能被正常析构
			_head = new Node();
			_head->_prev = _head;
			_head->_next = _head;

			list<T> temp(ls.begin(), ls.end());
			swap(_head, temp._head);
		}

		list(size_t n, const T& data = T())
		{
			_head = new Node();
			_head->_prev = _head;
			_head->_next = _head;

			for (size_t i = 0; i < n; i++)
			{
				push_back(data);
			}
		}

		list(int n, const T& data = T()) //重载为int n
		{
			_head = new Node();
			_head->_prev = _head;
			_head->_next = _head;

			for (size_t i = 0; i < n; i++)
			{
				push_back(data);
			}
		}

		~list()
		{
				clear();
				_head = nullptr;
		}


		//成员函数
		//insert后迭代器不失效
		iterator insert(iterator pos, const T& data)
		{
			Node* newnode = new Node(data);
			
			Node* cur = pos._node;
			Node* curprev = cur->_prev;

			newnode->_next = cur;
			newnode->_prev = curprev;
			cur->_prev = newnode;
			curprev->_next = newnode;

			return iterator(newnode);
		}
		
		iterator erase(iterator pos)
		{
			assert(pos != end());

			Node* prev = pos._node->_prev;
			Node* next = pos._node->_next;

			prev->_next = next;
			next->_prev = prev;

			delete pos._node;

			return iterator(next);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		void push_front(const T& data)
		{
			insert(begin(), data);
		}

		void push_back(const T& data) const //可复用
		{
			Node* newnode = new Node(data);

			Node* tail = _head->_prev;

			tail->_next = newnode;
			newnode->_next = _head;
			newnode->_prev = tail;
			_head->_prev = newnode;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				iterator del = it++;
				delete del._node;
			}

			_head->_next = _head;
			_head->_prev = _head;
			//或者用erase替代
		}


		//运算符重载
		//list<T>& operator= (const list<T>& ls) - 传统写法
		//{
		//	clear();
		//	for (auto e : ls)
		//	{
		//		push_back(e);
		//	}

		//	return *this;
		//}

		list<T>& operator= (list<T> ls)
		{
			swap(_head, ls._head);
			return *this;
		}


		//迭代器
		iterator begin()
		{
			return iterator(_head->_next);
		}

		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator end() const 
		{
			return const_iterator(_head);
		}

		//反向迭代器
		typedef Thepale::reverse_iterator<iterator, T&, T*> reverse_iterator;
		typedef Thepale::reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		const reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

	private:
		Node* _head;
	};

	class Date
	{
	public:
		Date(size_t year = 1, size_t month = 1, size_t day = 1)
			:_year(year)
			,_month(month)
			,_day(day) 
		{}

		size_t _year;
		size_t _month;
		size_t _day;
	};
}