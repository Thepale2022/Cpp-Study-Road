#pragma once

namespace Thepale
{
	template<class Iterator, class Ref, class Ptr>
	class reverse_iterator
	{
	public:
		reverse_iterator(Iterator it)
			:_it(it)
		{}

		 Ref operator* () const
		{
			 Iterator temp = _it;
			 return *(--temp);
		}

		 Ptr operator-> () const
		 {
			 return &operator*();
		 }

		reverse_iterator& operator++()
		{
			--_it;
			return *this;
		}

		reverse_iterator operator++(int)
		{
			reverse_iterator temp = *this;
			--_it;
			return temp;
		}

		reverse_iterator& operator--()
		{
			++_it;
			return *this;
		}

		reverse_iterator operator--(int)
		{
			reverse_iterator temp = *this;
			++_it;
			return temp;
		}

		bool operator!=(const reverse_iterator& rit) const
		{
			return _it != rit._it;
		}

		bool operator==(const reverse_iterator& rit) const
		{
			return _it == rit._it;
		}

	private:
		Iterator _it;
	};
}
