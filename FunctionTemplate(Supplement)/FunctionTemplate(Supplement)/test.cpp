#include "head.h"

int main()
{
	//非类型模板参数
	Thepale::Stack<int, 100> st1;
	Thepale::Stack<double, 200> st2;
	
	
	//函数模板 - 参数匹配
	cout << Thepale::ObjLess(1, 2) << endl; //匹配1

	Date* d1 = new Date(2022, 9, 12);
	Date* d2 = new Date(2022, 10, 1);
	//特化调用
	cout << Thepale::ObjLess(d1, d2) << endl; //匹配2 (有现成吃现成，没现成点外卖(实例化))
	//如果屏蔽2，会调用函数特化


	//类模板特化
	Thepale::Test<int, int> t1;
	Thepale::Test<int, char> t2; //全特化
	Thepale::Test <double, char> t3; //偏特化1
	Thepale::Test <double*, char*> t4; //偏特化2
	 

	//声明和定义分离 - 方便维护
	//看“.h”文件为了解设计框架和功能
	//看“.cpp”文件为了了解具体实现细节

	//模板的分离编译 - 见"template.h && template.cpp"

	Func1(10); //报错 - 无法解析的外部符号（链接错误）
	Func2(10); //正常
	//reason:1.预编译（生成 .i 文件，展开头文件/宏替换/条件编译/去除注释）
		   //2.编译（生成 .s 文件，这个阶段template.cpp中的 Func1 未编译处理 <因为模板未实例化>
		   //但template.h中的 Func1 和 Func2 都因头文件而证实其存在，等待链接 <填地址>）
		   //3.汇编（生产 .o 文件，把汇编代码变成二进制代码）
		   //4.链接（通过符号匹配不到符号表里的 Func1，因为 Func1 在template.cpp中未被编译） -- 报错
		   //**注意：在这个阶段中，template.h 是知道T应该实例化成int的，
		   //但是 template.cpp 不知道，才导致的未实例化报错**
	return 0;
}