#pragma once

#include <iostream>
#include "date.h"
#include "template.h"

using namespace std;

namespace Thepale
{
	//非类型模板参数 - **整型常量**
	template <class T, size_t N> 
	class Stack
	{
	private:
		T _arr[N]; //需要初始化固定大小栈
		size_t _size;
		size_t _capacity;
	};


	//函数模板特化
	template <class T>
	bool ObjLess(T e1, T e2) //1
	{
		return e1 < e2;
	}

	//bool ObjLess(Date* e1, Date* e2) //2
	//{
	//	return *e1 < *e2;
	//}

	template <>
	bool ObjLess<Date*>(Date* e1, Date* e2) //函数模板特化（专用显式实例化调用）
	{
		return *e1 < *e2;
	}


	//类模板特化
	template <class T1, class T2>
	class Test
	{
	public:
		Test()
			:_a(T1())
			,_b(T2())
		{
			cout << "template<T1, T2>" << endl;
		}

	private:
		T1 _a;
		T2 _b;
	};

	template <> //全特化
	class Test <int, char>
	{
	public:
		Test()
			:_a(0)
			, _b(0)
		{
			cout << "全特化：template<int, char>" << endl;
		}

	private:
		int _a;
		char _b;
	};

	template <class T> //偏特化1（是对参数的进一步限制）
	class Test <T, char>
	{
	public:

		Test()
			: _a(T())
			, _b(0)
		{
			cout << "偏特化1：template<int, char>" << endl;
		}

	private:
		T _a;
		char _b;
	};

	template <class T1, class T2> //偏特化2（是对参数的进一步限制）
	class Test <T1*, T2*> //此行是进一步限制类型为 * 
	{
	public:
		Test()
		{
			cout << "偏特化2：template<int, char>" << endl;
		}

	private:
		T1 _a;
		T2 _b;
	};
}
