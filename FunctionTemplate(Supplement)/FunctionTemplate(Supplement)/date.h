#pragma once

#include <iostream>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1); //初始化日期
	void Print(); //打印日期
	int GetMonthDay(int year, int month); //获得当月日期
	void PrintWeekDay();

	//运算符重载
	bool operator> (const Date& d) const;
	bool operator< (const Date& d) const;
	bool operator>= (const Date& d) const;
	bool operator<= (const Date& d) const; 
	bool operator== (const Date& d) const; 
	bool operator!= (const Date& d) const; 

	Date& operator= (const Date& d);
	Date& operator+= (int day);
	Date operator+ (int day);
	Date& operator-= (int day);
	Date operator- (int day);

	Date& operator++(); //前置++
	Date operator++(int placeholder); //后置++
	//C++为了做区分，后置++需要添加一个占位符	
	Date& operator--(); //前置--
	Date operator--(int placeholder); //后置--

	int operator- (const Date& d); //日期 - 日期

	//输入输出
	//ostream& operator<< (ostream& out);
	friend ostream& operator<< (ostream& out, const Date& d);
	friend istream& operator>> (istream& in, Date& d);
private:
	int _year;
	int _month;
	int _day;
};
