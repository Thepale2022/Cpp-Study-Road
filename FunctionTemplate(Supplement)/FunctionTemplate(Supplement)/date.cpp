#define _CRT_SECURE_NO_WARNINGS 1

#include "date.h"

Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;

	//判断日期合法性
	if (_year < 0 || _month <= 0 || _month > 12 || day <= 0 || day > GetMonthDay(_year, _month))
	{
		cout << "日期非法->";
		Print();
		cout << endl;
	}
}

void Date::Print()
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

int Date::GetMonthDay(int year, int month)
{
	static int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 }; // - 使下标直接和月份对齐

	if (month == 2 && (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)) //必须加括号
	{
		return MonthDay[2] + 1;
	}
	else
	{
		return MonthDay[month];
	}
}

bool Date::operator> (const Date& d) const
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year && _month > d._month)
	{
		return true;
	}
	else if (_year == d._year && _month == d._month && _day > d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator== (const Date& d) const
{
	if (_year == d._year && _month == d._month && _day == d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//实际上只要实现了 > 和 == 其他的就可以复用

bool Date::operator< (const Date& d) const
{
	//if (_year < d._year)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month < d._month)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month == d._month && _day < d._day)
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}

	return !(*this > d || *this == d);
}

bool Date::operator>= (const Date& d) const
{
	//if (_year > d._year)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month > d._month)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month == d._month && _day >= d._day)
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}
	return *this > d || *this == d;
}

bool Date::operator<= (const Date& d) const
{
	//if (_year < d._year)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month < d._month)
	//{
	//	return true;
	//}
	//else if (_year == d._year && _month == d._month && _day <= d._day)
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}
	return !(*this > d);
}

bool Date::operator!= (const Date& d) const
{
	//if (_year == d._year && _month == d._month && _day == d._day)
	//{
	//	return false;
	//}
	//else
	//{
	//	return true;
	//}
	return !(*this == d);
}

Date& Date::operator= (const Date& d)
{
	if (this != &d) //简化自己给自己赋值的情况
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	
	return *this;
}

Date& Date::operator+= (int day)
{
	if (day < 0)
	{
		return *this -= -day;
	}
	_day += day;

	int ret = 0;
	while (_day > (ret = GetMonthDay(_year, _month)))
	{
		_day -= ret;
		if (_month <= 12 && _month >= 1)
		{
			_month++;
		}
		else
		{
			_year++;
			_month -= 12;
		}
	}

	return *this;
}

Date Date::operator+ (int day)
{
	Date d(*this);
	d += day; //本质就是对+=代码的复用

	return d;
}

Date& Date::operator-= (int day)
{
	//*处理-=负数情况
	if (day < 0)
	{
		return *this += -day;
	}

	_day -= day;

	while (_day <= 0)
	{
		int ret = GetMonthDay(_year, _month - 1);
		_day += ret;
		if (_month-1 <= 12 && _month-1 >= 1)
		{
			_month--;
		}
		else
		{
			_year--;
			_month += 12;
		}
	}

	return *this;
}

Date Date::operator- (int day)
{
	Date d(*this);
	d -= day; //本质就是对+=代码的复用

	return d;
}

Date& Date::operator++()
{
	*this += 1;
	return *this;
}

Date Date::operator++(int placeholder)
{
	Date d(*this);
	*this += 1;
	return d;
}

Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

Date Date::operator--(int placeholder)
{
	Date d(*this);
	*this += 1;
	return d;
}

int Date::operator- (const Date& d)
{
	int flag = 1;
	Date max;
	Date min;
	if (*this > d)
	{
		max = *this;
		min = d;
	}
	else
	{
		flag = -1;
		max = d;
		min = *this;
	}

	int count = 0;
	while (max != min)
	{
		count++;
		min++;
	}

	return count * flag;
}

void Date::PrintWeekDay()
{
	Date std(1900, 1, 1);
	int count = *this - std;

	cout << "星期" << (count % 7 + 1) << endl;
}

ostream& operator<< (ostream& out, const Date& d)
{
	cout <<d._year << "-" << d._month << "-" <<d._day << endl;

	return out;
}

istream& operator>> (istream& in, Date& d)
{
	cin >> d._year >> d._month >> d._day;

	return in;
}