#include "template.h"
#include <iostream>
using namespace std;

//无法实例化，无法编译生成汇编代码
template <class T>
void Func1(const T& e)
{
	cout << "void Func1(const T& e)" << endl;
}

void Func2(int e)
{
	cout << "void Func2(int e)" << endl;
}

//solution:
//1.显式实例化 - 失去了泛型意义，用一个就得实例化一个
template
void Func1<int>(const int& e);

//2.声明定义不分离
//故被使用时，头文件展开声明定义都有，会直接实例化且填上地址，不需要链接去找
//但维护性变差