#define _CRT_SECURE_NO_WARNINGS 1

#include "func.h"

/*
//类定义了一个新的作用域，类的所有成员都在类的作用域中
//在类体外定义成员时，需要使用 :: 作用域操作符指明成员属于哪个类域
void StudentTest::Init(const char name[20], int age, int id)
{
	strcpy(_name, name);
	_age = age;
	_id = id;
}

void StudentTest::Print()
{
	cout << _name << endl;
	cout << _age << endl;
	cout << _id << endl;
}
*/