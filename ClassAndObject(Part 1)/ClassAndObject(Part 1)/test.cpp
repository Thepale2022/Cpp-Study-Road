#define _CRT_SECURE_NO_WARNINGS 1

//类和对象
//面对对象的三大特性：封装、继承、多态

#include "func.h"

/*
//这就默认是一个类
struct Student1
{
	char name[20];
	int age;
	int id;
};

//在类里面，实际上可以包含函数 - 面对对象的三大特性之一 - 封装
//**封装：将数据和操作数据的方法进行有机结合，隐藏对象的属性和实现细节
//仅对外公开接口来和对象进行交互
//封装本质上是一种更严格的管理
struct Student2
{
	//成员变量（加下划线是为了防止和成员函数的参数一致）
	char _name[20];
	int _age;
	int _id;

	//成员函数（成员方法）
	void Init(const char name[20], int age, int id)
	{
		strcpy(_name, name);
		_age = age;
		_id = id;
	}

	void Print()
	{
		cout << _name << endl;
		cout << _age << endl;
		cout << _id << endl;
	}
};
int main()
{
	Student2 st; //对类而言，可以直接使用类名
	//struct Student2 st; - 是为了兼容 C 的写法
	st.Init("ZC", 19, 5201314);
	st.Print();
	return 0;
}
*/


/*
//而这只是 C++ 对 C 的兼容
//C++ 中类为 class

//访问限定符：
//private（私有）; protected（保护）; public（公开）
//public修饰的成员在类外可以直接被访问
//protected和private修饰的成员在类外不能直接被访问

//访问权限作用域从该访问限定符出现的位置开始直到下一个访问限定符出现时为止
//如果没有下一个，则持续到类的结束

//class的默认访问权限为private，struct的为public（因为struct要兼容C）
class Student
{
private:  
	char _name[20];
	int _age;
	int _id;

public:
	void Init(const char name[20], int age, int id)
	{
		strcpy(_name, name);
		_age = age;
		_id = id;
	}

	void Print()
	{
		cout << _name << endl;
		cout << _age << endl;
		cout << _id << endl;
	}
};

int main()
{
	Student st;
	st.Init("ZC", 19, 5201314);
	st.Print();

	//st._age = 19; - error - 为private，无法访问

	return 0;
}
*/


/*
//见 func.c func.h
int main()
{
	StudentTest st;
	st.Init("ZC", 19, 5201314);
	st.Print();

	return 0;
}
*/


/*
//类的实例化
//类本质只是一张图纸，只有依据图纸建造出房子才能真正占用物理空间
*/


/*
//类的大小计算
class t1
{
private:
	int a;
	int b;
	double c;

public:
	void func();
};

void t1::func()
{
	cout << "hello" << endl;
}

class t2{};

int main()
{
	cout << sizeof(t1) << endl; //并没有计算被调函数地址大小，还是结构体内存对齐后的大小
	
	//如果用同一个类创建多个成员变量，每个实例化对象都会保存代码一次
	//这样出现了空间的浪费
	//**所以 C++ 将成员函数存放在了公共代码段

	cout << sizeof(t2) << endl; //不会为0，而是为1
	//编译器留给空类一个字节标识其存在

	return 0;
}
*/


/*
//日期类
class Data
{
public:
	void Init(int year, int month, int day)
	{
		Data::year = year;
		//如果将成员变量取名为 year，初始化时写成 year = year;
		//是可以编译通过的，且采用就近原则，都为形参
		_month = month;
		_day = day;
	}

	void Print()
	{
		cout << Data::year << "-" << _month << "-" << _day << endl;
		//可以采用 :: 的方式表明 year 为成员变量
	}

private:
	int year;
	int _month;
	int _day;
};

int main()
{
	Data d1;
	d1.Init(2022, 9, 12);
	d1.Print();
	return 0;
}
*/


/*
//隐含this指针
class Data
{
public:
	void Init(int year, int month, int day) 
	//真实形参：void Init(Data* const this, int year, int month, int day)
	{
		//_year = year; 
		//_month = month;
		//_day = day;

		this->_year = year;
		this->_month = month;
		this->_day = day; //真实形式
		//也可用this来解决成员变量和形参相同的情况
	}

	void Print()
	//真实形参：void Print(Data* const this)
	{
		//cout << _year << "-" << _month << "-" << _day << endl;
		cout << this->_year << "-" << this->_month << "-" << this->_day << endl; //真实形式
		
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Data d1;
	Data d2;

	d1.Init(2022, 9, 12); //真实传参：d1.Init(&d1, 2022, 9, 12);
	d1.Print(); //真实传参：d1.Print(&d1);

	d2.Init(2022, 11, 10);
	d2.Print(); //对象存在独立空间，但Print函数的调用地址是相同的，可查看汇编

	//**this指针不允许被显示于调用
	//**this指针不允许被显示于函数声明
	//**this指针允许被适用于函数内部

	//**this指针严格来说是作为形参，存放在栈区
	//**有些编译器存放于寄存器(ecx/rcx)中，如vs
	return 0;
}
*/