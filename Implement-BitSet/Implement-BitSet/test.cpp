#define _CRT_CECURE_NOWARNINGS 1

#include "bitset.h"

int main()
{
	Thepale::BitSet<100> bs;
	bs.set(1);
	bs.set(18);
	bs.set(66);
	bs.set(91);

	bs.reset(66);

	cout << bs.test(1) << endl;
	cout << bs.test(18) << endl;
	cout << bs.test(66) << endl;
	cout << bs.test(91) << endl;


	return 0;
}