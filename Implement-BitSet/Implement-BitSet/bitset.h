#pragma once

#include <iostream>
using namespace std;

#include <vector>

namespace Thepale
{
	template<size_t N>
	class BitSet
	{
	public:
		BitSet()
		{
			_bs.resize(N / 8 + 1);
		}

		void set(size_t pos)
		{
			size_t index = pos / 8;
			size_t last = pos % 8;
			_bs[index] |= 1 << last;
		}

		void reset(size_t pos)
		{
			size_t index = pos / 8;
			size_t last = pos % 8;
			_bs[index] &= (~(1 << last));
		}

		bool test(size_t pos)
		{
			size_t index = pos / 8;
			size_t last = pos % 8;

			return (bool)(_bs[index] & (1 << last));
		}

	protected:
		vector<char> _bs;
	};
}
