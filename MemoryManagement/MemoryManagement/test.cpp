#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//New and Delete
/*
int main()
{
	//C语言
	int* p1 = (int*)malloc(sizeof(int));
	int* p2 = (int*)malloc(sizeof(int) * 5);

	free(p1);
	free(p2);
	p1 = nullptr;
	p2 = nullptr;

	//C++
	int* p3 = new int;
	int* p4 = new int[5];

	delete p3;
	delete[] p4;
	p3 = nullptr;
	p4 = nullptr;
	//**对于内置类型而言其实现完全一致，只是语法不同

	//C++98不支持new数组初始化，C++11支持
	int* p5 = new int(520); //C++98：可以在new时对单个变量初始化
	delete p5;
	p5 = nullptr;

	int* p6 = new int[5] {1, 2, 3}; //C++11：和对数组初始化一致
	delete[] p6;
	p6 = nullptr;

	return 0;
}
*/

/*
class Date
{
public:
	Date(int a = 0)
		:_a(a)
	{
		cout << "构造函数被调用" << endl;
	}

	Date(const Date& d)
	{
		cout << "拷贝构造函数被调用" << endl;
	}

	~Date()
	{
		cout << "析构函数被调用" << endl;
	}

private:
	int _a;
};

int main()
{
	Date* p1 = (Date*)malloc(sizeof(Date));
	Date* p2 = (Date*)malloc(sizeof(Date) * 5);

	free(p1);
	free(p2);
	p1 = nullptr;
	p2 = nullptr;

	//C++对于自定义类型的优势
	Date* p3 = new Date; //先申请空间，再调用构造函数初始化
	Date* p4 = new Date[5];

	delete p3; //先调用析构函数清理，再释放空间
	delete[] p4;
	p3 = nullptr;
	p4 = nullptr;

	return 0;
}
*/

/*
//C语言和C++错误处理的不同方式
int main()
{
	//C语言 - 返回错误码，指针返回空
	//char* p1 = (char*)malloc(1024 * 1024 * 1024 * 2);
	//if (p1 == nullptr)
	//{
	//	printf("%d\n", errno);
	//	perror("Malloc Failed:");
	//	exit(-1);
	//}

	//C++ - 抛异常
	//char* p2 = new char[1024 * 1024 * 1024 * 2 - 1]; //换成32位，64位崩不掉
	// - 直接报错

	//抛异常
	try
	{
		char* p2 = new char[1024 * 1024 * 1024 * 2 - 1];
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}

	return 0;
}
*/

//**new和delete的底层原理：
//new -> 是对 operator new 和 构造函数 的封装
//operator new -> 是对 malloc 和 抛异常 的封装

//delete -> 是对 operator delete 和 析构函数 的封装
//operator delete -> 是对 free 和 抛异常 的封装

//new[] -> 是对 new 的多次调用
//delete[] -> 是对 delete 的多次调用

/*
//重载operator new
class List
{
private:
	List* _next;
	int _val;
public:
	List(int val = 0)
		:_next(nullptr)
	{
		_val = val;
	}

	void* operator new(size_t val) //定性规定，必须为void* 和 size_t
	{
		cout << "void* operator new(size_t val)" << endl;
		return malloc(sizeof(List));
	}

	void PushBack(int val = 0)
	{
		List* newnode = new List(val);
	}
};

int main()
{
	List l;
	l.PushBack(1);
	l.PushBack(1);
	l.PushBack(1);
	l.PushBack(1);
	l.PushBack(1);
	l.PushBack(1);

	return 0;
}
*/


/*
//replacement new - 定位new
class A
{
private:
	int _val;
public:
	A(int val = 0)
	{
		_val = val;
		cout << "CALL A" << endl;
	}

	~A()
	{
		cout << "CALL ~A" << endl;
	}
};

int main()
{
	A* p1 = (A*)malloc(sizeof(A));
	new(p1)A(520); //new(address)[type](val); - 显式调用构造函数

	//1
	A* p2 = new A;
	//2
	A* p3 = (A*)operator new(sizeof(A));
	new(p3)A(520); 
	// 1 和 2 是等价的

	p2->~A(); //析构函数是可以显式调用的

	//1
	delete p2;
	//2
	p3->~A();
	operator delete(p3);
	// 1 和 2 是等价的

	return 0;
}
*/


//内存泄露的预防：
//1. 工程前期良好的设计规范，养成良好的编码规范，申请的内存空间记着匹配的去释放。ps：
//这个理想状态。但是如果碰上异常时，就算注意释放了，还是可能会出问题。需要下一条智
//能指针来管理才有保证。
//2. 采用RAII思想或者智能指针来管理资源。
//3. 有些公司内部规范使用内部实现的私有内存管理库。这套库自带内存泄漏检测的功能选项。
//4. 出问题了使用内存泄漏工具检测。