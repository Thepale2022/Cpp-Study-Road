#include <iostream>
using namespace std;

//Polymorphism - 多态

//静态多态：函数重载 - 看起来是调用同一个函数，但有不同行为
//“静态”：编译时实现

//动态多态：一个父类对象的引用或指针去调用同一个函数，传递不同的对象会调用不同的函数
//“动态”：运行时实现

///////////////////////////////////////////////////////////////////////////////////////////////////
//***动态多态两个必须条件：
//1.必须通过基类的指针或引用调用虚函数
//2.被调用的函数必须是虚函数，且派生类必须对基类的虚函数进行了重写
//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//	}
//};
//
//class Student :public Person
//{
//public:
//  //***类的非静态成员函数前加上virtual就是虚函数***
//	//***派生类中满足和基类三同（函数名、返回值、参数相同）的虚函数，称之为 函数重写（覆盖）***
//  //***重写返回值可以不相同的例外是：协变 - 要求返回值是父子关系的指针或引用***
//	virtual void BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//	}
//};
//
//void Func(Person& p)
//{
//	p.BuyTicket(); //多态 - 本质是不同的人做同一件事，结果不同
//}
//
//int main()
//{
//	Person P;
//	Func(P);
//
//	Student S;
//	Func(S);
//
//	return 0;
//}

//构成多态：传哪个类型的对象，调用的就是这个类型的虚函数 -- 跟对象有关
//不构成多态：调用的就是p类型的函数 -- 跟类型有关


///////////////////////////////////////////////////////////////////////////////////////////////////
//析构函数是虚函数可以构成重写
//class Father
//{
//public:
//	~Father()
//	//virtual ~Father()
//	{
//		cout << "~Father()" << endl;
//	}
//};
//
//class Son : public Father
//{
//public:
//	~Son()
//	//virtual ~Son()
//	{
//		cout << "~Son()" << endl;
//	}
//};
//
//int main()
//{
//	//*普通对象：是否是虚函数且是否构成重写都正确调用了析构（但不符合多态条件）*
//	//构成重写是因为析构函数都会被处理成 ~destructor()
//	//Father F;
//	//Son S;
//
//	//*new对象，父类指针管理*
//	Father* pf = new Father; //new = operator new + 构造
//	Father* ps = new Son; //*注意：这里是用Father指针管理的*
//
//	delete pf; //析构 + operator delete
//	delete ps;
//
//	//不构成多态则ps只会析构Father的部分（掉用 destructor()取决于类型了，二不是对象），Son的部分构造了但未处理
//	//构成多态则正常析构
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//虚函数的重写特殊情况
//class Person
//{
//public:
//	virtual void BuyTicket() //**允许只有父类是虚函数，且满足三同，就构成重写**
//	{
//		cout << "买票-全价" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	void BuyTicket() //*子类虽然没写virtual，但本质是先继承了父类虚函数的属性，再完成重写，可认为其也是虚函数*
//	//*其可以不加virtual的原因是因为析构函数，其设计初衷是 父类析构 + virtual 后，那么就不存在不构成多态的场景*
//  //*是为了防止子类的析构不写 virtual 导致不构成多态而内存泄漏*
//	{
//		cout << "买票-半价" << endl;
//	}
//};
//
//void Func(Person& P)
//{
//	P.BuyTicket();
//}
//
//int main()
//{
//	Person P;
//	Student S;
//
//	Func(P);
//	Func(S);
//
//	return 0;
//}

//ps：子类的虚函数就算定义为private也可以访问 - 本质是通过虚基表找到的
//权限只是编译器加于程序员的限制，本质对于通过虚基表调用没有影响