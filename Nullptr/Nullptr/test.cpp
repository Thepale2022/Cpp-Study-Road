#define _CRT_SECURE_NO_WARNINGS 1

//NULL - 是一个宏定义，为0
//nullptr - 是一个指针类型

#include <iostream>
using namespace std;

/*
void f(int)
{
	cout << "f(int)" << endl;
}

void f(int*)
{
	cout << "f(int*)" << endl;
}

int main()
{
	f(0);
	f(NULL); //这里NULL会被替换成0，所以都调了f(int)这个函数

	f(0);
	f(nullptr); //用nullptr则可以正常调用第二个

	return 0;
}
*/