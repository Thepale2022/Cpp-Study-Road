#define _CRT_SECURE_NO_WARNINGS 1

//namespace - 命名空间关键字
//:: - 域作用限定符

//目的：解决命名冲突问题
//C语言的两大问题：
//1.自己定义的变量和库里面的重名导致冲突
//2.项目较大多人分工时，出现代码命名冲突

#include <stdio.h>
#include <stdlib.h> // - 包含rand函数

/*
//int rand = 0; - rand重名

int main()
{
	printf("%d\n", rand); //这将打印地址，即stdlib库中rand函数的地址

	return 0;
}
*/


/*
int main()
{
	int rand = 1;
	printf("%d", rand); //这是可以打印和命名的，因为局部优先

	return 0;
}
*/


/*
namespace myspace //定义出一个域
{
	int rand = 520; //为全局变量，放在静态区
}

int main()
{
	printf("%d\n", myspace::rand); //-- 打印出520
	printf("%d\n", rand); //-- 打印出rand函数的地址

	return 0;
}
*/


/*
namespace myspace
{
	//命名空间可以定义变量/函数/结构体
	int rand = 1;

	int add(int x, int y)
	{
		return x + y;
	}

	struct student
	{
		char name[20];
		int age;
	};
}

int main()
{
	struct myspace::student st = { 0, 0 };//结构体是这样用的（先无需纠结C/C++特性）

	return 0;
}
*/


/*
int a = 10;
int main()
{
	int a = 20;
	printf("%d\n", a); //打印20，局部优先
	printf("%d\n", ::a); //打印10，定位到全局域

	return 0;
}
*/


/*
//命名空间的嵌套
namespace L1
{
	int a = 0;
	namespace L2
	{
		int a = 0;
		namespace L3
		{
			int a = 0;
		}
	}
}

int main()
{
	L1::a = 520;
	L1::L2::a = 1314;
	L1::L2::L3::a = 2003;

	printf("%d %d %d", L1::a, L1::L2::a, L1::L2::L3::a);

	return 0;
}
*/


/*
//重名的命名空间会被合并
namespace test
{
	int a = 0;
}

namespace test
{
	int b = 1;
}

int main()
{
	test::a = 1;
	test::b = 2;
	printf("%d %d", test::a, test::b);
}
*/


/*
namespace myspace
{
	int a = 0;
	struct Student
	{
		char name[20];
		int age;
	};
}

//using namespace myspace; //展开命名空间，但会失去隐形，隔离失效
using myspace::Student; //单独展开结构体（但::也可访问）

int main()
{
	myspace::a = 10;

	struct Student st = {"周全", 18};
	printf("%s;%d", st.name, st.age);

	//struct myspace::Student st = {"周全", 18};
	//printf("%s;%d", st.name, st.age);

	return 0;
}
*/