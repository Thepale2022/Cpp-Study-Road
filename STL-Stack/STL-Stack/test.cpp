#include <iostream>
#include <stack> //stack是容器适配器
using namespace std;

int main()
{
	//stack - 后进先出 - 不支持迭代器

	//st1
	stack<int> st1;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st1.push(4);
	st1.push(5);

	while (!st1.empty())
	{
		cout << st1.top() << " ";
		st1.pop();
	}
	cout << endl;

	return 0;
}