#define _CRT_SECURE_NO_WARNINGS 1

//auto - 在C++11标准之前，auto指创建自动变量，在函数结束时销毁
//但这是完全无意义的，即 auto int a = 0; 与 int a = 0; 完全一致
//C++11标准后，auto的功能是自动识别类型

#include <iostream>
using namespace std;

/*
int main()
{
	int a = 520;
	const char* b = "ZC";
	double c = 0.1314;

	auto i = a;
	auto j = b;
	auto k = c;

	//打印变量的类型
	cout << typeid(i).name() << endl;
	cout << typeid(j).name() << endl;
	cout << typeid(k).name() << endl;

	return 0;
}
*/


/*
int main()
{
	int a = 520;
	auto& b = a; //引用则必须加上&，不然无法判断是赋值还是取别名

	const int c = 1314;
	auto d = c; //auto会自动去掉const属性，若需要则写为const auto
	cout << typeid(d).name() << endl;
	const auto e = c;
	cout << typeid(e).name() << endl; //这里打印本质是错误的，因为e不可被修改
	//e = 10; - error

	return 0;
}
*/


/*
//void test(auto a){}

int main()
{
	//auto a; - auto不能单独声明再定义
	//a = 10;

	int b = 10;
	//test(b); - auto不能作参数类型，因为编译器无法识别auto不具体类型

	//auto arr[] = { 1,2,3 }; - auto不能定义数组

	return 0;
}
*/


/*
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };

	//语法糖
	for (auto e : arr) //本质是取arr的每个值赋值给e
	{
		cout << e;
	}
	cout << endl;

	//如果想改值，采用以下方式
	for (auto& e : arr)
	{
		e++; //e的生命周期只有一次循环，和i不同
	}
	//**其实本质和普通for循环一致，只是语法简单，所以不能在函数中使用

	return 0;
}
*/