#define _CRT_SECURE_NO_WARNINGS 1

//缺省参数 - 函数调用时参数的省略

#include <iostream>
using namespace std;

/*
void Func(int a = 0) //形参给了默认值
{
	cout << a << endl << endl;
}

int main()
{
	Func(); //不给参数，则默认值充当实参传给形参
	Func(1); //给参数，则把所传实参给形参
	return 0;
}
*/


/*
void Func(int a = 0, int b = 1, int c = 2) //全缺省参数
{
	cout << a << " " << b << " " << c << endl << endl;
}

int main()
{
	//以下为所有传参情况
	//需要注意的是，不可能只传 b 或者只传 c 或者只传 a 和 c
	Func();
	Func(520);
	Func(520, 1314);
	Func(520, 1314, 2003);
	//Func(, 520, ); //这是非法的

	return 0;  
}
*/


/*
void Func(int a, int b = 0, int c = 1) //半缺省参数
{
	cout << a << " " << b << " " << c << endl << endl;
}

//只能从右往左连续缺省，以下为错误写法
//void Func_error1(int a = 0, int b, int c);
//void Func_error2(int a, int b = 0, int c);
//void Func_error3(int a = 0, int b = 0, int c);

int main()
{
	//以下为所有传参情况
	//需要注意的是，形参 a 的值是必须给的
	Func(520);
	Func(520, 1314);
	Func(520, 1314, 2003);

	return 0;
}
*/


/*
//声明和定义中对缺省参数只能其一定义
//都定义会报错，建议定义在声明中
void Func(int a = 0);
//void Func(int a = 0) - error
//{
//	cout << a << endl;
//}
void Func(int a)
{
	cout << a << endl;
}

int main()
{
	Func();

	return 0;
}
*/