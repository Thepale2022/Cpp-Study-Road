#include <iostream>
#include <list> //本list是双向带头循环链表
using namespace std;

int main()
{
	//ls1
	list<int> ls1;
	ls1.push_back(1);
	ls1.push_back(2);
	ls1.push_back(3);
	ls1.push_back(4);
	ls1.push_back(5);

	list<int>::iterator it1 = ls1.begin();
	while (it1 != ls1.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;

	list<int>::reverse_iterator rit1 = ls1.rbegin();
	while (rit1 != ls1.rend())
	{
		cout << *rit1 << " ";
		rit1++;
	}
	cout << endl;

	ls1.reverse();
	for (auto e : ls1)
	{
		cout << e << " ";
	}
	cout << endl;

	//ls2
	list<int> ls2;
	ls2.push_back(1);
	ls2.push_back(2);
	ls2.push_back(2);
	ls2.push_back(3);
	ls2.push_back(3);
	ls2.push_back(5);
	ls2.push_back(1);
	ls2.push_back(1);

	ls2.sort();
	for (auto e : ls2)
	{
		cout << e << " ";
	}
	cout << endl;

	ls2.unique(); //去重 - 必须先排序
	for (auto e : ls2)
	{
		cout << e << " ";
	}
	cout << endl;

	//ls3
	list<int> ls3;
	ls3.push_back(10);
	ls3.push_back(40);
	ls3.push_back(30);
	ls3.push_back(40);
	ls3.push_back(50);

	ls3.remove(40);
	for (auto e : ls3)
	{
		cout << e << " ";
	}
	cout << endl;

	//ls4
	list<int> ls4;
	list<int> ls_src;
	ls4.push_back(1);
	ls4.push_back(2);
	ls4.push_back(3);
	ls4.push_back(4);
	ls4.push_back(5);

	ls_src.push_back(666);
	ls_src.push_back(666);
	ls_src.push_back(666);
	ls_src.push_back(666);
	ls_src.push_back(666);

	ls4.splice(++ls4.begin(),ls_src, ++ls_src.begin(), --ls_src.end()); //捻接
	for (auto e : ls4)
	{
		cout << e << " ";
	}
	cout << endl;
	for (auto e : ls_src)
	{
		cout << e << " ";
	}
	cout << endl;
	return 0;
}