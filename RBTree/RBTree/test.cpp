#include "red_black_tree.h"

int main()
{
	int arr1[] = { 5,4,3,2,1,0 };
	int arr2[] = { 16,3,7,11,9,26,18,14,15 };
	int arr3[] = { 4,2,6,1,3,5,15,7,16,14 };

	RBTree<int, int> rbt;

	for (auto e : arr3)
	{
		rbt.Insert(make_pair(e, e));
	}

	rbt.Inorder();
	cout << rbt.IsBalance();
	return 0;
}