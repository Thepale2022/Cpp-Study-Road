#pragma once

#include <iostream>
using namespace std;
 
//红黑树
//1.每个节点不是黑色就是红色
//2.根节点是黑色的
//3.红节点的子节点必须是黑色
//4.每条路径上的黑色节点数量相同
//5.每个叶子（这里指的是空节点，即叶子节点的NULL字节的）节点是黑色的，且不参与节点数量运算
//（即计算规律时，不带入空黑色节点，规律如下）

//***** 以上规则保证了红黑树的最长路径 <= 2 * 最短路径 *****

//设一条路径上黑色节点为 X 个
//则树高 h: x <= h <= 2x（假如X为最短路径，全黑，则高度最小为 x; 假如X为最长路径，一黑一红，则高度最大为 2x）
//以满二叉树做假设，则总节点个数 N 保持在：2^X - 1 <= N <= 2^2X - 1 
//解得，log(N+1)/2 <= X <= log(N+1) - log都以二为底，+1可省略，影响太小
//则高度所在范围为：log(N)/2 <= h <= 2logN - 取最大值可知，时间复杂度为：2logN

enum Color
{
	RED,
	BLACK
};

template <class K, class V>
struct RBTNode
{
	RBTNode(const pair<K, V>& kv = make_pair(K(), V()))
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_col()
	{}

	RBTNode<K, V>* _left;
	RBTNode<K, V>* _right;
	RBTNode<K, V>* _parent;
	pair<K, V> _kv;
	Color _col;
};

template <class K, class V>
class RBTree
{
	typedef RBTNode<K, V> Node;
public:
	RBTree()
		:_root(nullptr)
	{}

	bool Insert(const pair<K, V>& kv)
	{
		//root为空的情况
		if (!_root)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}

		//查找
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			parent = cur;

			if (kv.first > cur->_kv.first)
			{
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		//插入
		Node* newnode = new Node(kv);
		newnode->_col = RED; //默认给红色，规则3比规则4的代价小得多
		if (kv.first > parent->_kv.first)
		{
			parent->_right = newnode;
		}
		else if (kv.first < parent->_kv.first)
		{
			parent->_left = newnode;
		}
		else
		{
			return false;
		}
		newnode->_parent = parent;

		//控制平衡
		//情况1：newnode为红，parent为红，uncle为红，grandpa为黑
		//即，遇到连续的红节点，uncle存在且为红，grandpa一定是黑（不能存在连续红节点），触发修改
		//***调整方式：parent和uncle变黑，grandp变红（是为了保证作为子树时路径黑节点数量一致）***
		//调整后需要将grandpa当作newnode继续向上调整，因为可能出现连续红节点
		
		//情况2：newnode为红，parent为红，uncle不存在/存在且为黑，grandpa为黑
		//此时无法通过颜色改变实现红黑平衡，则需触发旋转
		//<1>且符合parent为grandpa左，newnode为parent左，则触发右单旋
		//旋转后parent为新根节点（或子树根）变黑，grandpa为新右子树节点变红，维持支路黑节点数量不变
		//<2>或符合parent为grandpa右，newnode为parent右，则触发左单旋
		//旋转后parent为新根节点（或子树根）变黑，grandpa为新左子树节点变红，维持支路黑节点数量不变

		//情况3：newnode为红，parent为红，uncle不存在/存在且为黑，grandpa为黑
		//此时无法通过颜色改变实现红黑平衡，则需触发旋转
		//<1>且符合parent为grandpa左，newnode为parent右，则触发左右双旋
		//旋转后cur为新根节点（或子树根）变黑，grandpa为新右子树节点变红，维持支路黑节点数量不变
		//<2>或符合parent为grandpa右，newnode为parent左，则触发右左双旋
		//旋转后cur为新根节点（或子树根）变黑，grandpa为新左子树节点变红，维持支路黑节点数量不变

		while (parent && parent->_col == RED) //父亲存在且为红，保证不是根节点（根节点必须是黑色）
		{
			Node* grandpa = parent->_parent;
			Node* uncle = grandpa->_left == parent ? grandpa->_right : grandpa->_left;

			if (uncle && uncle->_col == RED) //Case1: 叔叔存在且为红：变色+继续向上调整
			{
				parent->_col = BLACK;
				uncle->_col = BLACK;
				grandpa->_col = RED;

				newnode = grandpa;
				parent = newnode->_parent;
				continue;
			}
			else //Case2: uncle不存在或uncle为黑
			{
				//uncle为黑是由Case1变色后得到的
				if (newnode == parent->_left && parent == grandpa->_left) //双左则右单旋
				{
					Rotate_R(grandpa);
					grandpa->_col = RED;
					parent->_col = BLACK;
				}
				else if (newnode == parent->_right && parent == grandpa->_right) //双右则左单旋
				{
					Rotate_L(grandpa);
					grandpa->_col = RED;
					parent->_col = BLACK;
				}
				else if (newnode == parent->_right && parent == grandpa->_left)
				{
					Rotate_L(parent);
					Rotate_R(grandpa);
					newnode->_col = BLACK;
					grandpa->_col = RED;
				}
				else if (newnode == parent->_left && parent == grandpa->_right)
				{
					Rotate_R(parent);
					Rotate_L(grandpa);
					newnode->_col = BLACK;
					grandpa->_col = RED;
				}

				break;
			}
		}

		_root->_col = BLACK; //暴力处理，不管什么情况，根节点都变黑
		return true;
	}

	void Inorder()
	{
		_Inorder(_root);
	}

	bool IsBalance()
	{
		//验证规则1
		if (_root && _root->_col == RED)
		{
			cout << "根节点为红色" << endl;
			return false;
		}
		
		//验证规则4
		size_t banchmark = 0; //以最左路径为基准值
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
			{
				++banchmark;
			}

			cur = cur->_left;
		}

		//验证规则3、4
		return _IsBalance(_root, banchmark, 0);
	}

protected:
	void Rotate_R(Node* parent)
	{
		//记录需修改节点
		Node* curL = parent->_left;
		Node* curLR = curL->_right;

		//更改链接关系
		parent->_left = curLR;
		curL->_right = parent;

		//维护三叉链
		//1.维护curL
		if (parent == _root)
		{
			curL->_parent = nullptr;
			_root = curL;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curL;
			}
			else
			{
				ParentP->_right = curL;
			}
			curL->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curL;

		//3.维护curLR
		if (curLR != nullptr)
		{
			curLR->_parent = parent;
		}
	}
	void Rotate_L(Node* parent)
	{
		//记录需修改节点
		Node* curR = parent->_right;
		Node* curRL = curR->_left;

		//更改链接关系
		parent->_right = curRL;
		curR->_left = parent;

		//维护三叉链
		//1.维护curR
		if (parent == _root)
		{
			curR->_parent = nullptr;
			_root = curR;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curR;
			}
			else
			{
				ParentP->_right = curR;
			}
			curR->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curR;

		//3.维护curRL
		if (curRL != nullptr)
		{
			curRL->_parent = parent;
		}
	}

	void _Inorder(const Node* node)
	{
		if (!node)
		{
			return;
		}

		_Inorder(node->_left);
		cout << node->_kv.first << ":" << node->_kv.second << endl;
		_Inorder(node->_right);
	}

	bool _IsBalance(const Node* node, size_t banchmark, size_t blacknum = 0)
	{
		if (node == nullptr)
		{
			if (banchmark != blacknum)
			{
				cout << "存在路径黑色节点数量不相等" << endl;
				return false;
			}

			return true;
		}

		if (node->_col == RED && node->_parent->_col == RED)
		{
			cout << "出现连续的红色节点" << endl;
			return false;
		}

		if (node->_col == BLACK)
		{
			++blacknum;
		}

		return _IsBalance(node->_left, banchmark, blacknum) && _IsBalance(node->_right, banchmark, blacknum);
	}

protected:
	Node* _root;
};