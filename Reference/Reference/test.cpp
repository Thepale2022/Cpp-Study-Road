#define _CRT_SECURE_NO_WARNINGS 1

//& - 引用 - 给变量取别名，且不会开辟新的内存空间，而是共用内存空间

#include <iostream>
using namespace std; 

/*
int main()
{
	int a = 520;
	int& b = a; //类型& 引用变量名(对象名) = 引用实体;
	//**引用类型必须和引用实体是同种类型的
	//&a == &b

	return 0;
}
*/


/*
//引用在定义时必须初始化
int main()
{
	int a = 520;
	//int& b; b = a; -- error
	int& b = a;

	return 0;
}
*/


/*
//一个变量可以同时有多个引用
int main()
{
	int a = 520;
	int& b = a;
	int& c = a;
	int& d = b;

	printf("a:%p; b:%p; c:%p; d:%p\n", &a, &b, &c, &d);

	return 0;
}
*/


/*
//引用一旦引用一个实体，就不能再引用其他实体
int main()
{
	int a = 520;
	int& b = a;
	int c = 1314;

	b = c;//产生歧义：1.让 b 重新引用 c；×   2.把 c 赋值给 b √

	cout << &a << " " << &b << endl;
	return 0;
}
*/


//引用的应用
/*
//传引用调用
void swap(int& x, int& y)
{
	int temp = x;
	x = y;
	y = temp;
}

//传址调用
void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

//传值调用
//void swap(int x, int y)
//{
//	int temp = x;
//	x = y;
//	y = temp;
//}

//以上三个函数构成函数重载，但若同时存在
//调用传值或传引用会出现调用歧义

int main()
{
	int a = 520;
	int b = 1314;

	swap(a, b);
	cout << "a:" << a << " " << "b:" << b << endl;

	return 0;
}
*/


/*
//返回引用
int& Add(int x, int y)
{
	int c = x + y;
	return c;
}

int Sub(int x, int y)
{
	return x - y;
}

int main()
{
	int a = 520;
	int b = 1314;
	//传过来的是 c 的别名，但按道理 Add 函数结束 c 的空间已经被销毁
	//只是在VS环境下还给操作系统的空间没有被置随机	值，即没有修改
	//这里只是进行了读操作，虽然程序未崩溃，但实际上已经是非法内存访问
	int& ret = Add(a, b); 
	cout << ret << endl;

	//由于调用函数一样，栈帧开辟空间一样，在没有执行其他代码的情况下
	//再调用一次甚至能保证 c 再次创建的地址一模一样，所以 ret 非常巧合的又被更改为 30
	Add(10, 20);
	cout << ret << endl;

	//但如果调用其他函数，开辟空间不一样且覆盖到 c ,这样返回值就会是随机值
	//ps：cout也调用了函数，只是没有覆盖到 c
	Sub(10, 20);
	cout << ret << endl;

	return 0;
} //具体可见调试或研究汇编代码

//**如果函数返回时，出了函数作用域，如果返回对象还在（没有还给操作系统）
//**则可以使用引用返回，如果已经还给操作系统，则必须使用传值返回
*/


/*
//引用的价值 - 不生成拷贝
#include <time.h>
struct A
{
	int arr[10000]; //40000byte
};

A a;
A Test1()
{
	return a;
}

A& Test2()
{
	return a;
}

int main()
{
	int begin = 0;
	int end = 0;

	begin = clock();
	for(int i = 0; i < 100000; i++)
		Test1();
	end = clock();
	cout << "传值返回：" << end - begin << endl;

	begin = clock();
	for (int i = 0; i < 100000; i++)
		Test2();
	end = clock();
	cout << "传引用返回：" << end - begin << endl;

	return 0;
}
*/


/*
//常引用
void f(int& x)
{
	cout << x << endl;
}
int main()
{
	const int a = 0;
	//int& b = a; - 存在权限放大，不允许：a 只读，b 却可读可写

	const int& b = a; //权限不变

	int c = 0;
	const int& d = c; //权限缩小：c可读可写，b 只可读

	//f(a); - error
	//f(b); - error
	f(c);
	//f(d); - error
	//故最好写成 void f(const int& x)

	return 0;
}
*/ 