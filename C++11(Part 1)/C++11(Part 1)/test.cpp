#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#include <vector>
#include <list>
#include <map>
#include <initializer_list>

#include <array>
#include <forward_list>

void Unified_List_Initialization()
{
	struct Test
	{
		int _a;
		int _b;

		/*explicit*/ Test(int a, int b) //加上explicit可见不可通过编译
		//即花括号实际意义就是构造后赋值
			:_a(a)
			,_b(b)
		{
			cout << "Test(int a, int b)" << endl;
		}
	};

	int a = 0;
	int b = { 0 }; //C++11
	int c{ 0 }; //C++11
	int* pa = new int(1);
	int* pb = new int{ 1 }; //C++11

	int arr1[] = { 1,2,3,4,5 };
	int arr2[]{ 1,2,3,4,5 }; //C++11
	int* parr1 = new int[5];
	int* parr2 = new int[5] {1, 2, 3, 4, 5}; //C++11

	Test t1 = { 1, 2 }; //实际本身是调用构造函数后再赋值，编译器优化为直接赋值
	//这里一石二鸟，既兼容了C语言，也是C++11开拓的新特性
	Test t2{ 1, 2 }; //C++11
	Test* pt1 = new Test{ 1, 2 }; //C++11
	Test* pt2 = new Test[2]{ { 1, 2 }, { 3, 4 } };

	
	//同时，vector、list、map等类型，也可以用花括号初始化，实际上是支持了
	//initializer_list 初始化
	vector<int> v1 = { 1,2,3,4,5 };
	list<int> l1 = { 1,2,3,4,5 };
	map<int, int> m1 = { {1,1},{2,2},{3,3} };

	initializer_list<int> il = { 1,2,3,4,5 };
	vector<int> v2 = il;
	list<int> l2 = il;

	v2 = { 1,2,3 }; //也可赋值

	//***总而言之，花括号在初始化自定义类型会调用构造函数***
	//***初始化容器会被编译器处理成 initializer_list 初始化***
}

void Auto()
{
	auto x = 1; //自动识别类型

	//运用于范围for
	vector<string> v = { "I", "LOVE", "ZC" };
	for (auto e : v)
	{
		cout << e << ' ';
	}
}

void Decltype()
{
	double d = 5.201314;
	cout << typeid(d).name() << endl; //输出类型名

	decltype(d) dd = 9.9999; //推导d的类型并声明
}

void NewSTL()
{
	//array - 静态数组
	int arr1[10] = { 0 }; //C
	array<int, 10> arr2 = { 0 }; //C++11

	//arr1[14] = 666; //本质是转换为 *(arr1+14) 抽检
	//arr2[14] = 666; //本质是调用函数，arr2.operator[]() 必检


	//forward_list - 单链表
	forward_list<int> fl = { 1,2,3,4,5 };
	fl.push_front(666);
	for (auto e : fl) { cout << e << " "; } cout << endl;
	fl.pop_front();
	for (auto e : fl) { cout << e << " "; } cout << endl;
	fl.insert_after(fl.begin(), 520);
	for (auto e : fl) { cout << e << " "; } cout << endl;
	fl.erase_after(fl.begin());
	for (auto e : fl) { cout << e << " "; } cout << endl;

	
	//unordered_map && unordered_set 略
}

int main()
{
	Unified_List_Initialization(); //统一列表初始化

	Auto(); //自动推导类型

	Decltype(); //declare type - 声明类型

	//Nullptr(); //空指针（略）

	//For(); //范围for（略）

	NewSTL(); //STL中的新增容器

	return 0;
}