#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

void Lambda_Expression()
{
	//格式：[capture-list](parameters)mutable->return-type { statement };
	//例：两数相加
	[](int a, int b)->int { return a + b; };

	auto add1 = [](int a, int b)->int { return a + b; };
	cout << add1(520, 1314) << endl;

	//返回值类型明确时，可自动推导，返回值类型可省略
	auto add2 = [](int a, int b) { return a + b; };
	cout << add2(520, 1314) << endl;

	//无需参数时，参数列表可省略
	auto func = []{ cout << "func" << endl; };

	//[] 和 {} 是必须的


	//捕捉列表
	int a = 10;
	int b = 20;

	//捕捉的对象默认是 const 的，需要用 mutable 解除 const 属性，且有 mutable 必须带上参数列表
	auto swap1 = [a, b]()mutable 
	{
		int tmp = a;
		a = b;
		b = tmp;
	};
	swap1();
	cout << "a = " << a  << " " << "b = " << b << endl; //捕捉到的是值，所以无法实现改变

	auto swap2 = [&a, &b]
	{
		int tmp = a;
		a = b;
		b = tmp;
	};
	swap2();
	cout << "a = " << a << " " << "b = " << b << endl; //捕捉到的是引用，所以可以实现改变

	auto swap3 = [=]()mutable // [=] 传值捕捉所有变量
	{
		int tmp = a;
		a = b;
		b = tmp;
	};
	swap3();
	cout << "a = " << a << " " << "b = " << b << endl; //捕捉到的是值，所以无法实现改变

	auto swap4 = [&] // [&] 传引用捕捉所有变量
	{
		int tmp = a;
		a = b;
		b = tmp;
	};
	swap4();
	cout << "a = " << a << " " << "b = " << b << endl; //捕捉到的是值，所以无法实现改变

	//捕捉列表不能出现重复捕捉：[a, a] - error; [=, a] - error; 但可以 [=, &a] - right; [&, a] - right;
}

auto la = [](int a, int b) {}; //在全局范围内，lambda 表达式不可以捕捉任何变量（可以传参，请区分清楚）

///////////////////////////////////////////////////////////////////////////////////////////////////

void Lambda_Principle()
{
	auto Func = []() {};
	cout << typeid(Func).name() << endl; //本质是被处理为名为 class_uuid 的仿函数类

	//可参见汇编代码（注意：高版本编译器可能做了优化，不利于理解原理）
	class Compare1
	{
		bool operator() (int a, int b)
		{
			return a > b;
		}
	};

	auto Compare2 = [](int a, int b)->bool { return a > b; };
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void CPP11_Class()
{
	//Final
	//1.设定不允许被继承的类
	class End final {};
	//class Cur : public End {}; - error 不允许的继承

	//2.设定不允许重写的虚函数
	class Basic
	{
		virtual void Func() final {}
	};

	class Now : public Basic
	{
		//virtual void Func(); - error 不允许的重写
	};


	//Override
	//必须重写基类的 override 函数
	class Father
	{
		virtual void Func() {}
	};

	class Son : public Father
	{
		void Func() override {} //保证此函数必须重写父类函数，否则报错
	};


	//Default
	//强制生成默认构造函数
	class T1
	{
	public:
		T1(int a) :_a(a) {} //写了构造函数则不会生成默认构造函数

		//如果以创建 T1 对象不赋值则会报错，即强制生成默认构造函数
		T1() = default;

		//其他构造同理
	private:
		int _a;
	};

	T1 t1_1(1);
	T1 t1_2; //有了 default 后是合法的


	//delete
	//禁止调用对应函数
	class T2
	{
	public:
		T2() = delete; //禁止调用该函数

		T2(int a) :_a(a) {}
	private:
		int _a;
	};

	T2 t2_1(1);
	//T2 t2_1; //error 禁止调用

///////////////////////////////////////////////////////////////////////////////////////////////////
	//delete - 详细讲解“禁止拷贝”
	//在 C++98 中，如果想要禁止拷贝，可采取的最直接方法就是定义为 private
	class NoCopy_1
	{
	public:
		NoCopy_1(int data = 0) : _data(data) {}

	private:
		NoCopy_1(const NoCopy_1& obj) { cout << "NoCopy_1(const NoCopy_1& obj)" << endl; }

		int _data = 0;
	};

	NoCopy_1 obj1_1(520);
	//NoCopy_1 obj1_2obj1); //error - 私有对象不可访问

///////////////////////////////////////////////////////////////////////////////////////////////////
	//但如果出现这种成员函数呢？
	class NoCopy_2
	{
	public:
		NoCopy_2(int data = 0) : _data(data) {}

		void ILoveCopy(const NoCopy_2& obj) //*无法阻止拷贝*
		{
			*this = obj;
		}

	private:
		NoCopy_2(const NoCopy_2& obj) { cout << "NoCopy_2(const NoCopy_2& obj)" << endl; }

		NoCopy_2& operator= (const NoCopy_2& obj)
		{
			NoCopy_2 tmp(obj);
			swap(tmp._data, _data);
			return *this;
		}

		int _data = 0;
	};

	NoCopy_2 obj2_1(520);
	NoCopy_2 obj2_2;
	obj2_2.ILoveCopy(obj2_1); //还是可以调用到拷贝构造

///////////////////////////////////////////////////////////////////////////////////////////////////	
	//接下↓：
}
//可以将其只声明不定义 - 即无法调用	
class NoCopy_3
{
public:
	NoCopy_3(int data = 0) : _data(data) {}

	NoCopy_3(const NoCopy_3& obj);

private:
	int _data = 0;
};

//但出现这种情况：
NoCopy_3::NoCopy_3(const NoCopy_3& obj)
{
	cout << "外定义" << endl;
}

//就算定义为 private 也防止不了外定义后内调用

//所以 C++11 的 delete 实现了它的价值 - 禁止调用

///////////////////////////////////////////////////////////////////////////////////////////////////

void CPP11_NewConstructor()
{
	//默认移动构造和默认移动赋值重载
	//1.不能有拷贝构造、拷贝复制重载、析构的任何一种
	//2.默认移动构造对内置类型实行逐字节拷贝，对自定义类型，若有移动构造则调用移动构造，没有则调用拷贝构造
	class String
	{
	public:
		String(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size + 1) //算上 '\0'
		{
			_str = new char[_capacity];
			strcpy(_str, str);
		}

		String(const String& str)
		{
			cout << "String(const String& str) - 深拷贝" << endl;
		}

		String(const String&& str)
		{
			cout << "String(const String&& str) - 资源转移" << endl;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
	class MyClass
	{
	public:
		MyClass()
			:_str("")
			,_a(1)
		{}

	private:
		String _str;
		int _a;
	};
		
	MyClass test1;
	MyClass test2(move(test1));
}

void Initialize_Member_List()
{
	class T
	{
	public:
		T(int n = 888, char ch = 't')
			//:_n(n)
			//, _ch(ch)
		{
			//_n = n;
			//_ch = ch;
		}
	private:
		int _n = 1;
		char _ch = 'a'; //C++11 允许这样给缺省值初始化

		//在 private 里面只是声明，并没有定义出变量，若是构造函数未完成初始化，则会使用 private 中的缺省值初始化
		//如果构造函数已经初始化，则这里的缺省值没有意义（必须是构造函数执行完毕才会使用 private 的缺省值）
	};

	T t;
}

int main()
{
	//Lambda_Expression();

	//Lambda_Principle();

	//CPP11_Class();

	//CPP11_NewConstructor();

	Initialize_Member_List();

	return 0;
}