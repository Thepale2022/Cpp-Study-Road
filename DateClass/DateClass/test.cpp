#define _CRT_SECURE_NO_WARNINGS 1

#include "date.h"

int main()
{
	Date d1(2022, 9, 12);
	Date d2(2022, 11, 16);
	
	cout << (d1 - d2) << endl;
	cout << (d2 - d1) << endl;

	d1.PrintWeekDay();
	d2.PrintWeekDay();

	Date test(1, 1, 1);
	test.PrintWeekDay();

	//d1 << cout; //因为第一个必须传对象，所以衍生出这种奇怪写法
	
	//友元函数
	cout << d1 << d2;
	cin >> d1 >> d2;
	cout << d1 << d2;
	return 0;
}