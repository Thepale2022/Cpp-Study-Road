#define _CRT_SECURE_NO_WARNINGS 1

//inline - 内联，会把函数在调用时展开（Release下），不会调用新的堆栈（无CALL）
//可以类比为 C 语言的宏定义：#define Add(x, y) ((x)+(y))

#include <iostream>
using namespace std;

/*
inline int Add(int x, int y) //Debug下看不到，因为Debug重于展示程序细节，不考虑性能（可通过属性设置显示）
{
	return x + y;
}

int main()
{
	int ret = Add(520, 1314);

	return 0;
}
*/

//**适用于函数被多次调用的情况
//**inline本质是以空间换时间（长函数和递归函数不适合展开）
//**inline只是给编译器的建议，具体还要看编译器自己的优化
//**内联函数不能声明和定义分离，因为调用内联函数时已经被展开
//在*.o 的符号表中无法生成函数映射的地址