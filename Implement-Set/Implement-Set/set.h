#pragma once

#include "red_black_tree-plus.h"

namespace Thepale
{
	template <class K>
	class set
	{
	public:
		struct KeyofSet
		{
			const K& operator() (const K& key)
			{
				return key;
			}
		};

		bool Insert(const K& key)
		{
			return _rbt.Insert(key);
		}

		//������ - ��ʱδ����const
		typedef typename RBTree<K, K, KeyofSet>::Iterator Iterator;

		Iterator begin()
		{
			return _rbt.begin();
		}

		Iterator end()
		{
			return _rbt.end();
		}

	protected:
		RBTree<K, K, KeyofSet> _rbt;
	};
}