#include "set.h"

int main()
{
	Thepale::set<int> set1;

	set1.Insert(1);
	set1.Insert(2);
	set1.Insert(3);
	set1.Insert(4);
	set1.Insert(5);

	Thepale::set<int>::Iterator it = set1.begin();
	while (it != set1.end())
	{
		cout << *it << " ";
		++it;
	}

	return 0;
}