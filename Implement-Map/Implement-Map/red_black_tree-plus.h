#pragma once

#include <iostream>
using namespace std;
 
//红黑树
//1.每个节点不是黑色就是红色
//2.根节点是黑色的
//3.红节点的子节点必须是黑色
//4.每条路径上的黑色节点数量相同
//5.每个叶子（这里指的是空节点，即叶子节点的NULL字节的）节点是黑色的，且不参与节点数量运算
//（即计算规律时，不带入空黑色节点，规律如下）

//***** 以上规则保证了红黑树的最长路径 <= 2 * 最短路径 *****

//设一条路径上黑色节点为 X 个
//则树高 h: x <= h <= 2x（假如X为最短路径，全黑，则高度最小为 x; 假如X为最长路径，一黑一红，则高度最大为 2x）
//以满二叉树做假设，则总节点个数 N 保持在：2^X - 1 <= N <= 2^2X - 1 
//解得，log(N+1)/2 <= X <= log(N+1) - log都以二为底，+1可省略，影响太小
//则高度所在范围为：log(N)/2 <= h <= 2logN - 取最大值可知，时间复杂度为：2logN


enum Color
{
	RED,
	BLACK
};

template <class T>
struct RBTNode
{
	RBTNode(const T& data = T(), Color col = RED)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_data(data)
		,_col(col)
	{}

	RBTNode<T>* _left;
	RBTNode<T>* _right;
	RBTNode<T>* _parent;
	T _data;
	Color _col;
};

template <class T, class Ref, class Ptr>
class RBTree_Iterator
{
	typedef RBTNode<T> Node;
	typedef RBTree_Iterator<T, Ref, Ptr> Self;

public:
	//构造
	RBTree_Iterator(Node* node)
		:_node(node)
	{}

	//运算符重载
	Ref operator* ()
	{
		return _node->_data;
	}

	Ptr operator-> ()
	{
		return &_node->_data;
	}

	Self& operator++ ()
	{
		if (_node->_right)
		{
			Node* left = _node->_right;
			while (left->_left)
			{
				left = left->_left;
			}
			_node = left;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_right == cur)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}

		return *this;
	}

	Self& operator-- ()
	{
		if (_node->_left)
		{
			Node* right = _node->_left;
			while (right->_right)
			{
				right = right->_right;
			}
			_node = right;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_left == cur)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}

		return *this;
	}

	bool operator != (const Self& obj)
	{
		return _node != obj._node;
	}

protected:
	Node* _node;
};

template <class K, class T, class KeyofT>
class RBTree
{
public:
	typedef RBTNode<T> Node;
	typedef RBTree_Iterator<T, T&, T*> Iterator;
public:
	RBTree()
		:_root(nullptr)
	{}

	RBTree(const RBTree<K, T, KeyofT>& tree)
	{
		_root = Copy(tree._root);
		_root->_parent = nullptr;
	}
	
	~RBTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	pair<Iterator, bool> Insert(const T& data)
	{
		KeyofT kot;
		//root为空的情况
		if (!_root)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(Iterator(_root), true);
		}

		//查找
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			parent = cur;

			if (kot(data) > kot(cur->_data))
			{
				cur = cur->_right;
			}
			else if (kot(data) < kot(cur->_data))
			{
				cur = cur->_left;
			}
			else
			{
				return make_pair(Iterator(cur), false);
			}
		}

		//插入
		Node* newnode = new Node(data);
		Node* ret = newnode; //返回时用
		newnode->_col = RED; //默认给红色，规则3比规则4的代价小得多
		if (kot(data) > kot(parent->_data))
		{
			parent->_right = newnode;
		}
		else if (kot(data) < kot(parent->_data))
		{
			parent->_left = newnode;
		}
		else
		{
			return make_pair(Iterator(newnode), false);
		}
		newnode->_parent = parent;

		//控制平衡
		//情况1：newnode为红，parent为红，uncle为红，grandpa为黑
		//即，遇到连续的红节点，uncle存在且为红，grandpa一定是黑（不能存在连续红节点），触发修改
		//***调整方式：parent和uncle变黑，grandp变红（是为了保证作为子树时路径黑节点数量一致）***
		//调整后需要将grandpa当作newnode继续向上调整，因为可能出现连续红节点
		
		//情况2：newnode为红，parent为红，uncle不存在/存在且为黑，grandpa为黑
		//此时无法通过颜色改变实现红黑平衡，则需触发旋转
		//<1>且符合parent为grandpa左，newnode为parent左，则触发右单旋
		//旋转后parent为新根节点（或子树根）变黑，grandpa为新右子树节点变红，维持支路黑节点数量不变
		//<2>或符合parent为grandpa右，newnode为parent右，则触发左单旋
		//旋转后parent为新根节点（或子树根）变黑，grandpa为新左子树节点变红，维持支路黑节点数量不变

		//情况3：newnode为红，parent为红，uncle不存在/存在且为黑，grandpa为黑
		//此时无法通过颜色改变实现红黑平衡，则需触发旋转
		//<1>且符合parent为grandpa左，newnode为parent右，则触发左右双旋
		//旋转后cur为新根节点（或子树根）变黑，grandpa为新右子树节点变红，维持支路黑节点数量不变
		//<2>或符合parent为grandpa右，newnode为parent左，则触发右左双旋
		//旋转后cur为新根节点（或子树根）变黑，grandpa为新左子树节点变红，维持支路黑节点数量不变

		while (parent && parent->_col == RED) //父亲存在且为红，保证不是根节点（根节点必须是黑色）
		{
			Node* grandpa = parent->_parent;
			Node* uncle = grandpa->_left == parent ? grandpa->_right : grandpa->_left;

			if (uncle && uncle->_col == RED) //Case1: 叔叔存在且为红：变色+继续向上调整
			{
				parent->_col = BLACK;
				uncle->_col = BLACK;
				grandpa->_col = RED;

				newnode = grandpa;
				parent = newnode->_parent;
				continue;
			}
			else //Case2: uncle不存在或uncle为黑
			{
				//uncle为黑是由Case1变色后得到的
				if (newnode == parent->_left && parent == grandpa->_left) //双左则右单旋
				{
					Rotate_R(grandpa);
					grandpa->_col = RED;
					parent->_col = BLACK;
				}
				else if (newnode == parent->_right && parent == grandpa->_right) //双右则左单旋
				{
					Rotate_L(grandpa);
					grandpa->_col = RED;
					parent->_col = BLACK;
				}
				else if (newnode == parent->_right && parent == grandpa->_left)
				{
					Rotate_L(parent);
					Rotate_R(grandpa);
					newnode->_col = BLACK;
					grandpa->_col = RED;
				}
				else if (newnode == parent->_left && parent == grandpa->_right)
				{
					Rotate_R(parent);
					Rotate_L(grandpa);
					newnode->_col = BLACK;
					grandpa->_col = RED;
				}

				break;
			}
		}

		_root->_col = BLACK; //暴力处理，不管什么情况，根节点都变黑
		return make_pair(Iterator(ret), false);;
	}

	Iterator Find(const K& key) //传K的原因（有T能取出对象，取不出类型）
	{
		Node* cur = _root;
		KeyofT kot;
		while (cur)
		{
			if (key > kot(cur->_data))
			{
				cur = cur->_right;
			}
			else if (key < kot(cur->_data))
			{
				cur = cur->_left;
			}
			else
			{
				return Iterator(cur);
			}
		}

		return Iterator(nullptr);
	}

	void Inorder()
	{
		_Inorder(_root);
	}

	//拷贝
	RBTree<K, T, KeyofT>& operator= (RBTree<K, T, KeyofT> tree)
	{
		swap(_root, tree._root);
		return *this;
	}

	//迭代器
	Iterator begin()
	{
		Node* left = _root;
		while (left && left->_left)
		{
			left = left->_left;
		}

		return Iterator(left);
	}
	Iterator end()
	{
		return Iterator(nullptr);
	}

	Iterator rbegin()
	{
		Node* right = _root;
		while (right && right->_right)
		{
			right = right->_right;
		}

		return Iterator(right);
	}
	Iterator rend()
	{
		return Iterator(nullptr);
	}

protected:
	void Rotate_R(Node* parent)
	{
		//记录需修改节点
		Node* curL = parent->_left;
		Node* curLR = curL->_right;

		//更改链接关系
		parent->_left = curLR;
		curL->_right = parent;

		//维护三叉链
		//1.维护curL
		if (parent == _root)
		{
			curL->_parent = nullptr;
			_root = curL;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curL;
			}
			else
			{
				ParentP->_right = curL;
			}
			curL->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curL;

		//3.维护curLR
		if (curLR != nullptr)
		{
			curLR->_parent = parent;
		}
	}
	void Rotate_L(Node* parent)
	{
		//记录需修改节点
		Node* curR = parent->_right;
		Node* curRL = curR->_left;

		//更改链接关系
		parent->_right = curRL;
		curR->_left = parent;

		//维护三叉链
		//1.维护curR
		if (parent == _root)
		{
			curR->_parent = nullptr;
			_root = curR;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curR;
			}
			else
			{
				ParentP->_right = curR;
			}
			curR->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curR;

		//3.维护curRL
		if (curRL != nullptr)
		{
			curRL->_parent = parent;
		}
	}

	Node* Copy(Node* node)
	{
		if (node == nullptr)
		{
			return nullptr;
		}

		Node* newnode = new Node(node->_data, node->_col);
		newnode->_left = Copy(node->_left);
		newnode->_right = Copy(node->_right);
		
		if (newnode->_left)
		{
			newnode->_left->_parent = newnode;
		}
		
		if (newnode->_right)
		{
			newnode->_right->_parent = newnode;
		}

		return newnode;
	}
	void Destroy(Node* node)
	{
		if (!node)
		{
			return;
		}

		Destroy(node->_left);
		Destroy(node->_right);

		delete node;
		node = nullptr;
	}

protected:
	Node* _root;
};