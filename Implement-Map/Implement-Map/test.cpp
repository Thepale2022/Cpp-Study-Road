#include "map.h"

int main()
{
	//map1
	cout << "map1:" << endl;
	Thepale::map<int, int> map1;
	map1.Insert(make_pair(1, 1));
	map1.Insert(make_pair(2, 2));
	map1.Insert(make_pair(3, 3));
	map1.Insert(make_pair(4, 4));
	map1.Insert(make_pair(5, 5));

	auto it1 = map1.begin();
	while (it1 != map1.end())
	{
		cout << it1->first << ":" << it1->second << endl;
		++it1;
	}
	cout << endl;

	auto rit1 = map1.rbegin();
	while (rit1 != map1.rend())
	{
		cout << rit1->first << ":" << rit1->second << endl;
		--rit1;
	}
	cout << endl;

	Thepale::map<int, int>::Iterator ret = map1.Find(4);
	cout << ret->first << ":" << ret->second << endl;

	
	//map2
	cout << "map2:" << endl;
	Thepale::map<int, int> map2;

	map2[1] = 1;
	map2[2] = 2;
	map2[3] = 3;
	map2[4] = 4;
	auto it2 = map2.begin();
	while (it2 != map2.end())
	{
		cout << it2->first << ":" << it2->second << endl;
		++it2;
	}
	cout << endl;


	//map3
	cout << "map3:" << endl;
	Thepale::map<int, int> map3(map2);

	map3[3] = 666;

	auto it3 = map3.begin();
	while (it3 != map3.end())
	{
		cout << it3->first << ":" << it3->second << endl;
		++it3;
	}
	cout << endl;
	return 0;
}