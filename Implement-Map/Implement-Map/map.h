#pragma once

#include "red_black_tree-plus.h"

namespace Thepale
{
	template<class K, class V>
	class map
	{
	public:
		struct KeyofValue
		{
			const K& operator() (const pair<K, V>& kv)
			{
				return kv.first;
			}
		};

		typedef typename RBTree<K, pair<K, V>, KeyofValue>::Iterator Iterator;

		//迭代器
		Iterator begin()
		{
			return _rbt.begin();
		}

		Iterator end()
		{
			return _rbt.end();
		}

		Iterator rbegin()
		{
			return _rbt.rbegin();
		}

		Iterator rend()
		{
			return _rbt.rend();
		}

		//函数接口
		pair<Iterator, bool> Insert(const pair<K, V>& kv)
		{
			return _rbt.Insert(kv);
		}

		Iterator Find(const K& key)
		{
			return _rbt.Find(key);
		}

		//运算符重载
		V& operator[] (const K& key)
		{
			pair<Iterator, bool> ret = _rbt.Insert(make_pair(key, V()));
			return ret.first->second;
		}
	protected:
		RBTree<K, pair<K, V>, KeyofValue> _rbt;
	};
}
