#pragma once

#include <iostream>
using namespace std;

#include <assert.h>

template <class K, class V>
struct AVLTreeNode
{
	AVLTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_bf(0)
	{}

	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf; //balance factor - 平衡因子
};

template <class K, class V>
class AVLTree
{
public:
	//typedef
	typedef AVLTreeNode<K, V> Node;

public:
	AVLTree() : _root(nullptr) {}

	bool Insert(const pair<K, V>& kv)
	{
		//根为空的情况
		if (!_root)
		{
			_root = new Node(kv);
			return true;
		}

		//不为空则查找
		Node* parent = nullptr;
		Node* cur = _root;

		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		//插入
		cur = new Node(kv);
		if (kv.first > parent->_kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		//控制平衡
		//1.更新平衡因子
		//五种情况：1.左加节点，父平衡因子--
		//			2.右加节点，父平衡因子++
		//			3.更新后，父平衡因子为0，更新结束 - 说明更新后parent所在子树高度不变，不会影响上层
		//即更新前为-1或1，此更新填补了矮的部分，但未对高度产生影响，故平衡因子不会变化
		//			4.更新后，父平衡因子为-1/+1，更新继续 - 说明更新后parent所在子树高度变化，影响上层
		//即更新前为0，此更新必然改变树的高度，则父平衡因子会受影响，故平衡因子会变化
		//			5.更新后，父平衡因子为-2/+2，更新停止 - 说明parent所在子树已经不平衡，需要旋转处理
		while (parent)
		{
			if (cur == parent->_left)
			{
				parent->_bf--; //1
			}
			else
			{
				parent->_bf++; //2
			}

			if (parent->_bf == 0) //3 - 更新结束
			{
				return true;
			}
			else if (parent->_bf == 1 || parent->_bf == -1) //4 - 继续往上更新
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2) //5 - 旋转
			{
				//右单旋
				if (parent->_bf == -2 && cur->_bf == -1)
				{
					Rotate_SR(parent);
				}
				//左单旋
				else if (parent->_bf == 2 && cur->_bf == 1)
				{
					Rotate_SL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					Rotate_DLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					Rotate_DRL(parent);
				}
				else
				{
					assert(false);
				}
				break; //旋转后就不用继续更新了，所以要跳出
			}
			else
			{
				assert(false); //不可能出现此情况，说明插入更新平衡因子之前就出现了问题
			}
	
		}
		//2.若有异常平衡因子，则旋转平衡树
		return true;
	}

	void InOrder()
	{
		_InOrder(_root);
	}

	//这个函数是为了找出双旋问题，双旋必须对平衡因子重新更新
	bool IsBalance()
	{
		return _IsBalance(_root);
	}

protected:
	void Rotate_SR(Node* parent)
	{
		//记录需修改节点
		Node* curL = parent->_left;
		Node* curLR = curL->_right;

		//更改链接关系
		parent->_left = curLR;
		curL->_right = parent;

		//更新平衡因子
		curL->_bf = 0;
		parent->_bf = 0;

		//维护三叉链
		//1.维护curL
		if (parent == _root)
		{
			curL->_parent = nullptr;
			_root = curL;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curL;
			}
			else
			{
				ParentP->_right = curL;
			}
			curL->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curL;

		//3.维护curLR
		if (curLR != nullptr)
		{
			curLR->_parent = parent;
		}
	}

	void Rotate_SL(Node* parent)
	{
		//记录需修改节点
		Node* curR = parent->_right;
		Node* curRL = curR->_left;

		//更改链接关系
		parent->_right = curRL;
		curR->_left = parent;

		//更新平衡因子
		curR->_bf = 0;
		parent->_bf = 0;

		//维护三叉链
		//1.维护curR
		if (parent == _root)
		{
			curR->_parent = nullptr;
			_root = curR;
		}
		else
		{
			Node* ParentP = parent->_parent;
			if (ParentP->_left == parent)
			{
				ParentP->_left = curR;
			}
			else
			{
				ParentP->_right = curR;
			}
			curR->_parent = ParentP;
		}

		//2.维护parent
		parent->_parent = curR;

		//3.维护curRL
		if (curRL != nullptr)
		{
			curRL->_parent = parent;
		}
	}

	void Rotate_DLR(Node* parent)
	{
		//通过curLR的平衡因子判断情况，-1是curLR左插入，1是curLR右插入，0是插入的就是curLR
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		int bf = curLR->_bf;

		Rotate_SL(parent->_left);
		Rotate_SR(parent);

		if (bf == 1)
		{
			parent->_bf = 0;
			curL->_bf = -1;
			curLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void Rotate_DRL(Node* parent)
	{
		//通过curRL的平衡因子判断情况，-1是curRL左插入，1是curRL右插入，0是插入的就是curRL
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		int bf = curRL->_bf;

		Rotate_SR(parent->_right);
		Rotate_SL(parent);

		if (bf == 1)
		{
			parent->_bf = -1;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			curR->_bf = 1;
			curRL->_bf = 0;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	int Height(Node* node)
	{
		if (node == nullptr)
		{
			return 0;
		}

		int left = Height(node->_left);
		int right = Height(node->_right);

		return left > right ? left + 1 : right + 1; //再次告诫自己，递归重思想不重过程！
	}

	void _InOrder(Node* node)
	{
		if (node == nullptr)
		{
			return;
		}

		_InOrder(node->_left);
		cout << node->_kv.first << ":" << node->_kv.second << endl;
		_InOrder(node->_right);
	}

	bool _IsBalance(Node* node)
	{
		if (node == nullptr)
		{
			return true;
		}

		int height_left = Height(node->_left);
		int height_right = Height(node->_right);

		if (height_right - height_left != node->_bf)
		{
			cout << node->_kv.first << ":" << node->_kv.second << " 此节点平衡因子错误！" << endl;
			cout << "原平衡因子为：" << node->_bf << endl;
			cout << "正确的平衡因子为：" << height_right - height_left << endl;
			return false;
		}

		//递归冗余，可考虑后序递归
		return ((height_right - height_left) < 2 || (height_right - height_left) > -2) && _IsBalance(node->_left) && _IsBalance(node->_right);

	//有病可以实现一下删除
	}
private:
	Node* _root;
};