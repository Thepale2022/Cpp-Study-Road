#include <iostream>
#include <string>
using namespace std;

//假设要定义一个学校信息管理系统
//则需要定义：学生类、老师类等等

//class Student
//{
//private:
//	string _name;
//	string _id;
//	int _age;
//	string _address;
//
//	string _dorm;
//	string _major;
//};
//
//class Teacher
//{
//private:
//	string _name;
//	string _id;
//	int _age;
//	string _address;
//
//	string _position;
//	string _major;
//};

//以上可发现，大多数成员变量都是相同的，只有极个别是不同的
//则实现成员函数时必然也有大多数内容是冗余重复的

//则引出：父类/基类 和 子类/派生类 的概念

//class Person //父类
//{
//	string _name;
//	string _id;
//	int _age;
//	string _address;
//	string _major;
//};
//
////class [子类] : [继承方式] [父类]
//class Student : public Person //子类（对父类的继承）
//{
//private:
//	string _dorm;
//};
//
//class Teacher : public Person //子类（对父类的继承）
//{
//private:
//	string _position;
//};

//继承是类设计层次的复用


//继承方式：
//父类public + public继承 = 子类public
//父类public + private继承 = 子类private
//父类public + protected继承 = 子类protected

//父类protected + public继承 = 子类protected
//父类protected + private继承 = 子类private
//父类protected + protected继承 = 子类protected
 
//父类private + public继承 = 子类不可见
//父类private + private继承 = 子类不可见
//父类private + protected继承 = 子类不可见

//规律（不包括private）：父类的成员在子类的访问方式 == Min（取访问限定符和继承方式中小的那一个）
//public > protected > private


//不可见 - 父类的私有成员还是继承到了子类，但语法上限制
//子类对象不管在类内还是类外都不能访问他
//*这里体现了 private 和 protected 的区别*

//class Father
//{
//private:
//	int _a;
//};
//
//class Son : public Father
//{
//public:
//	void Func()
//	{
//		_a; // error - 存在但不可见（不可使用）
//	}
//};

//total:
//常规继承（父类的直接一动不动的给子类）：public继承
//父类继承给子类只能在子类类内用：protected继承
//只想父类自己用，继承子类不可用：private继承

//不写访问限定符，class默认private继承，struct默认public继承


///////////////////////////////////////////////////////////////////////////////////////////////////
//赋值兼容规则/切割/切片

//class Father
//{
//protected:
//	int _father = 0;
//};
//
//class Son : public Father
//{
//protected:
//	int _son = 0;
//};
//
//int main()
//{ 
//	Father f;
//	Son s;
//
//	//赋值兼容（仅限于public继承）
//	f = s; //只对父类的那部分成员切割赋值给父类
//	Father* ptr = &s; //只管理父类成员的部分
//	Father& ref = s; //同指针
//
//	//*不可以把父类赋值给子类*
//
//	//不存在类型转换，是语法天然支持的行为
//	
//	//仅限于public继承的原因：
//	//假如父类有protected和public，用private继承下去都变成了子类的private
//	//而子类给父类则是把private的成员给protected和public
//	//存在不合理的权限转换
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//class Father
//{
//protected:
//	int _a = 520;
//};
//
//class Son : public Father
//{
//public:
//	void Print()
//	{
//		cout << "_a = " << _a << endl;
//	}
//
//protected:
//	int _a = 1314;
//};
//
//int main()
//{
//	Son s;
//	s.Print(); //就近原则，访问子类_a
//	//这一特性被称为 隐藏 或 重定义（子类隐藏了父类的成员）
//
//	return 0;
//}

//class Father
//{
//public:
//	void func()
//	{
//		cout << "void func()" << endl;
//	}
//};
//
//class Son : public Father
//{
//public:
//	void func(int a)
//	{
//		cout << "void func(int a)" << endl;
//	}
//};
//
////1.Father::func 和 Son::func 不构成函数重载，函数重载的前提是在同一作用域
////2.Father::func 和 Son::func 构成函数隐藏（只要函数名相同，和参数无关）
//
//int main()
//{
//	Son s;
//	//s.func(); - 隐藏无法访问父类func
//	s.Father::func(); //指定类域访问
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//调用逻辑
//class Father
//{
//public:
//	Father()
//	{
//		cout << "Father()" << endl;
//	}
//
//	Father(const Father& F)
//	{
//		_f = F._f;
//		cout << "Father(const Father& F)" << endl;
//	}
//
//	~Father()
//	{
//		cout << "~Father()" << endl;
//	}
//
//protected:
//	int _f = 520;
//};
//
//class Son : public Father
//{
//public:
//	Son()
//	{
//		cout << "Son()" << endl;
//	}
//
//	Son(const Son& S)
//	{
//		_s = S._s;
//		cout << "Son(const Son& S)" << endl;
//	}
//
//	~Son()
//	{
//		cout << "~Son()" << endl;
//	}
//
//protected:
//	int _s = 1314;
//};
//
//int main()
//{
//	Son s1; //Father() -> Son() -> ~Son() -> ~Father()
//
//	cout << endl;
//
//	Son s2(s1); //Father(const Father& F) -> Son(const Son& S)
//
//	cout << endl;
//
//	return 0;
//
//	//total：继承下来的调用父类处理，自己的按普通类基本规则处理
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//必须自己写的情况：
//1.父类没有默认构造函数（自己实现构造）
//class Father
//{
//public:
//	Father(int f) //*这是构造函数但不是默认构造函数，默认构造不传参！*
//	{
//		_f = f;
//	}
//protected:
//	int _f;
//};
//
//class Son : public Father
//{
//public:
//	//Son(int s = 0) //error，调不到父类的默认构造函数
//	//{
//	//	_s = s;
//	//}
//	
//	Son(int f = 0, int s = 0)
//		:Father(f) //**必须调用父类的构造处理！不能在子类中初始化父类！**
//		,_s(s)
//	{}
//
//protected:
//	int _s;
//};
//
//int main()
//{
//	Son(520, 1314);
//
//	return 0;
//}

//2.子类有资源需要释放（自己实现析构）
//3.子类存在浅拷贝问题（自己实现拷贝和赋值）
//class Father
//{
//public:
//	Father(size_t size)
//		:_size(size)
//	{
//		_strf = new int[size];
//		cout << "Father(size_t size)" << endl;
//	}
//
//	Father(const Father& F)
//	{
//		_strf = new int[F.size()];
//		cout << "Father(const Father& F)" << endl;
//	}
//
//	~Father() //释放空间
//	{
//		delete[] _strf;
//		_strf = nullptr;
//		cout << "~Father()" << endl;
//	}
//
//	size_t size() const
//	{
//		return _size;
//	}
//
//protected:
//	int* _strf;
//	size_t _size;
//};
//
//class Son : public Father
//{
//public:
//	Son(size_t size1, size_t size2)
//		:Father(size1)
//		, _size(size2)
//	{
//		_strs = new int[size2];
//		cout << "Son(size_t size1, size_t size2)" << endl;
//	}
//
//	//Son(const Son& S) - //error，调不到父类的默认构造函数 (3)
//	//{
//	//	_strs = new int[S.size()];
//	//} 
//
//	Son(const Son& S)
//		:Father(S) //这里传过去默认就切片了，是把S中的父类成员传给了F
//	{
//		_strs = new int[S.size()];
//		cout << "Son(const Son& S)" << endl;
//	}
//
//	~Son()
//	{
//		//Father::~Father(); - 析构函数不需要显式调用，子类析构结束时会自动调用父类析构
//		//**reason：保证析构顺序（构造是先构造父类再构造子类，析构应该先析构子类再析构父类）**
//		
//		//ps：父类析构不能被显式调用，因为析构函数名字会被统一处理成destructor()，构成函数隐藏
//		delete[] _strs;
//		_strs = nullptr;
//		cout << "~Son()" << endl;
//	}
//
//	size_t size() const
//	{
//		return _size;
//	}
//
//protected:
//	int* _strs;
//	size_t _size;
//};
//
//int main()
//{
//	Son s1(10, 20);
//	Son s2(s1);
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//友元关系不能继承
//class Son;
//class Father
//{
//public:
//	friend void Func(const Father& F, const Son& S);
//protected:
//	int _f = 520;
//};
//
//class Son : public Father
//{
//public:
//	//friend void Func(const Father& F, const Son& S); - 也要加上这行才行
//protected:
//	int _s = 1314;
//};
//
//void Func(const Father& F, const Son& S)
//{
//	cout << F._f << endl;
//	cout << S._s << endl; //父类的友元不是子类的友元
//}
//
//int main()
//{
//	Father f;
//	Son s;
//	Func(f, s);
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//静态成员变量继承后仍使用同一空间
//class Father
//{
//public:
//	Father() { ++_count; cout << _count << endl; }
//protected:
//	static int _count;
//};
//
//int Father::_count = 0;
//
//class Son : public Father{};
//
//class Grandson : public Son{};
//int main()
//{
//	Father f;
//	Son s;
//	Grandson g;
//
//	return 0;
//}


///////////////////////////////////////////////////////////////////////////////////////////////////
//单继承 - 一个子类只有一个直接父类
//class Grandparent {};
//class Parent : public Grandparent {};
//class Son : public Grandparent {};

//多继承 - 一个子类有两个或以上的直接父类
//class Parent1 {};
//class Parent2 {};
//class Son : public Parent1, public Parent2 {};

//菱形继承 - 多继承的特殊情况
//class Grandparent {};
//class Parent1 : public Grandparent {};
//class Parent2 : public Grandparent {};
//class Son : public Parent1, public Parent2 {}; //存在数据冗余和二义性（存在两份Grandparent）

//虚继承 - 解决菱形继承的数据冗余和二义性问题
//class Grandparent {};
//class Parent1 : virtual public Grandparent {};
//class Parent2 : virtual public Grandparent {};
//class Son : public Parent1, public Parent2 {};


//class A //虚基类
//{
//public:
//	int _a;
//};
//
////class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
////class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	//请分析内存
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//
//	//未经虚继承处理
//	//0x0000007B386FFC88  01 00 00 00  ....
//	//0x0000007B386FFC8C  03 00 00 00  ....
//	//0x0000007B386FFC90  02 00 00 00  ....
//	//0x0000007B386FFC94  04 00 00 00  ....
//	//0x0000007B386FFC98  05 00 00 00  ....
//
//	//虚继承处理
//	//0x0000000A588FF808  20 bc b9 04 ?? .
//	//0x0000000A588FF80C  f7 7f 00 00 ?...
//	//0x0000000A588FF810  03 00 00 00 .... //d._b
//	//0x0000000A588FF814  cc cc cc cc ????
//	//0x0000000A588FF818  28 bc b9 04 (??.
//	//0x0000000A588FF81C  f7 7f 00 00 ?...
//	//0x0000000A588FF820  04 00 00 00 .... //d._c
//	//0x0000000A588FF824  cc cc cc cc ????
//	//0x0000000A588FF828  05 00 00 00 .... //d._d
//	//0x0000000A588FF82C  cc cc cc cc ????
//	//0x0000000A588FF830  02 00 00 00 .... //*d._a变成了公共区域*
//
//	//0x00007FF704B9BC20  00 00 00 00 .... //d._b中储存的偏移量
//	//0x00007FF704B9BC24  28 00 00 00 (... //0x0000000A588FF830 - 0x0000000A588FF808 = 28
//
//	//0x00007FF704B9BC28  00 00 00 00 .... //d._c中储存的偏移量
//	//0x00007FF704B9BC2C  18 00 00 00 .... //0x0000000A588FF830 - 0x0000000A588FF818 = 18
//
//	//A一般称为虚基类
//	//通过B或C找A，需要通过虚基表中的偏移量进行计算
//
//	return 0;
//}


//ps:
//class Grandparent {};
//class Parent1 : virtual public Grandparent {};
//class Son1 : public Parent1 {};
//class Parent2 : virtual public Grandparent {};
//class END : public Son1, public Parent2 {};
//这种情况虚继承应该在Parent1和Parent2上
//（数据冗余和二义性是Grandparent，虚继承应在继承虚基类的派生类上）


///////////////////////////////////////////////////////////////////////////////////////////////////
//继承和组合
//继承 - 适用于is-a：学生和人，学生具备人的特征，学生是个人
//class A
//{
//	int _a;
//};
//
//class B : public A
//{
//	int _b;
//};

//组合 - 适用于has-a：眼睛和头，眼睛是头的一部分，眼睛不是头
//class C
//{
//	int _c;
//};
//
//class D
//{
//	C _obj;
//	int _d;
//};

//*既具有 is-a 又具有 has-a 时，优先使用组合*


///////////////////////////////////////////////////////////////////////////////////////////////////
//拓展：
//继承，通过派生类复用时一种白箱复用 - 内部细节可见
//组合，时一种黑箱复用 - 内部细节不可见

//白箱测试复杂度很高，需要涉及到代码覆盖率，对测试要求很高，即根据代码逻辑测试代码
//黑箱测试复杂度一般，一般而言是根据功能进行代码测试

//一定程度而言，继承破坏了基类的封装
//就以上ABCD四类而言，B可以访问A的公有和保护，而D只能访问C的公有

//类之间、模块之间，最好保证低耦合，高内聚
//而结合能保证耦合率低（和基类的保护成员无关，故耦合率低，关联性弱）
//（可了解UML设计）