#define _CRT_SECURE_NO_WARNINGS 1

//类的6个默认成员函数

#include <iostream>
using namespace std;


//构造函数 - 负责完成成员变量的初始化
 
/*
//**特征：
//1.函数名于类名一致（一模一样，不带任何类型）
//2.无返回值
//3.对象实例化时编译器自动调用对应的构造函数
//4.构造函数可以重载
class Date
{
public:
	Date() //1
	{
		_year = 0;
		_month = 1;
		_day = 1;
	} //2

	Date(int year, int month, int day) //4
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1; //4
	d1.Print();

	Date d2(2022, 9, 12); //第二种初始化方式
	d2.Print();
	return 0;
}
*/


//5. 如果类中没有显式定义构造函数，则C++编译器会自动生成一个无参的默认构造函数
//一旦用户显式定义构造函数编译器将不再生成

/*
class Date
{
public:
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1; //这时其实已经调用了C++自动生成的无参默认构造函数
	//但是调试发现，d1的值没有任何变化，原因是：
	//C++把类型分成内置类型(基本类型)和自定义类型
	//内置类型就是语言提供的数据类型，如int/char/float/double/数组/指针等
	//自定义类型就是我们自己定义的，如struct/class等
	
	//**C++生成的无参默认构造函数对内置类型不初始化，对自定义类型初始化
	//**对自定义类型成员变量初始化的方式，就是去调用其成员变量的默认构造函数初始化
	//**如果没有默认构造函数就会报错
	 
	//****默认构造函数就是不需要参数就可以调用的函数，不一定是C++自动生成的
	//也可以是自己定义的无参或全缺省构造函数
	
}
*/

/*
class A
{
public:
	// -- 1
	//A()
	//{
	//	cout << "无参构造函数被调用" << endl;
	//	_member = 0;
	//}

	// -- 2
	//A(int a = 0)
	//{
	//	cout << "全缺省构造函数被调用" << endl;
	//	_member = 0;
	//}

	// -- 3
	//A(int member)
	//{
	//	cout << "有参构造函数被调用" << endl;
	//	_member = member;
	//}
private:
	int _member;
};

class Date
{
public:
private:
	int _year;
	int _month;
	int _day;

	A a;
};

int main()
{
	Date d1;
	//如果只放出 1，正常执行：进入Date d1->无默认构造函数->C++自动生成无参默认构造函数
	//->不初始化内置类型->遇到自定义类型->进入A _a->找到无参构造函数->调用

	//如果只放出 2，正常执行：进入Date d1->无默认构造函数->C++自动生成无参默认构造函数
	//->不初始化内置类型->遇到自定义类型->进入A _a->找到全缺省构造函数->调用

	//如果只放出 3，编译报错：进入Date d1->无默认构造函数->C++自动生成无参默认构造函数
	//->不初始化内置类型->遇到自定义类型->进入A _a->构造函数已经被显式构造，C++不生成
	//->找不到无参默认构造函数->报错

	//如果都不放出，正常执行：进入Date d1->无默认构造函数->C++自动生成无参默认构造函数
	//->不初始化内置类型->遇到自定义类型->进入A _a->无默认构造函数->C++自动生成无参默认构造函数
	//->不初始化内置类型->正常执行

	//1，2不可同时放出，会有二义性；1，3 / 2，3同时放出都会执行1 / 2

	return 0;
}

//****再次强调：默认构造函数是不需要参数可以调用的，这和无参是有区别的
//因为全缺省可以有参，但是不需要参数可以调用
*/


//析构函数 - 完成对象中资源的清理工作

//**特征：
//1.析构函数名是在类名前加上字符 ~
//2.无参数无返回值类型
//3.一个类只能有一个析构函数
//若未显式定义，系统会自动生成默认的析构函数
//注意：析构函数不能重载
//4. 对象生命周期结束时，C++编译系统系统自动调用析构函数

/*
class Date
{
public:
	~Date()
	{
		cout << "析构函数被调用" << endl;
		//Date类无资源需要释放
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1;
	return 0;
}
*/

/*
class Stack
{
public:
	Stack(size_t capacity = 4)
	{
		int* temp = (int*)malloc(sizeof(int) * capacity);
		if (temp == nullptr)
		{
			cout << "Malloc Failed!" << endl;
			exit(-1);
		}

		_arr = temp;
		_top = 0;
		_capacity = 0;
	}

	~Stack()
	{
		free(_arr);
		_arr = nullptr;
		_top = 0;
		_capacity = 0;
	}
	
private:
	int* _arr;
	size_t _top;
	size_t _capacity;
};

int main()
{
	Stack st; //调试可见malloc申请和释放空间过程

	return 0;
}

//**同构造函数，如果在类中有自定义类型，析构时会调用自定义类型的析构
*/


//拷贝构造函数

/*
//**特征：
//1.拷贝构造函数是构造函数的一个重载形式
//2.拷贝构造函数的参数只有一个且必须是类类型对象的引用
//使用传值方式编译器直接报错，因为会引发无穷递归调用
class Date
{
public:
	Date()
	{
		_year = 0;
		_month = 1;
		_day = 1;
	}
	Date(Date& d) //拷贝构造函数
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	//如果这里写成：Date(Date d){}
	//会导致无限递归的拷贝构造
	//原理：给对象d2传值d1，于是如果line239进行拷贝构造
	//调用拷贝构造先生成了形参对象d，又把实参d1传给形参d
	//即又是一个拷贝构造，无限循环；所以必须加引用！
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1;
	Date d2(d1);

	return 0;
}
*/

/*
//3.若未显式定义，编译器会生成默认的拷贝构造函数，默认的拷贝构造函数对象
//按内存存储按字节序完成拷贝，这种拷贝叫做浅拷贝，或者值拷贝
//**默认拷贝函数对所有内置类型进行浅拷贝，对自定义类型则调用其默认拷贝构造函数
class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1;
	d1.Init(2022, 9, 12);
	d1.Print();

	Date d2(d1); //默认拷贝构造 - 始终注意是字节序的拷贝（单字节单字节的拷贝）
	d2.Print();

	return 0;
}

//**自己构造拷贝构造函数是至关重要的，见如下情况：
class Stack
{
public:
	Stack(size_t capacity = 4)
	{
		int* temp = (int*)malloc(sizeof(int) * capacity);
		if (temp == nullptr)
		{
			cout << "Malloc Failed!" << endl;
			exit(-1);
		}

		_arr = temp;
		_top = 0;
		_capacity = 0;
	}

	~Stack()
	{
		free(_arr);
		_arr = nullptr;
		_top = 0;
		_capacity = 0;
	}
private:
	int* _arr;
	size_t _top;
	size_t _capacity;
};

int main()
{
	//Stack st1;
	//Stack st2(st1); //这个时候st1和st2的int*都指向同一空间
	//但是如果对st1操作，st2也会同时变动，对st2操作也是同理

	//如果再加上析构函数，会直接报错
	Stack st1;
	Stack st2(st1); 
	//同一块空间被free了两次，具体来说是第二次free了nullptr

	//**由此可见，浅拷贝（默认拷贝）是完全不够的
	return 0;
}
*/


//运算符重载
//C++为了增强代码的可读性引入了运算符重载，运算符重载是具有特殊函数名的函数
//也具有其返回值类型，函数名字以及参数列表，其返回值类型与参数列表与普通的函数类似
//函数名字为：关键字operator后面接需要重载的运算符符号
//函数原型：返回值类型 operator操作符(参数列表)

//**特征：
//1.不能通过连接其他符号来创建新的操作符：比如operator@
//2.重载操作符必须有一个类类型参数
//3.用于内置类型的运算符，其含义不能改变，例如：内置的整型 + ，不能改变其含义
//4.作为类成员函数重载时，其形参看起来比操作数数目少1，因为成员函数的第一个参数为隐藏的this
//5.(.*) (::) (sizeof) (?:) (.) 注意以上5个运算符不能重载

/*
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

//private: // - 为了防止operator>函数访问不到成员变量
	int _year;
	int _month;
	int _day;
};

bool operator> (const Data& d1, const Data& d2)
{
	if (d1._year > d2._year)
	{
		return true;
	}
	else if (d1._year == d2._year && d1._month > d2._month)
	{
		return true;
	}
	else if (d1._year == d2._year && d1._month == d2._month && d1._day > d2._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main()
{
	Date d1(2022, 9, 12);
	Date d2(2022, 11, 11);

	cout << (d1 > d2) << endl; //根据参数类型进行了运算符重载
	cout << operator> (d1, d2) << endl; //可以就当作函数调用，但为了代码可读性所以写成上一种
	return 0;
}
*/

/*
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//方法1：
	int GetYear()
	{
		return _year;
	}
	int GetMonth()
	{
		return _month;
	}
	int GetDay()
	{
		return _day;
	}

private: // - 不注释此行，要保证代码的封装性
	int _year;
	int _month;
	int _day;
};

bool operator> (Date& d1, Data& d2) //如果形参写成 const Date& d1, 那么需要修饰this指针为const的
//因为调用GetYear传了隐式this指针，会导致权限放大
//	写成 Date& const d1，实际上和 Date& d1没有区别
{
	if (d1.GetYear() > d2.GetYear())
	{
		return true;
	}
	else if (d1.GetYear() == d2.GetYear() && d1.GetMonth() > d2.GetMonth())
	{
		return true;
	}
	else if (d1.GetYear() == d2.GetYear() && d1.GetMonth() == d2.GetMonth() && d1.GetDay() > d2.GetDay())
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main()
{
	Date d1(2022, 9, 12);
	Date d2(2022, 11, 11);

	cout << (d1 > d2) << endl; //根据参数类型进行了运算符重载
	cout << operator> (d1, d2) << endl; //可以就当作函数调用，但为了代码可读性所以写成上一种
	return 0;
}
*/

/*
//定义Get函数在成员变量多的时候可能不方便
//故也可以把运算符重载定义在类里面
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	
	bool operator> (Date& d) //看似一个参数，实则是两个参数，还有一个隐含的this指针
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year == d._year && _month > d._month)
		{
			return true;
		}
		else if (_year == d._year && _month == d._month && _day > d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private: 
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 9, 12);
	Date d2(2022, 11, 11);

	cout << (d2 > d1) << endl;

	return 0;
}
*/

//赋值运算符重载

/*
//首先弄清赋值运算符是否有返回值
//int main()
//{
//	int a, b, c;
//	a = b = c = 520; //从右至左赋值
//	//可以发现，赋值运算符返回了左操作数
//	return 0;
//}

class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 9, 12);
	Date d2;
	d2 = d1; //1.如果没有构造赋值运算符重载，C++会生成默认的赋值运算符重载
	//依然是对内置类型进行字节序的浅拷贝，对自定义类型调用其赋值运算符重载
	return 0;
}
*/

/*
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(Date& d)
	{
		cout << "拷贝构造函数被调用" << endl; //拷贝构造函数被调用的原因是：
		//程序返回的是Date对象，即拷贝一份Date对象的值返回，所以调用了拷贝构造函数
	}

	Date operator= (Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;

		return *this;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 9, 12);
	Date d2;
	Date d3;
	d2 = d1; //调试查看

	return 0;
}
*/

/*
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(Date& d)
	{
		cout << "拷贝构造函数被调用" << endl;
	}

	Date& operator= (Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;

		return *this;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 9, 12);
	Date d2;
	Date d3;

	d3 = d2 = d1; //传引用返回不会调用拷贝构造函数

	return 0;
}
*/

/*
//一些问题：
class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(Date& d)
	{
		cout << "拷贝构造函数被调用" << endl;
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	Date& operator= (Date& d)
	{
		cout << "赋值运算符重载被调用" << endl;
		_year = d._year;
		_month = d._month;
		_day = d._day;

		return *this;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 9, 12);
	//以下是调用了拷贝构造函数
	Date d2(d1);
	Date d3 = d1; //一切对将要实例化的对象进行赋值都是调用拷贝构造函数

	//以下是调用了赋值运算符重载
	Date d4;
	d4 = d1;

	Date d5, d6;
	d5 = d6 = d1;

	d1 = d1; //这种情况可以酌情优化

	//**不允许把赋值运算符重载定义在全局，因为编译器默认优先在类中查找
	//类中没有则会创建默认赋值运算符重载，产生冲突

	return 0;
}
*/


/*
//const 成员函数
class Date
{
private:
	int _year;
	int _month;
	int _day;

public:
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print() const // - 修饰this指针为const Date* const this（如果定义声明分开，则都要加）
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
};

int main()
{
	Date d1(2022, 9, 12);
	d1.Print();

	const Date d2(2022, 11, 16);
	//d2.Print(); - error - 从 const Date* -> Date*

	//究其原因其实是this指针，d2传参实际是：d2.Print(&d2);
	//&d2的类型是：const Date*，Print默认接收的this指针是：Date* const this
	//首先明白，const this的根本目的是为了防止this指针被修改
	//其次知晓，const Date*是防止this指针指向的数据被修改
	
	//解决方案：函数后面加const，表示修饰this指针为const Date* const this
	d2.Print();
	return 0;
}
*/

//友元函数 - 举例见Date日期类