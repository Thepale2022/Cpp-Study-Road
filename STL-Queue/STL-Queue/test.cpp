
#include <iostream>
#include <queue> //queue是容器适配器

using namespace std;

int main()
{
	//queue - 先进先出 - 不支持迭代器

	//qu1
	queue<int> qu1;
	qu1.push(1);
	qu1.push(2);
	qu1.push(3);
	qu1.push(4);
	qu1.push(5);

	while (!qu1.empty())
	{
		cout << qu1.front() << " ";
		qu1.pop();
	}
	cout << endl;

	return 0;
}