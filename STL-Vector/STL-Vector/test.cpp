#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include <vector>
#include <algorithm>
 
int main()
{
	//构造
	vector<int> v1; //全缺省
	vector<int> v2(10, 1); //指定值初始化
	vector<int> v3(v2.begin() + 1, v2.end() - 1); //迭代器区间初始化
	vector<int> v4(v3); //拷贝初始化


	//成员函数
	vector<int> v5(10, 6);
	v5.push_back(5201314); //尾插
	for (auto e : v5)
	{
		cout << e << " ";
	}
	cout << endl;

	v5.pop_back();
	for (auto e : v5)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << v5.size() << endl; //大小
	cout << v5.capacity() << endl; //容量
	cout << v5.empty() << endl; //判空
	cout << v5.max_size() << endl; //可控的最长序列
	cout << v5.front() << endl; //取头数据
	cout << v5.back() << endl; //取尾数据

	v5.reserve(128); //扩容
	cout << v5.capacity() << " " << v5.size() << endl;
	v5.resize(256, 666); //扩容+初始化
	cout << v5.capacity() << " " << v5.size() << endl;
	v5.resize(10, 666); //缩小尺寸不缩小容量
	cout << v5.capacity() << " " << v5.size() << endl;

	vector<int> v6(10, 123);
	v6.assign(5, 666); //相当于 resize
	for (auto e : v6)
	{
		cout << e << " ";
	}
	cout << endl;
	v6.assign(v6.begin() + 1, v6.end() - 1); //迭代器区间分配空间
	for (auto e : v6)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v7(10, 123); //find包含在算法头文件中
	vector<int>::iterator ret = find(v7.begin(), v7.end(), 123); //C++迭代器区间一定是左闭右开
	if (ret != v7.end())
	{
		cout << "Find it" << endl;
	}

	vector<int> v8(10, 123);
	vector<int>::iterator it = v8.begin();
	v8.insert(it, 666);
	//v8.erase(it); - 迭代器失效问题
	v8.clear(); //清除所有数据但不销毁空间


	//迭代器
	vector<int> v9(10, 520);
	vector<int>::iterator b = v9.begin();
	while (b != v9.end())
	{
		cout << *b;
		b++;
	}

	return 0;
	}