#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

/*
//构造函数的初始化列表初始化
//**特征：
//1.每个成员变量在初始化列表中只能出现一次(初始化只能初始化一次)
//2.类中包含以下成员，必须放在初始化列表位置进行初始化
// const 引用 没有默认构造函数的自定义函数成员变量
//3.初始化的顺序由声明决定，和初始化列表内顺序无关
class Date
{
public:
	//初始化列表 - 成员变量定义的地方
	Date(int year, int month, int day, int n)
		:_year(year)
		, _month(month)
		, _day(day)
		, _N(n) //可以在初始化列表时初始化const修饰的常变量
		//**构造函数体中的语句只能将其称为赋初值，而不能称作初始化
		//**因为初始化只能初始化一次，而构造函数体内可以多次赋值
	{}

private: //声明
	int _year;
	int _month;
	int _day;
	const int _N;
};

//****可以默认初始化列表是默认存在的，可以人为操作
//其固定发生在成员变量定义的阶段

//声明 - 告诉编译器成员变量类型和名字
//定义 - 给变量分配空间
//初始化 - 如果在定义时赋值，则称为初始化，不然为默认随机值
int main()
{
	Date d(2022, 9, 12, 666);
	return 0;
}
*/


/*
//explicit - 禁止类型转换
class Date
{
public:
	explicit Date(int year)
		:_year(year)
	{}
private:
	int _year;
};

int main()
{
	double a = 1.11;
	int b = a; //隐式类型转换

	int c = 1;
	int* p = &c;
	//int d = p; - error
	int d = (int)p; //强制类型转换
	
	//隐式类型转换：相似类型
	// - 生成被需要转换成类型的临时变量，且临时变量具有常性，支持手动或自动转换
	//强制类型转换：无关类型
	// - 生成被需要转换成类型的临时变量，且临时变量具有常性，但必须手动转换

	Date d1 = 2022; //实际上是一种隐式类型转换
	//有explicit则不能转换，无则可以转换
	//无explicit具体过程：本来用2022构造一个临时对象Date(2022)，再用这个对象拷贝构造d2
	//但是C++编译器在连续的一个过程中，多个构造会被优化，合二为一
	//所以这里被优化为直接就是一个构造
	Date d2(2022);

	return 0;
}
*/


/*
//static成员
class Date
{
public:
	Date(int year = 1)
		:_year(year)
	{
		_sCount++;
	}
	Date(const Date& d)
	{
		_year = d._year;
		_sCount++;
	}

	int GetCount() //1
	{
		return _sCount;
	}
	//static int _sCount;

	static int sGetCount()
	{
		return _sCount;
	}

private:
	int _year;
	static int _sCount;
};
int Date::_sCount = 0; //必须在类外定义静态成员变量

int main()
{
	Date d1;
	Date d2;
	Date d3 = d1;

	//放开为public的读取方式：
	//cout << Date::_sCount << endl; //可直接用类读取
	//cout << d1._sCount << endl; //也可用对象读取
	//** static成员不属于任何一个函数，所以可以用类直接读取
	//就算用对象读取，也只是定位到类的方式而已
	
	//隐藏为private的读取方式：
	//1.用函数读取：
	cout << d1.GetCount() << endl; //本质上this->_sCount访问的就是类中_sCount
	//2.用static函数读取：
	cout << Date::sGetCount() << endl;
	cout << d1.sGetCount() << endl;
	//** static成员函数和static成员变量不属于任何一个对象，而是属于类
	//同样，可用类直接访问，也可通过对象定位到类访问
	//被static修饰的函数没有this指针，不能访问非静态成员

	return 0;
}
*/


/*
//友元
//**特征：
//1.友元函数可访问类的私有和保护成员，但不是类的成员函数
//2.友元函数不能用const修饰
//3.友元函数可以在类定义的任何地方声明，不受类访问限定符限制
//4.一个函数可以是多个类的友元函数
//5.友元函数的调用与普通函数的调用原理相同
class Date
{
private:
	int _year;
public:
	Date(int year = 1)
	{
		_year = year;
	}

	friend int FriendGetYear(Date& d);
	int GetYear();
};

int FriendGetYear(Date& d)
{
	return d._year; 
}

//int NoFriendGetYear(Date& d)
//{
//	return d.year; //不在类内不可访问私有，但友元可以
//}

int Date::GetYear()
{
	return _year;
}

int main()
{
	Date d;
	cout << FriendGetYear(d) << endl; //直接调用访问
	cout << d.GetYear() << endl; //通过对象访问
	return 0;
}
*/

/*
//友元类
class Date
{
private:
	int _year;
	int _month;
	int _day;

public:
	friend class Time; //Date把Time当朋友，Time可以访问Date，但Date不能访问Time
	//因为Time没有把Date当朋友，Date是Time的舔狗
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year),
		_month(month),
		_day(day)
	{}

};

class Time
{
private:
	int _hour;
	int _min;
	int _sec;
	Date _d; //为Date的友元类所以可以直接访问_d的私有和保护
public:
	Time(int hour = 0, int min = 0, int sec = 0)
		:_hour(hour),
		_min(min),
		_sec(sec)
	{}

	void SetYearInTime(int year, int month, int day)
	{
		_d._year = year;
		_d._month = month;
		_d._day = day;
	}
};

int main()
{
	Time t(5, 13, 14);
	t.SetYearInTime(2022, 5, 21);

	return 0;
}
*/

/*
//内部类
//**特征：
//1.外部类默认是内部类的友元
//2.内部类可以定义在外部类的任何地方
//3.sizeof(外部类)=外部类，和内部类没有任何关系
class A
{
public:
	class B
	{
	public:
		void InBtoA(const A& a)
		{
			cout << a._numa << endl; //B默认就是A的友元
			_testa;
		}

	private:
		static int _testb;
		int _numb;
	};

private:
	static int _testa;
	int _numa = 5201314;
};

int A:: _testa = 520;
int A::B::_testb = 1314;

int main()
{
	A::B b; //定义B类对象
	b.InBtoA(A()); //**匿名对象具有常性，隐式类型转换时，也是创建的匿名对象
	return 0;
}
*/


/*
//C++11特性 - 打补丁
class Date
{
public:
	//如果在初始化列表阶段，没有对成员变量初始化
	//C++11下会自动使用缺省值初始化
	Date(int year = 2022)
	{
		_year = year;
	}
private:
	int _year = 520; //这个地方不是初始化，在这里只允许声明
	//520给的是_year的缺省值
	int _month = 13;
	int _day = 14; //需要注意的是：尽管有构造函数，但在构造函数中没有初始化
	//那么还是会使用缺省值
};

int main()
{
	Date d;
	return 0;
}
*/