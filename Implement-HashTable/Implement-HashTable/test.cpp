#define _CRT_CECURE_NOWARNINGS 1

#include "hash_table.h"

template <class T>
struct HashFunc_Default
{
	size_t operator() (const T& t)
	{
		return t;
	}
};

struct HashFunc_String
{
	size_t operator() (const string& str)
	{
		size_t ret = 0;

		for (auto& e : str)
		{
			ret += e * 31;
		}

		return ret;
	}
};

int main()
{
	//闭散列/开放定值法
	{
		//ht1
		CloseHash::HashTable<int, int> ht1;
		ht1.Insert(make_pair(2, 2));
		ht1.Insert(make_pair(12, 12));
		ht1.Insert(make_pair(22, 22));
		ht1.Insert(make_pair(32, 32));
		ht1.Insert(make_pair(42, 42));
		ht1.Insert(make_pair(52, 52));
		ht1.Insert(make_pair(62, 62));
		ht1.Insert(make_pair(72, 72));
		ht1.Insert(make_pair(82, 82));
		ht1.Insert(make_pair(92, 92));

		//ht2
		CloseHash::HashTable<string, int, HashFunc_String> ht2;
		ht2.Insert(make_pair("Thepale", 1));
		ht2.Insert(make_pair("Jones", 2));
		ht2.Insert(make_pair("abc", 3));
		ht2.Insert(make_pair("bbb", 4));
	}
	
	//开散列/拉链法/哈希桶
	{
		LinkHash::HashTable<int, int> ht1;
		ht1.Insert(make_pair(2, 2));
		ht1.Insert(make_pair(12, 12));
		ht1.Insert(make_pair(22, 22));
		ht1.Insert(make_pair(32, 32));
		ht1.Insert(make_pair(42, 42));
		ht1.Insert(make_pair(52, 52));
		ht1.Insert(make_pair(62, 62));
		ht1.Insert(make_pair(72, 72));
		ht1.Insert(make_pair(82, 82));
		ht1.Insert(make_pair(92, 92));
		ht1.Insert(make_pair(21, 21));

		ht1.Erase(62);
	}

	return 0;
}