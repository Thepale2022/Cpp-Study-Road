#pragma once

#include <iostream>
using namespace std;

#include <vector>
#include <string>
#include <list>

namespace CloseHash
{
	enum Status
	{
		EXIST,
		DELETE,
		EMPTY
	};

	template <class K, class V>
	struct HashData
	{
		HashData(const pair<K, V>& kv = make_pair(K(), V()), Status status = EXIST)
			:_kv(kv)
			, _status(status)
		{}

		pair<K, V> _kv;
		Status _status;
	};

	template <class K, class V, class HashFunc = HashFunc_Default<K>>
	class HashTable
	{
	public:
		HashTable()
			:_n(0)
		{}

		HashFunc hf;

		HashData<K, V>* Find(const K& key)
		{
			if (_tables.size() == 0)
			{
				return nullptr;
			}

			size_t pos = hf(key) % _tables.size();

			//线性探测
			while (_tables[pos]._status != EMPTY)
			{
				if (_tables[pos]._status == EXIST && _tables[pos]._kv.first == key) //比较相等不要用hf转换
				{
					return &_tables[pos];
				}

				++pos;
				pos %= _tables.size();
			}

			return nullptr;
		}

		bool Insert(const pair<K, V>& kv)
		{
			//检查是否重复
			auto ret = Find(kv.first);
			if (ret != nullptr)
			{
				return false;
			}

			//依据载荷因子扩容
			//ps：载荷因子越小，冲突概率越低，浪费空间越多（或许二次探测需要更小的载荷因子）
			if (_tables.size() == 0 || (double)_n / (double)_tables.size() >= 0.7)
			{
				if (_tables.size() == 0)
				{
					_tables.resize(10, HashData<K, V>(make_pair(K(), V()), EMPTY));
				}
				else
				{
					//扩容
					size_t NewSize = _tables.size() * 2;
					HashTable NewTable;
					NewTable._tables.resize(NewSize, HashData<K, V>(make_pair(K(), V()), EMPTY));

					for (size_t i = 0; i < _tables.size(); ++i)
					{
						if (_tables[i]._status == EXIST)
						{
							NewTable.Insert(_tables[i]._kv);
						}

					}
					_tables.swap(NewTable._tables);
				}
			}

			size_t pos = hf(kv.first) % _tables.size();

			//线性探测
			while (_tables[pos]._status != EMPTY)
			{
				++pos;
				pos %= _tables.size();
			}

			_tables[pos]._kv = kv;
			_tables[pos]._status = EXIST;
			++_n;

			//二次探测
			//size_t index = pos, i = 0;
			//while (_tables[pos]._status == EXIST)
			//{
			//	++i;
			//	pos = index + i * i;
			//	pos %= _tables.size();
			//}
			// 
			//_tables[pos]._kv = kv;
			//_tables[pos]._status = EXIST;
			//++_n;


			return true;
		}

		//二次拥挤在一定程度上缓解了线性探测的拥挤情况，但在剩余空间小的情况下
		//CPU缓冲不命中概率直线上升，应把载荷因子控制在 0.7~0.8 之间

		bool Erase(const K& key)
		{
			auto ret = Find(hf(key));
			if (ret == nullptr)
			{
				return false;
			}
			else
			{
				_n--;
				ret->_status = DELETE;
				return true;
			}
		}

	protected:
		vector<HashData<K, V>> _tables;
		size_t _n;
	};
}

namespace LinkHash
{
	template <class K, class V>
	struct HashData
	{
		HashData(const pair<K, V>& kv = make_pair(K(), V()))
			:_kv(kv)
			,_next(nullptr)
		{}

		pair<K, V> _kv;
		HashData* _next;
	};

	template <class K, class V, class HashFunc = HashFunc_Default<K>>
	class HashTable
	{
	public:
		typedef HashData<K, V> Node;
		HashFunc hf;

		//构造函数
		HashTable()
			:_n(0)
		{
			if (_ht.size() == 0)
			{
				_ht.resize(10, nullptr);
			}
		}

		//成员函数
		Node* Find(const K& key)
		{
			size_t pos = hf(key) % _ht.size();

			Node* cur = _ht[pos];
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					return cur;
				}

				cur = cur->_next;
			}

			return nullptr;
		}

		bool Insert (const pair<K, V>& kv)
		{
			auto ret = Find(kv.first);
			if (ret != nullptr)
			{
				return false;
			}

			//扩容
			if (_ht.size() == _n)
			{
				size_t NewSize = _ht.size() * 2;
				vector<Node*> NewTable;
				NewTable.resize(NewSize);

				for (size_t i = 0; i < _ht.size(); ++i)
				{
					Node* cur = _ht[i];
					while (cur != nullptr)
					{
						Node* next = cur->_next;
						size_t pos = hf(cur->_kv.first) % NewSize;
						
						//头插
						Node* temp = NewTable[pos];
						NewTable[pos] = cur;
						cur->_next = temp;

						cur = next;
					}
					_ht[i] = nullptr;
				}

				swap(_ht, NewTable);
			}


			//插入
			size_t pos = hf(kv.first) % _ht.size();

			Node* temp = _ht[pos];
			_ht[pos] = new Node(kv);
			_ht[pos]->_next = temp;

			_n++;

			return true;
		}

		bool Erase(const K& key)
		{
			if (_ht.empty())
			{
				return false;
			}

			size_t pos = hf(key) % _ht.size();

			Node* prev = nullptr;
			Node* cur = _ht[pos];
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					//头删	
					if (prev == nullptr)
					{
						_ht[pos] = cur->_next;
					}
					//正常删除
					else
					{
						prev->_next = cur->_next;
					}

					--_n;
					delete cur;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}
			
			return false;
		}

	protected:
		vector<Node*> _ht;
		size_t _n;
	};
}


