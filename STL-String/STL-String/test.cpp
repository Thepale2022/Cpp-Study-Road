#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include <string>

int main()
{
	//构造
	cout << "构造：" << endl;
	string s1; //string [objname]; - 构造空对象
	string s2("hello"); //string [objname](const char* str); - 以字符串构造对象
	string s3(s2); //string [objname](); - 拷贝构造
	string s4(10, 'U'); //string [objname](size_t n, char c); - n 个字符 c 的对象

	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;


	//成员函数
	cout << "成员函数：" << endl;
	int ret = 0;
	ret = s1.size(); //size - 计算字符串长度（为了统一，一般不用length）
	ret = s1.capacity(); //capacity - 计算字符串开辟的空间容量
	ret = s1.empty(); //empty - 字符串为空则返回1，非空则返回0
	s1.clear(); //清除所有有效字符但不改变容量

	s2[1] = 'a'; //下标引用操作符重载
	cout << s2 << endl;
	s2.at(1) = 'a'; //等同于operator[]
	//at 和 [] 检查越界方式不一样，at抛异常，[]断言
	ret = s2.front(); //返回字符串首字符
	ret = s2.back(); //返回字符串尾字符

	s1 += "I love ZC"; //+=操作符重载
	s1.push_back('!'); //尾插字符
	s1.append("Forever!"); //尾插字符串
	cout << s1 << endl;

	s1.reserve(1000); // 申请至少能存储1000个数据的容量
	cout << s1.capacity() << endl;
	s1.resize(100, 'c'); //开空间且初始化
	cout << s1 << endl;

	auto p = s1.c_str(); //返回 const char* 指针

	string file("Iloveyou.txt");
	size_t pos = file.find('.'); //查找指定的字符或字符串并返回下标
	if (pos != file.npos) //找不到会返回npos
	{
		string suffix = file.substr(pos, file.size()); //截取字符串
		cout << suffix << endl;
	}

	file = "ZC.txt.zip"; // = 重载会直接替换
	pos = file.rfind('.'); //逆序查找
	if (pos != file.npos) //找不到会返回npos
	{
		string suffix = file.substr(pos, file.size()); //截取字符串
		cout << suffix << endl;
	}

	string url("https://www.thepale2022.cn/");
	pos = url.find("www");
	string domain = url.substr(pos, url.find('/', pos) - pos);
	cout << domain << endl;

	string i("LoveZC");
	i.insert(0, "ZQ"); //插入
	i.insert(i.end(), '!');
	cout << i << endl;

	i.erase(0, 2); //删除
	cout << i << endl;
	//头删头插效率低


	ret = stoi("1234"); //一些转换
	cout << ret << endl;

	string double_str = to_string(5.201314);
	cout << double_str << endl;


	//迭代器 - 访问和修改
	cout << "迭代器:" << endl;
	string::iterator it = s1.begin(); //顺序迭代
	while (it != s1.end())
	{
		cout << *it;
		it++;
	}
	cout << endl;

	string::reverse_iterator rit = s1.rbegin(); //逆序迭代
	while (rit != s1.rend())
	{
		cout << *rit;
		rit++;
	}
	cout << endl;

	const string s5("I love you");
	string::const_iterator cit = s5.cbegin(); //const顺序迭代
	while (cit != s5.cend())
	{
		cout << *cit;
		cit++;
	}
	cout << endl;

	const string s6("I love you");
	string::const_reverse_iterator crit = s6.crbegin(); //const顺序迭代
	while (crit != s6.crend())
	{
		cout << *crit;
		crit++;
	}
	cout << endl;

	return 0;
}

//建议用getline(cin, [objname]); 来获取一行