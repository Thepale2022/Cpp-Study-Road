#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#include <string>
#include <list>

//可变参数列表

//展开 - 1
//template <class T>
//void ShowList(const T& t) //中止函数
//{
//	cout << typeid(t).name() << ":" << t << endl;
//}
//
//template <class T, class ...Args>
//void ShowList(T val, Args... args) //递归解析
//{
//	//解析参数包
//	cout << typeid(val).name() << ":" << val << endl;
//	ShowList(args...);
//
//	//sizeof可以用来计算args的参数个数
//	cout << sizeof...(args) << endl;
//}

//展开 - 2
//template <class T>
//int PrintArgs(const T& t)
//{
//	cout << typeid(t).name() << ":" << t << endl;
//	return 0;
//}
//
//template < class ...Args>
//void ShowList(Args... args) //递归解析
//{
//	//解析参数包
//	//int arr[] = { (PrintArgs(args), 0)... };
//	int arr[] = { PrintArgs(args)... };
//}
//
//int main()
//{
//	ShowList(1); cout << endl;
//	ShowList(1, 'a'); cout << endl;
//	ShowList(1, 'a', string("Thepale")); cout << endl;
//
//
//	return 0;
//}


//////////////////////////////////////////////////////////////////////////////////////////////////

//emplace
//emplace_back 和 push_back

namespace Thepale
{
	class string
	{
	public:
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
			cout << "string(const char* str) - 构造" << endl;
		}

		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}

		// 拷贝构造
		string(const string& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(const string& s) -- 深拷贝" << endl;

			string tmp(s._str);
			swap(tmp);
		}

		//移动构造
		string(string&& s) noexcept
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) -- 资源转移" << endl;

			this->swap(s);
		}

		//移动赋值
		string& operator=(string&& s) noexcept
		{
			cout << "string& operator=(string&& s) -- 转移资源" << endl;
			swap(s);

			return *this;
		}

		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 深拷贝" << endl;
			string tmp(s);
			swap(tmp);

			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity; // 不包含最后做标识的\0
	};
}

int main()
{
	//emplace特性分析 - 直接构造
	cout << "emplace特性分析 - 直接构造" << endl;
	list<Thepale::string> strls;
	Thepale::string str1; cout << endl;

	strls.push_back(str1); cout << endl; //左值 - 深拷贝
	strls.push_back("Thepale2022"); cout << endl; //隐式类型构造 + 资源转移

	strls.emplace_back(str1); cout << endl; //左值 - 深拷贝
	strls.emplace_back("Thepale2022"); cout << endl; //直接构造

	cout << endl;
	//深拷贝是现代写法，所以多出来一次构造，可调试分析


	//emplace特性分析 - 直接传参
	cout << "emplace特性分析 - 直接传参" << endl;
	list<pair<int, int>> ls;
	pair<int, int> pr1(1, 2);

	ls.push_back(pr1);
	ls.push_back(make_pair(1, 2));

	ls.emplace_back(pr1);
	ls.emplace_back(1, 2); //emplace 可以支持直接传参，因为其参数是 args... 程序递归到最后会直接构造到 ls 中

	cout << endl;

	//move右值问题分析
	cout << "move右值问题分析" << endl;
	Thepale::string test1("Thepale2022");
	Thepale::string test2("Thepale2022");

	strls.push_back(move(test1));
	strls.emplace_back(move(test2));

	//这里的 emplace 也是资源转移，其原因是被 move 的对象已经是构造好了的，只能资源转移而不能直接构造
	//所以必须转移 move 后的对象的资源，emplace 也无能为力，请分清这种情况

	//emplace 并没有比 push_back 更高效，右值版本 push_back 只是多了一次移动，消耗很少

	return 0;
}